## Instruktioner

1. I det klonet projekts folder. Skift til folderen SqlInjection

2. udfør taint analysen ved at følge instruktionerne fra afsnittet Testing a repository from its root folder i denne dokumentation

3. Noter dig hvilke linje nummer SQL inject sårbarhed bliver fundet på.

4.  Verificer i koden, at linje nummeret for sårbarheden er der hvor den forurenet data sendes til sink.

Først Test

    PS C:\Users\mkn\Documents\GitHub\ASP.NET-Core-SQL-Injection-Example\SqlInjection> Snyk code test

    Testing C:\Users\mkn\Documents\GitHub\ASP.NET-Core-SQL-Injection-Example\SqlInjection ...

    ✗ [Low] Anti-forgery token validation disabled
        Path: Controllers/HomeController.cs, line 63
        Info: This ASP.NET MVC action should use an anti-forgery validation attribute. Not using this attribute disables Cross Site Request Forgery (CSRF) protection and allows CSRF attacks.

    ✗ [High] SQL Injection
        Path: Controllers/HomeController.cs, line 32
        Info: Unsanitized input from an HTTP parameter flows into QueryAsync, where it is used in an SQL query. This may result in an SQL Injection vulnerability.


    ✔ Test completed

    Organization:      undefined
    Test type:         Static code analysis
    Project path:      C:\Users\mkn\Documents\GitHub\ASP.NET-Core-SQL-Injection-Example\SqlInjection

    2 Code issues found
    1 [High]  1 [Low]



    