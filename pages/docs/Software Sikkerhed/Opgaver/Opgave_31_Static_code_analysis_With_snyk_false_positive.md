## Instruktioner

1. I klassen HomeController, opret en privat metode til validering af en tekst streng. Teksten må kun indehold alfabetiske karakter.

2. I metoden SearchStudentUnsecure skal du nu bruge validerings metoden, til at valider tekst strengen name. Validerings metoden skal være den første metode der bliver kaldt

3. Eksekver nu taint analysen med Snyk. (tilsvarende forrige opgave)

4. Noter om SQL Injection stadig detekteres som en sårbarhed.

Med validerings metode

    PS C:\Users\mkn\Documents\GitHub\ASP.NET-Core-SQL-Injection-Example\SqlInjection> Snyk code test

    Testing C:\Users\mkn\Documents\GitHub\ASP.NET-Core-SQL-Injection-Example\SqlInjection ...

    ✗ [Low] Anti-forgery token validation disabled
        Path: Controllers/HomeController.cs, line 79
        Info: This ASP.NET MVC action should use an anti-forgery validation attribute. Not using this attribute disables Cross Site Request Forgery (CSRF) protection and allows CSRF attacks.

    ✗ [High] SQL Injection
        Path: Controllers/HomeController.cs, line 35
        Info: Unsanitized input from an HTTP parameter flows into QueryAsync, where it is used in an SQL query. This may result in an SQL Injection vulnerability.


    ✔ Test completed

    Organization:      undefined
    Test type:         Static code analysis
    Project path:      C:\Users\mkn\Documents\GitHub\ASP.NET-Core-SQL-Injection-Example\SqlInjection

    2 Code issues found
    1 [High]  1 [Low]