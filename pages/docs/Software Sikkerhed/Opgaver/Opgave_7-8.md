# Øvelse 7 - (Gruppe øvelse) Fælles forståelse for system & sikkerhedsmål

### Gruppen skal diskuterer dagens forberedelse, som er:
- Introduktion til sikker software udvikling - del 1.
- Introduktion til sikker software udvikling - del 2.
- Introduktion til sikker software udvikling - del 3.
- Secure by design kapitel 1.

#### Hvad er læren fra Öst-Götha Bank røveriet?
(Tænk security in depth)
- At bare fordi du tænker den mest åbenlyse måde at sikre noget (højkvalitets lås) betyder ikke at der ikke også er andre, mindre synlige steder at sikkerheden mangler (i dette tilfælde hængslerne på vault døren)

#### Bør sikkerhed tænkes ind i software design/udvikling som en selvstændig Feature?
- I følge bogen, nej, sikkerhed bør tænkes som et (concern)problem der skal opfyldes, ikke som et sæt features der skal implementeres.

#### Hvilket type angreb er XSS overordnet? (Listing 1.1 viser en klasse som er sårbar overfor dette)
- XSS (Cross site scripting) Det er et injection angreb.

#### Hvad er sammenhængen mellem security in depth og software design?
- Uden ordentligt software design er det svært at tænke og implementere sikkerhed på alle lagene i softwaren.

#### Hvad er forskellen på en use case , og et use case diagram?
- Use case er en beskrivelse på hvordan en user laver en task.
- Diagrammet viser high level funktioner for hele systemet, ikke bare en specifik task.
- Diagrammet er et højere niveau af abstraktion.



#### Inde på borger.dk

#### Udvælg hvilken kontekst af systemet i vil fokuserer på (F.eks. ved at vælge en enkelt aktør)
- Pensionist

#### Definere som minimum 3 system mål med brugstilfælde. Brug eksemplet i dagens forberedelse som vejledning

- Søg folkepension
Brugstilfælde 1: søg folkepension
Forudsætning: Brugen er en bestemt alder, brugeren har nødvendige dokumenter klar og tilgængelig.
Brugeren logger ind på borger.dk med deres MitID. Her bliver de mødt med en række forskellige muligheder alt efter deres behov. De kan, blandt andet, søge, eller udskyde, deres folkepension, samt ændre/opdater de informationer baseret på de dokumenter, brugeren har tilgængelig. Når brugeren har udfyldt alle relevante og nødvendige oplysninger, kan brugeren følge selvbetjeningsløsning til at færdiggøre søgning om pension.

- Se dine egne pensioner – PensionsInfo
Brugstilfælde 2: PensionsInfo
Forudsætning: Brugeren er, gennem Borger, eller på egen hånd, kommet frem til hjemmesiden, samt at de er aktive modtager af pension.
Brugeren logger ind på PensionsInfo med deres MitID. Herefter bliver brugen mødt af et interface, hvor de kan se deres pensions opsparing, samt en masse andet information omkring relevante udbetalinger.

- Søg om tidlig pension
Brugstilfæde 3: Søg om tidlig pension.
Forudsætning: Personen har adgang til MitID, samt adgang til relevante oplysninger.
Brugeren, igennem Borger, bruger deres MitID til at logge ind, og derefter følger den selvbetjening de imødekommer. Gennem denne selvbetjening vil de skulle udfylde felter der er relevante for dem med deres oplysninger/information.
Udarbejde til sidst et brugstilfælde diagram. Brug eksemplet i dagens forberedelse som vejledning

<https://docs.google.com/document/d/1KCHfZlyP8KMIpmo5QUnfDTCb_Zco02AbdcUr7do_Pic/edit?pli=1>
