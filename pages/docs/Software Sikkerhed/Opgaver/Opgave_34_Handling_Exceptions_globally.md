## Instruktioner

- I klassen Startup skal du udkommenterer koden for global håndtering af exceptions(tidligere vist i afsnittet Information), og i stedet indsætte app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");

- Eksekver applikationen, og frem provoker en fejl med ' eller ; (samme fremgang måde som i forrige øvelses).

- Noter dig hvilken fejl der opstår? Giver den fejl mening? Og er den fejl valgt fremfor F.eks. Status kode 500?


I de næste trin af øvelsen skal du helt fjerne den globale fejl håndtering.

- I i metoden Configure, i klassen Startup, skal du udkommenterer app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");

- Eksekver applikationen, og frem provoker en fejl med ' eller ;

- Noter dig hvilken fejl der opstår? Hvilken HTTP status kode bliver der retuneret?


I den sidste øvelse skal Developer exceptions pages anvendes.

- I i metoden Configure, i klassen Startup,skal du indsætte koden app.UseDeveloperExceptionPage(); i starten af koden.

- Eksekver applikationen, og frem provoker en fejl med ' eller ;

- Noter dig hvilken fejl der opstår?



Når man <code>app.UseDeveloperExceptionPage();</code> og sætter <code>app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");</code> 
rammer man siden under

![404 side](../Billeder/404Exceptiom.png)


Når man helt fjerner <code>app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");</code>  rammer den altid stacktrace fejlen som vist under

![Stack trace](../Billeder/SqLiteStackTrace.png)

Når man har udkommentere if statementen helt og kun har <code>app.UseDeveloperExceptionPage();</code> stående rammer man altid stacktrace.