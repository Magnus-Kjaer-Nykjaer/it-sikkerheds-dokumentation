## Instruktioner

Først skal du verificerer at applikationen virker ved at fortag et køb, når du så har verificeret applikationens almindelig virke, skal du fremprovokerer en SQL exception.

### Verificer at applikationen virker.

- Start den klonet applikation.

- Opret en bruger i applikationen.

- Køb nogle vare i applikationen (tilføj vare til kurven og bestil dem).


### Fremprovoker en Sql Exception

- Læg nogle vare i indkøbs kurven.

- Gå til checkout

- Udfyld alle felter i Check out. Men i feltet Name to ship to skal du tilføje karakteren <code>'</code>.

- Klik Place order

- Læs exceptionen og stacktrace som bliver vist. Hvilken information får du?
Stacktracen viser at linje 52 og linje 151 i kildekoden smed exceptionen. Prøv om du kan finde disse i kilde koden. Hvad er årsagen til at applikationen er sårbar?

- Gentag trin 1 - 6. Denne gang med <code>;</code> i stedet for <code>'</code>.



Som man kan se under så fejler den når man indsætter karakteren <code>'</code>

![SqLite Exception](../Billeder/SqLiteException.png)

![Stack trace](../Billeder/SqLiteStackTrace.png)