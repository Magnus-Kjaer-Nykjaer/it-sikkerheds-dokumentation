## Instruktioner

- Opret en test klasse til Firstname domæne primitivet

- Lav i test klassen, en test metode der validerer at der ikke bliver kastet en exception når reglerne for lægenden på navnet bliver overholdt.

- Lav i test klassen, en test metode der validerer at der ikke bliver kastet en exception når reglerne for hvilken karakter navnet må indehold bliver overholdt.

- Lav i test klassen, en test metode der validerer at der bliver kastet en exception når reglerne for længden på navnet ikke bliver overholdt. (For mange eller for får karakterer)

- Lav i test klassen, en test metode der validerer at der bliver kastet en exception når reglerne for hvilken karakter navnet må indeholde ikke bliver overholdt(test både med tal og speciel karakter)

- Lav i test klassen, en test metode der validerer at der bliver kastet en exception når objektet bliver initialiseret med null.


Svar fra første name test

```
public class FirstNameTest
{
  [Theory]
  [InlineData("Bo")]
  [InlineData("Magnus")]
  [InlineData("HejMedDigDetteSkulleGerneVaereEtLangtNavn")]
  public void NameLenghtTest(string name)
  {
    FirstName firstName = new FirstName(name);  
  }
}
```

```
    PS C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests> dotnet test
    Determining projects to restore...
    All projects are up-to-date for restore.
    ValidateTheNameModelBinding -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikk
    erhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\bin\Debug\net7.0\ValidateTheNameModelBinding.dll
    ValidateTheNameModelBinding.Tests -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Softwar
    e Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\bin\Debug\net7.0\ValidateTheNameMode
    lBinding.Tests.dll
    Test run for C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\bin\Debug\net7.0\ValidateTheNameModelBinding.Tests.dll (.NETCoreApp,Version=v7.0)
    Microsoft (R) Test Execution Command Line Tool Version 17.7.1 (x64)
    Copyright (c) Microsoft Corporation.  All rights reserved.

    Starting test execution, please wait...
    A total of 1 test files matched the specified pattern.
    [xUnit.net 00:00:10.44]     ValidateTheNameModelBinding.Tests.FirstNameTest.NameLenghtTest(name: "HejMedDigDetteSkulleGerneVaereEtLangtNavn") [FAIL]
    Failed ValidateTheNameModelBinding.Tests.FirstNameTest.NameLenghtTest(name: "HejMedDigDetteSkulleGerneVaereEtLangtNavn") [2 ms]
    Error Message:
    System.ArgumentException : name is not within length parameters
    Stack Trace:
        at ValidateTheNameModelBinding.Models.FirstName.IsNameValid(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 39
    at ValidateTheNameModelBinding.Models.FirstName..ctor(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 11
    at ValidateTheNameModelBinding.Tests.FirstNameTest.NameLenghtTest(String name) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs:line 13
    at System.RuntimeMethodHandle.InvokeMethod(Object target, Void** arguments, Signature sig, Boolean isConstructor)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)

    Failed!  - Failed:     1, Passed:     2, Skipped:     0, Total:     3, Duration: 8 ms - ValidateTheNameModelBinding.Tests.dll (net7.0)
```


Efter den første test rettede jeg til og lavede nogle flere test der tester for alle tingene 

```
public class FirstNameTest
{
  [Theory]
  [InlineData("Bo")]
  [InlineData("Magnus")]
  [InlineData("HejMedDigDetteSkulleGerneVaereEtLangtNavn")]
  public void NameLenghtTest(string name)
  {
    FirstName firstName = new FirstName(name);
  }
  [Theory]
  [InlineData("")]
  [InlineData("123")]
  [InlineData("æøå")]
  [InlineData("<>'/")]
  public void NameCharacterTest(string name)
  {
    FirstName firstName = new FirstName(name);
  }
  [Fact]
  public void NameNullTest()
  {
    FirstName firstName = new FirstName(null);
  }
}
```

```
    PS C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests> dotnet test
    Determining projects to restore...
    All projects are up-to-date for restore.
    ValidateTheNameModelBinding -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikk
    erhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\bin\Debug\net7.0\ValidateTheNameModelBinding.dll
    C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTe
    stOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs(29,41): warning CS8625: Cannot convert null literal to non-n
    ullable reference type. [C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Pr
    ogrammer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\ValidateTheNameModelBinding.Tests.csproj]
    ValidateTheNameModelBinding.Tests -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Softwar
    e Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\bin\Debug\net7.0\ValidateTheNameMode
    lBinding.Tests.dll
    Test run for C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\bin\Debug\net7.0\ValidateTheNameModelBinding.Tests.dll (.NETCoreApp,Version=v7.0)
    Microsoft (R) Test Execution Command Line Tool Version 17.7.1 (x64)
    Copyright (c) Microsoft Corporation.  All rights reserved.

    Starting test execution, please wait...
    A total of 1 test files matched the specified pattern.
    [xUnit.net 00:00:01.13]     ValidateTheNameModelBinding.Tests.FirstNameTest.NameLenghtTest(name: "HejMedDigDetteSkulleGerneVaereEtLangtNavn") [FAIL]
    [xUnit.net 00:00:01.14]     ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "123") [FAIL]
    [xUnit.net 00:00:01.15]     ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "æøå") [FAIL]
    [xUnit.net 00:00:01.15]     ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "<>'/") [FAIL]
    [xUnit.net 00:00:01.15]     ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "") [FAIL]
    [xUnit.net 00:00:01.15]     ValidateTheNameModelBinding.Tests.FirstNameTest.NameNullTest [FAIL]
    Failed ValidateTheNameModelBinding.Tests.FirstNameTest.NameLenghtTest(name: "HejMedDigDetteSkulleGerneVaereEtLangtNavn") [2 ms]
    Error Message:
    System.ArgumentException : name is not within length parameters
    Stack Trace:
        at ValidateTheNameModelBinding.Models.FirstName.IsNameValid(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 39
    at ValidateTheNameModelBinding.Models.FirstName..ctor(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 11
    at ValidateTheNameModelBinding.Tests.FirstNameTest.NameLenghtTest(String name) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs:line 13
    at System.RuntimeMethodHandle.InvokeMethod(Object target, Void** arguments, Signature sig, Boolean isConstructor)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)
    Failed ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "123") [< 1 ms]
    Error Message:
    System.ArgumentException : name not valid
    Stack Trace:
        at ValidateTheNameModelBinding.Models.FirstName.IsNameValid(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 34
    at ValidateTheNameModelBinding.Models.FirstName..ctor(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 11
    at ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(String name) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs:line 23
    at System.RuntimeMethodHandle.InvokeMethod(Object target, Void** arguments, Signature sig, Boolean isConstructor)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)
    Failed ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "æøå") [< 1 ms]
    Error Message:
    System.ArgumentException : name not valid
    Stack Trace:
        at ValidateTheNameModelBinding.Models.FirstName.IsNameValid(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 34
    at ValidateTheNameModelBinding.Models.FirstName..ctor(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 11
    at ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(String name) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs:line 23
    at InvokeStub_FirstNameTest.NameCharacterTest(Object, Object, IntPtr*)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)
    Failed ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "<>'/") [< 1 ms]
    Error Message:
    System.ArgumentException : name not valid
    Stack Trace:
        at ValidateTheNameModelBinding.Models.FirstName.IsNameValid(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 34
    at ValidateTheNameModelBinding.Models.FirstName..ctor(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 11
    at ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(String name) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs:line 23
    at InvokeStub_FirstNameTest.NameCharacterTest(Object, Object, IntPtr*)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)
    Failed ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(name: "") [< 1 ms]
    Error Message:
    System.ArgumentException : name is null
    Stack Trace:
        at ValidateTheNameModelBinding.Models.FirstName.IsNameValid(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 24
    at ValidateTheNameModelBinding.Models.FirstName..ctor(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 11
    at ValidateTheNameModelBinding.Tests.FirstNameTest.NameCharacterTest(String name) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs:line 23
    at InvokeStub_FirstNameTest.NameCharacterTest(Object, Object, IntPtr*)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)
    Failed ValidateTheNameModelBinding.Tests.FirstNameTest.NameNullTest [< 1 ms]
    Error Message:
    System.ArgumentException : name is null
    Stack Trace:
        at ValidateTheNameModelBinding.Models.FirstName.IsNameValid(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 24
    at ValidateTheNameModelBinding.Models.FirstName..ctor(String firstName) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding\Models\FirstName.cs:line 11
    at ValidateTheNameModelBinding.Tests.FirstNameTest.NameNullTest() in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\InvarianceUnitTestOpave\ValidateTheNameModelBinding.Tests\FirstNameTest.cs:line 29
    at System.RuntimeMethodHandle.InvokeMethod(Object target, Void** arguments, Signature sig, Boolean isConstructor)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)

    Failed!  - Failed:     6, Passed:     2, Skipped:     0, Total:     8, Duration: 16 ms - ValidateTheNameModelBinding.Tests.dll (net7.0)
```