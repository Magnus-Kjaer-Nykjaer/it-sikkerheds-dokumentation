
Resultat fra den første test.

```
    PS C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests> dotnet test
    Determining projects to restore...
    All projects are up-to-date for restore.
    PrimeService -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programme
    r\unit-testing-using-dotnet-test\PrimeService\bin\Debug\net7.0\PrimeService.dll
    PrimeService.Tests -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Pro
    grammer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll
    Test run for C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll (.NETCoreApp,Version=v7.0)
    Microsoft (R) Test Execution Command Line Tool Version 17.7.1 (x64)
    Copyright (c) Microsoft Corporation.  All rights reserved.

    Starting test execution, please wait...
    A total of 1 test files matched the specified pattern.
    [xUnit.net 00:00:04.49]     Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_InputIs1_ReturnFalse [FAIL]
    Failed Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_InputIs1_ReturnFalse [3 ms]
    Error Message:
    System.NotImplementedException : Not implemented.
    Stack Trace:
        at Prime.Services.PrimeService.IsPrime(Int32 candidate) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService\PrimeService.cs:line 9
    at Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_InputIs1_ReturnFalse() in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests\PrimeService_IsPrimeShould.cs:line 12
    at System.RuntimeMethodHandle.InvokeMethod(Object target, Void** arguments, Signature sig, Boolean isConstructor)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)

    Failed!  - Failed:     1, Passed:     0, Skipped:     0, Total:     1, Duration: < 1 ms - PrimeService.Tests.dll (net7.0)
```


Resultat efter at jeg har rette koden til ikke at fejle

```
    PS C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests> dotnet test
    Determining projects to restore...
    All projects are up-to-date for restore.
    PrimeService -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programme
    r\unit-testing-using-dotnet-test\PrimeService\bin\Debug\net7.0\PrimeService.dll
    PrimeService.Tests -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Pro
    grammer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll
    Test run for C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll (.NETCoreApp,Version=v7.0)
    Microsoft (R) Test Execution Command Line Tool Version 17.7.1 (x64)
    Copyright (c) Microsoft Corporation.  All rights reserved.

    Starting test execution, please wait...
    A total of 1 test files matched the specified pattern.

    Passed!  - Failed:     0, Passed:     1, Skipped:     0, Total:     1, Duration: < 1 ms - PrimeService.Tests.dll (net7.0)
```


Efter at have implementeret Theory og InlineData får vi dette resultat, dette er før ændre det til at virke.

```
    PS C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests> dotnet test
    Determining projects to restore...
    All projects are up-to-date for restore.
    PrimeService -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programme
    r\unit-testing-using-dotnet-test\PrimeService\bin\Debug\net7.0\PrimeService.dll
    PrimeService.Tests -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Pro
    grammer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll
    Test run for C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll (.NETCoreApp,Version=v7.0)
    Microsoft (R) Test Execution Command Line Tool Version 17.7.1 (x64)
    Copyright (c) Microsoft Corporation.  All rights reserved.

    Starting test execution, please wait...
    A total of 1 test files matched the specified pattern.
    [xUnit.net 00:00:00.71]     Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(value: 0) [FAIL]
    [xUnit.net 00:00:00.71]     Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(value: -1) [FAIL]
    Failed Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(value: 0) [< 1 ms]
    Error Message:
    System.NotImplementedException : Not fully implemented.
    Stack Trace:
        at Prime.Services.PrimeService.IsPrime(Int32 candidate) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService\PrimeService.cs:line 13
    at Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(Int32 value) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests\PrimeService_IsPrimeShould.cs:line 21
    at InvokeStub_PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(Object, Object, IntPtr*)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)
    Failed Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(value: -1) [< 1 ms]
    Error Message:
    System.NotImplementedException : Not fully implemented.
    Stack Trace:
        at Prime.Services.PrimeService.IsPrime(Int32 candidate) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService\PrimeService.cs:line 13
    at Prime.UnitTests.Services.PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(Int32 value) in C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests\PrimeService_IsPrimeShould.cs:line 21
    at InvokeStub_PrimeService_IsPrimeShould.IsPrime_ValuesLessThan2_ReturnFalse(Object, Object, IntPtr*)
    at System.Reflection.MethodInvoker.Invoke(Object obj, IntPtr* args, BindingFlags invokeAttr)

    Failed!  - Failed:     2, Passed:     1, Skipped:     0, Total:     3, Duration: 35 ms - PrimeService.Tests.dll (net7.0)
```


Efter at have rettet det til at pass, får vi dette svar

```
    PS C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests> dotnet test
    Determining projects to restore...
    All projects are up-to-date for restore.
    PrimeService -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programme
    r\unit-testing-using-dotnet-test\PrimeService\bin\Debug\net7.0\PrimeService.dll
    PrimeService.Tests -> C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Pro
    grammer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll
    Test run for C:\Users\mkn\Desktop\It-sikkerhed\it-sikkerheds-dokumentation\pages\docs\Software Sikkerhed\Programmer\unit-testing-using-dotnet-test\PrimeService.Tests\bin\Debug\net7.0\PrimeService.Tests.dll (.NETCoreApp,Version=v7.0)
    Microsoft (R) Test Execution Command Line Tool Version 17.7.1 (x64)
    Copyright (c) Microsoft Corporation.  All rights reserved.

    Starting test execution, please wait...
    A total of 1 test files matched the specified pattern.

    Passed!  - Failed:     0, Passed:     3, Skipped:     0, Total:     3, Duration: 8 ms - PrimeService.Tests.dll (net7.0)
```