

## Opgave 18

``` C#
    static bool nameInputIsvalid(string? nameInput)
    {
        if (nameInput == null)
        {
            return false;
        }

        if (nameInput.Length <= 20 && nameInput.Length >= 3)
        {
            var pattern = @"^[a-zA-Z]+$";
            var inputValidation = Regex.IsMatch(nameInput, pattern);

            if (inputValidation)
            return true;
        }

        return false;
    }
```

## Opgave 19

``` C#
    private Boolean NameIsValid(string name)
    {
      if (name.Length <= 20 && name.Length >= 3)
      {
        var pattern = @"^[a-zA-Z]+$";
        var inputValidation = Regex.IsMatch(name, pattern);

        if (inputValidation)
          return true;
      }

      return false;
    }
```

## Opgave 20

``` C#
  public class PersonDTO
  {
    [NotNull]
    [RegularExpression(@"^[a-zA-Z''-'\s]{3,20}$")]
    [MinLength(3)]
    [MaxLength(20)]
    public string Firstname { get; set; }
    
    [NotNull]
    [RegularExpression(@"^[a-zA-Z''-'\s]{3,20}$")]
    [MinLength(3)]
    [MaxLength(20)]
    public string Lastname { get; set; }
  }
```