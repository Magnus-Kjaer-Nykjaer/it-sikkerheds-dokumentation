## Instruktioner
1. Du skal installer package manageren chocolatey. Følg guide til installation her

2. Brug chocolatey til at installer Snyk CLI, se guide her.

3. Anvend kommandoen Snyk test i projekt folderen.

4. Samling resultaterne med dotnet vulnerability


<https://chocolatey.org/install#individual>

Resultat af Snyk test

    PS C:\Users\mkn\Documents\GitHub\WebGoat.NET\WebGoat.NET> Snyk test

    Testing C:\Users\mkn\Documents\GitHub\WebGoat.NET\WebGoat.NET...

    Tested 197 dependencies for known issues, found 10 issues, 133 vulnerable paths.


    Issues to fix by upgrading:

    Upgrade System.Data.SqlClient@4.8.3 to System.Data.SqlClient@4.8.5 to fix
    ✗ Information Exposure [Medium Severity][https://snyk.io/vuln/SNYK-DOTNET-SYSTEMDATASQLCLIENT-3110424] in System.Data.SqlClient@4.8.3
        introduced by System.Data.SqlClient@4.8.3


    Issues with no direct upgrade or patch:
    ✗ Remote Code Execution (RCE) [High Severity][https://snyk.io/vuln/SNYK-DOTNET-NUGETCOMMON-5709252] in NuGet.Common@6.2.1
        introduced by Microsoft.VisualStudio.Web.CodeGeneration.Design@6.0.8 > NuGet.Packaging@6.2.1 > NuGet.Configuration@6.2.1 > NuGet.Common@6.2.1 and 18 other path(s)
    This issue was fixed in versions: 6.0.5, 6.2.4, 6.3.3, 6.4.2, 6.5.1, 6.6.1
    ✗ Privilege Escalation [High Severity][https://snyk.io/vuln/SNYK-DOTNET-NUGETPROTOCOL-3043006] in NuGet.Protocol@5.11.0
        introduced by Microsoft.VisualStudio.Web.CodeGeneration.Design@6.0.8 > Microsoft.DotNet.Scaffolding.Shared@6.0.8 > NuGet.ProjectModel@5.11.0 > NuGet.DependencyResolver.Core@5.11.0 > NuGet.Protocol@5.11.0 and 3 other path(s)
    This issue was fixed in versions: 4.9.6, 5.7.3, 5.9.3, 5.11.3, 6.0.3, 6.2.2, 6.3.1
    ✗ Remote Code Execution (RCE) [High Severity][https://snyk.io/vuln/SNYK-DOTNET-NUGETPROTOCOL-5709254] in NuGet.Protocol@5.11.0
        introduced by Microsoft.VisualStudio.Web.CodeGeneration.Design@6.0.8 > Microsoft.DotNet.Scaffolding.Shared@6.0.8 > NuGet.ProjectModel@5.11.0 > NuGet.DependencyResolver.Core@5.11.0 > NuGet.Protocol@5.11.0 and 3 other path(s)
    This issue was fixed in versions: 6.0.5, 6.2.4, 6.3.3, 6.4.2, 6.5.1, 6.6.1
    ✗ Denial of Service (DoS) [High Severity][https://snyk.io/vuln/SNYK-DOTNET-SYSTEMNETHTTP-60045] in System.Net.Http@4.3.0
        introduced by Microsoft.EntityFrameworkCore.Proxies@6.0.8 > Castle.Core@4.4.1 > NETStandard.Library@1.6.1 > System.Net.Http@4.3.0 and 12 other path(s)
    This issue was fixed in versions: 4.1.2, 4.3.2
    ✗ Improper Certificate Validation [High Severity][https://snyk.io/vuln/SNYK-DOTNET-SYSTEMNETHTTP-60046] in System.Net.Http@4.3.0
        introduced by Microsoft.EntityFrameworkCore.Proxies@6.0.8 > Castle.Core@4.4.1 > NETStandard.Library@1.6.1 > System.Net.Http@4.3.0 and 12 other path(s)
    This issue was fixed in versions: 4.1.2, 4.3.2
    ✗ Privilege Escalation [High Severity][https://snyk.io/vuln/SNYK-DOTNET-SYSTEMNETHTTP-60047] in System.Net.Http@4.3.0
        introduced by Microsoft.EntityFrameworkCore.Proxies@6.0.8 > Castle.Core@4.4.1 > NETStandard.Library@1.6.1 > System.Net.Http@4.3.0 and 12 other path(s)
    This issue was fixed in versions: 4.1.2, 4.3.2
    ✗ Authentication Bypass [Medium Severity][https://snyk.io/vuln/SNYK-DOTNET-SYSTEMNETHTTP-60048] in System.Net.Http@4.3.0
        introduced by Microsoft.EntityFrameworkCore.Proxies@6.0.8 > Castle.Core@4.4.1 > NETStandard.Library@1.6.1 > System.Net.Http@4.3.0 and 12 other path(s)
    This issue was fixed in versions: 4.1.2, 4.3.2
    ✗ Information Exposure [High Severity][https://snyk.io/vuln/SNYK-DOTNET-SYSTEMNETHTTP-72439] in System.Net.Http@4.3.0
        introduced by Microsoft.EntityFrameworkCore.Proxies@6.0.8 > Castle.Core@4.4.1 > NETStandard.Library@1.6.1 > System.Net.Http@4.3.0 and 12 other path(s)
    This issue was fixed in versions: 2.0.20710, 4.0.1-beta-23225, 4.1.4, 4.3.4
    ✗ Regular Expression Denial of Service (ReDoS) [High Severity][https://snyk.io/vuln/SNYK-DOTNET-SYSTEMTEXTREGULAREXPRESSIONS-174708] in System.Text.RegularExpressions@4.3.0
        introduced by Microsoft.EntityFrameworkCore.Proxies@6.0.8 > Castle.Core@4.4.1 > NETStandard.Library@1.6.1 > System.Text.RegularExpressions@4.3.0 and 39 other path(s)
    This issue was fixed in versions: 4.3.1



    Organization:      magnus-kjaer-nykjaer
    Package manager:   nuget
    Target file:       obj/project.assets.json
    Project name:      WebGoat.NET
    Open source:       no
    Project path:      C:\Users\mkn\Documents\GitHub\WebGoat.NET\WebGoat.NET
    Licenses:          enabled



    A medium severity vulnerability was found in the Snyk CLI versions you are using.
    We fixed the vulnerability in version 1.996.0. We recommend updating to the latest version.
    More details here: https://snyk.co/ue1NS