## Instruktioner

1. Gå ind i folderen til det klonet repository.

2. Følg instruksen fra denne tutorial for at udføre en 3. Parts sårbarheds skanning(Se afsnittet dotnet CLI)

3. Følg linket til den fundende sårbarhed, og noter CVE nummeret(i toppen af siden fra linket).

4. Slå CVE'en op på CVE.org. Kan der findes mere information om CVE'en?

5. Brug --include-transitive for at se under sårbarhederne. (Se tutorial for yderlige information)



![Dotnet List Package](../Billeder/DotnetListPackage.png)

CVE-2022-41064

<https://www.cve.org/CVERecord?id=CVE-2022-41064>