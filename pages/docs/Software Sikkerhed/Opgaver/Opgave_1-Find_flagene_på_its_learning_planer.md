# Instruktioner
Der skal findes 3 flag på its learning planer. Hvert trin i instruktionerne, er en ledetråd til hvor flaget kan findes. Lokationen til alle 3 flag skal findes. Herudover skal alle flagene være i deres oprindelig format (og fremgangsmåden til at konventer dem til deres oprindelig format skal kunne beskrives). Altså, opgaven er som følger:

Find lokationen til de 3 flag.

Konvetere flagene til deres oprindelig format

Vær forberedt på at kunne beskrive fremgangsmåden for at konventer hvert flag til deres oprindelige format.

# Ledetråd til hvert af de 3 flag

Forberedelsen til den lektion hvor et af emnerne er statisk kode analyse.

En resource til den sidste lektion, inden arbejdet med eksamens projektet starter.

En refleksion efter du har forberedt dig til undervisning i input validering.


    PHNjcmlwdD5BbGVydCgiZXIiKTwvc2NyaXB0Pg%3D%3D == <script>Alert("er")</script>

    719d067b229178f03bcfa1da4ac4dede == Software

    Jmx0O2RpdiZndDsgZm9yYW5kcmVsaWd0ICZsdDsvZGl2Jmd0Ow== == &lt;div&gt; forandreligt &lt;/div&gt; == <div> forandreligt </div>



    NjY2RjcyNjE2RTY0NjU3MjZDNjk2Nzc0 == foranderligt


