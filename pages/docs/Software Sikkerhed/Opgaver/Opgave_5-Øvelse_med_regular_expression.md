# Opgave 5 (Individuel øvelse) - Regular expression

## Information

Formålet med denne øvelse, er at øge bevidstheden om tilgangen til opgave løsning, når man står overfor en opgave der som udgangspunkt kan virke uoverskuelig, eller handler om et emne som man endnu ikke er introduceret til.

### Alle hjælpe midler er tilladt

## Instruktioner

I denne opgave skal i udarbejde et regular expression (Regex) som kan detekter mønsteret for et dansk cpr nummer. Herefter skal i producerer et eksempel i C# som bruger Regex mønster, til at verificere CPR numre.

Når i har fundet et mønster, kan i teste det med Regex 101<https://regex101.com/>


    /^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{2}[-]?\d{4}$/gm

```
using System.Text.RegularExpressions;

var input = Console.ReadLine();

var regex = @"^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{2}[-]?\d{4}";

Regex rx = new Regex(regex,RegexOptions.Compiled | RegexOptions.IgnoreCase);

// Find matches.
MatchCollection matches = rx.Matches(input);

// Report the number of matches found.
Console.WriteLine("{0} matches found in:\n   {1}", matches.Count, input);

// Report on each match.
foreach (Match match in matches)
{
    GroupCollection groups = match.Groups;
    Console.WriteLine("'{0}' repeated at positions {1} and {2}", 
    groups["word"].Value, groups[0].Index, groups[1].Index);
}

```