using System.Text.RegularExpressions;


var input = Console.ReadLine();

var regex = @"^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{2}[-]?\d{4}";

Regex rx = new Regex(regex,RegexOptions.Compiled | RegexOptions.IgnoreCase);

// Find matches.
MatchCollection matches = rx.Matches(input);

// Report the number of matches found.
Console.WriteLine("{0} matches found in:\n   {1}", matches.Count, input);

// Report on each match.
foreach (Match match in matches)
{
    GroupCollection groups = match.Groups;
    Console.WriteLine("'{0}' repeated at positions {1} and {2}", 
    groups["word"].Value, groups[0].Index, groups[1].Index);
}
