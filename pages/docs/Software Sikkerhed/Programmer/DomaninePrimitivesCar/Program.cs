﻿
using DomaninePrimitivesCar.Model;

Car car = new Car(new RegistrationNumber("AB12345"), new Age(2000), new Color("Roed"));

Console.WriteLine(car.GetRegistrationNumber());
Console.WriteLine(car.GetAge());
Console.WriteLine(car.GetColor());