﻿using System.Text.RegularExpressions;

namespace DomaninePrimitivesCar.Model
{
  internal class Color
  {
    private string _color;
    public Color(string color)
    {
      IsColorValid(color);
      this._color = color;
    }

    public string GetValue()
    {
      return _color;
    }

    private void IsColorValid(string color)
    {
      if (string.IsNullOrEmpty(color))
      {
        throw new ArgumentException("Color is null");
      }

      var pattern = @"^[a-zA-Z]+$";
      var inputValidation = Regex.IsMatch(color, pattern);

      if (!inputValidation)
      {
        throw new ArgumentException("color is not valid");
      }
    }
  }
}
