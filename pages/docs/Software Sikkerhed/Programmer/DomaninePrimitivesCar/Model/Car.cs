﻿namespace DomaninePrimitivesCar.Model
{
  internal class Car
  {
    private RegistrationNumber _registrationNumber;
    private Age _age;
    private Color _color;

    public Car(RegistrationNumber registrationNumber, Age age, Color color) 
    { 
      _registrationNumber = registrationNumber;
      _age = age;
      _color = color;
    }

    public string GetRegistrationNumber()
    {
      return _registrationNumber.GetValue();
    }

    public int GetAge() 
    {
      return _age.GetValue();
    }

    public string GetColor()
    {
      return _color.GetValue();
    }
  }
}
