﻿using System.Text.RegularExpressions;

namespace DomaninePrimitivesCar.Model
{
  internal class RegistrationNumber
  {
    private string _registrationNumber;
    public RegistrationNumber(string registrationNumber)
    {
      IsRegistrationNumberValid(registrationNumber);
      _registrationNumber = registrationNumber;
    }

    public string GetValue()
    {
      return _registrationNumber;
    }

    private void IsRegistrationNumberValid(string registrationNumber)
    {
      if (string.IsNullOrEmpty(registrationNumber))
      {
        throw new ArgumentException("Registration number can not be null");
      }

      var pattern = @"^[A-Z]{2}[0-9]{5}$";
      var inputValidation = Regex.IsMatch(registrationNumber, pattern);

      if (!inputValidation)
      {
        throw new ArgumentException("Registration number is not valid");
      }
    }
  }
}