﻿namespace DomaninePrimitivesCar.Model
{
  internal class Age
  {
    private int age;

    public Age(int age)
    {
      IsAgeValid(age);
      this.age = age;
    }

    public int GetValue()
    {
      return age;
    }

    private void IsAgeValid(int age)
    {
      if (age <= 1886 && age >= 2023)
      {
        throw new ArgumentException("Age cannot be less then 18 or greater than 99");
      }
    }
  }
}
