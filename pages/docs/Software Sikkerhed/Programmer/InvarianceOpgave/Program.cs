﻿using InvarianceOpgave;

var firstName = Console.ReadLine();
var lastName = Console.ReadLine();

var person = new Person (firstName, lastName);

var getFirstName = person.GetFirstName();
var getLastName = person.GetLastName();

Console.WriteLine("First name: " + getFirstName);
Console.WriteLine("Last name: " + getLastName);