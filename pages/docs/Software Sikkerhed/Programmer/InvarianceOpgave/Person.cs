﻿using System.Text.RegularExpressions;

namespace InvarianceOpgave
{
  internal class Person
  {
    private string _firstname;

    private string _lastname;

    public Person(string firstname, string lastname)
    {
      ValidateInput(firstname);
      ValidateInput(lastname);
      _firstname = firstname;
      _lastname = lastname;
    }

    private void ValidateInput(string input)
    {
      if (string.IsNullOrEmpty(input))
      {
        throw new ArgumentException("Input is null");
      }

      if (input.Length <= 20 && input.Length >= 3)
      {
        var pattern = @"^[a-zA-Z]+$";
        var inputValidation = Regex.IsMatch(input, pattern);

        if (!inputValidation)
        {
          throw new ArgumentException("Input not valid");
        }
      }
      else
      {
        throw new ArgumentException("Input is not within length parameters");
      }
    }

    public string GetFirstName()
    {
      return _firstname;
    }

    public string GetLastName()
    {
      return _lastname;
    }
  }
}
