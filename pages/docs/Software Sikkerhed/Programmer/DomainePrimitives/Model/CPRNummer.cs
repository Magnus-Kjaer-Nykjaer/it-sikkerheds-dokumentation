﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DomainePrimitives.Model
{
  public class CPRNummer
  {
    private string? cpr;
    public CPRNummer(string cpr)
    {
      IsCPRValid(cpr);
      this.cpr = cpr;
    }

    public string GetValue()
    {
      if (cpr is not null)
      {
        var tempCPR = cpr;
        cpr = null;
        return tempCPR;
      }
      else
      {
        throw new ArgumentException("CPR can only be fetched once");
      }
    }

    private void IsCPRValid(string cpr)
    {
      if (string.IsNullOrEmpty(cpr))
      {
        throw new ArgumentException("CPR can not be null");
      }

      if (cpr.Length == 11)
      {
        var pattern = @"^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{2}[-]?\d{4}$";
        var inputValidation = Regex.IsMatch(cpr, pattern);

        if (!inputValidation)
        {
          throw new ArgumentException("CPR is not valid");
        }
      }
      else
      {
        throw new ArgumentException("CPR can only be 11 characters long");
      }
    }
  }
}