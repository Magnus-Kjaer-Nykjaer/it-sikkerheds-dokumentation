﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DomainePrimitives.Model
{
  internal class Fornavn
  {
    private string _fornavn;

    public Fornavn(string fornavn)
    {
      IsNameValid(fornavn);
      this._fornavn = fornavn;
    }

    public string GetValue()
    {
      return _fornavn;
    }

    private void IsNameValid(string fornavn)
    {
      if (string.IsNullOrEmpty(fornavn))
      {
        throw new ArgumentException("name is null");
      }

      if (fornavn.Length <= 20 && fornavn.Length >= 1)
      {
        var pattern = @"^[a-zA-Z]+$";
        var inputValidation = Regex.IsMatch(fornavn, pattern);

        if (!inputValidation)
        {
          throw new ArgumentException("name not valid");
        }
      }
      else
      {
        throw new ArgumentException("name is not within length parameters");
      }
    }
  }
}
