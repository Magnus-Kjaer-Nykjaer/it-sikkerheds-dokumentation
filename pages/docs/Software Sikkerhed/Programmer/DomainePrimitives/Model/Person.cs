﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainePrimitives.Model
{
  internal class Person
  {
    private Fornavn Fornavn;
    private Efternavn Efternavn;
    private Alder Alder;
    private CPRNummer CPRNummer;

    public Person(Fornavn fornavn, Efternavn efternavn, Alder alder, CPRNummer cprNummer)
    {
      this.Fornavn = fornavn;
      this.Efternavn = efternavn;
      this.Alder = alder;
      this.CPRNummer = cprNummer;
    }

    public string GetFornavn() 
    {
      return Fornavn.GetValue();
    }

    public string GetEfternavn()
    {
      return Efternavn.GetValue();
    }

    public int GetAlder()
    {
      return Alder.GetValue();
    }

    public string GetCPRNummer()
    {
      return CPRNummer.GetValue();
    }
  }
}
