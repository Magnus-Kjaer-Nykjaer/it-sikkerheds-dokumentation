﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DomainePrimitives.Model
{
  internal class Efternavn
  {
    private string _efternavn;
    public Efternavn(string efternavn)
    {
      IsNameValid(efternavn);
      this._efternavn = efternavn;
    }

    public string GetValue()
    {
      return _efternavn;
    }

    private void IsNameValid(string efternavn)
    {
      if (string.IsNullOrEmpty(efternavn))
      {
        throw new ArgumentException("name is null");
      }

      if (efternavn.Length <= 20 && efternavn.Length >= 1)
      {
        var pattern = @"^[a-zA-Z]+$";
        var inputValidation = Regex.IsMatch(efternavn, pattern);

        if (!inputValidation)
        {
          throw new ArgumentException("name not valid");
        }
      }
      else
      {
        throw new ArgumentException("name is not within length parameters");
      }
    }
  }
}