﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainePrimitives.Model
{
  internal class Alder
  {
    private int alder;

    public Alder(int alder)
    {
      IsAgeValid(alder);
      this.alder = alder;
    }

    public int GetValue()
    {
      return alder;
    }

    private void IsAgeValid(int alder)
    {
      if (alder <= 18 && alder >= 99)
      {
        throw new ArgumentException("Age cannot be less then 18 or greater than 99");
      }
    }
  }
}
