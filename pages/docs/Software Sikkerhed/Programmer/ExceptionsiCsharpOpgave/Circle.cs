﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionsiCsharpOpgave
{
  public class Circle
  {
    public double Radius
    {
      get; set;
    }
    public Circle(double radius)
    {
      if (radius <= 0)
      {
        throw new ArgumentOutOfRangeException(
             nameof(radius),
            "The radius should be positive"
         );
      }
      Radius = radius;
    }
    public double GetArea() => Math.PI * Radius * Radius;

  }
}
