﻿using ExceptionsiCsharpOpgave;

Console.WriteLine("Enter a radius:");
var input = Console.ReadLine();
if (input != null)
{
  var radius = double.Parse(input);
  //try
  //{
    var circle = new Circle(radius);
    Console.WriteLine($"The area is {circle.GetArea():F2}");
  //}
  //catch (ArgumentOutOfRangeException ex)
  //{
  //  Console.WriteLine(ex.Message);
  //}

}