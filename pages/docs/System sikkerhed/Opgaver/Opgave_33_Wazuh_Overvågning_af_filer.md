# Opgave beskrivelse

## Instruktioner

1. opret directoriet /home/SecretFolder

2. Åben Wazuh agentens konfiguration file i en tekst editor. Filen findes i /var/ossec/etc/ossec.conf

3. I blokken <code><syscheck></code> skal følgende block tilføjes <code><directories check_all="yes" report_changes="yes" realtime="yes">/home/SecretFolder</directories></code>

4. genstart wazuh agenten med kommandoen systemctl restart wazuh-agent

5. opret filen /home/SecretFolder/secretFile.txt.

6. Tilføj teksten Bad mojo til filen /home/SecretFolder/secretFile.txt

7. Slet filen /home/SecretFolder/secretFile.txt

## Wazuh Dashboard

1. I Wazuh dashboards, går ind på security events

2. I søge felte skal der indtastes rule.id:(550 OR 553 OR 554)

3. Dette bør frembring 3 nye begivenheder i security alerts

</br>

# Udføresle af opgave

### Part 1

    xubunutuklient@xubunutuklient-virtual-machine:~$ mkdir Secretfolder
    xubunutuklient@xubunutuklient-virtual-machine:~$ ls
    Desktop  Documents  Downloads  Music  Pictures  Public  Secretfolder  snap  Templates  Videos  wazuh-agent.deb
    xubunutuklient@xubunutuklient-virtual-machine:~$ nano /var/ossec/etc/ossec.conf
    xubunutuklient@xubunutuklient-virtual-machine:~$ sudo nano /var/ossec/etc/ossec.conf
    [sudo] password for xubunutuklient: 
    xubunutuklient@xubunutuklient-virtual-machine:~$ systemctl restart wazuh-agent
    xubunutuklient@xubunutuklient-virtual-machine:~$ cd Seccretfolder
    bash: cd: Seccretfolder: No such file or directory
    xubunutuklient@xubunutuklient-virtual-machine:~$ ls
    Desktop  Documents  Downloads  Music  Pictures  Public  Secretfolder  snap  Templates  Videos  wazuh-agent.deb
    xubunutuklient@xubunutuklient-virtual-machine:~$ cd Secretfolder
    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ nano secretfile.txt
    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ ls
    secretfile.txt
    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ rm secretfile.txt 
    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ ls
    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ 

    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ echo Bad mojo > secretfile.txt 
    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ ls
    secretfile.txt
    xubunutuklient@xubunutuklient-virtual-machine:~/Secretfolder$ rm secretfile.txt 

Har prøvet en del forskelige ting men har ikke kunne få den til at sende ændringerne til wazuh

</br>

# Kilder og brugbar links 
