# Opgave beskrivelse

## Instruktioner

1. Tilføj reglen for auditctl -a always,exit -F arch=b64 -S kill -F key=kill_rule

2. start en baggrunds proces med kommandoen sleep 600 &

3. find proces id'et med ps aux på sleep processen Sleep proces

4. dræb processen med kommandoen kill <proces id>

5. Eksekver kommandoen aureport -i -k | grep kill_rule og verificer at der er lavet nye rækker.

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
