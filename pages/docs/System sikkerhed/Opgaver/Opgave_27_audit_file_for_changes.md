# Opgave beskrivelse

## Instruktioner

### Tilføj audit regel med auditctl

1. Opret en audit regel for hver gang der skrives eller ændres på filen /etc/passwd med kommandoen auditctl -w /etc/passwd -p wa -k user_change

Muligheden -w står for where og referer til den file hvor audit reglen skal gælde. -p står for permission og fortæller hvilken rettigheder der skal overvåges. I eksemplet står der wa hvilket står for attribute og write. Hvis filens metadata ændres, eller der skrives til filen, udløser det en begivenhed og der bliver oprettet et audit log

2. udskriv en rapport over alle hændelser der er logget med kommandoen aureport -i -k | grep user_change

Muligheden -i står for intepret, og betyder at numerisk entiter såsom bruger id bliver fortolket som brugernavn. -k står for key, hvilket betyder at regel der udløst audit loggen skal vises

3. tilføj noget text i bunden af filen fra trin 1.

4. Udfør trin 2 igen, og bekræft at der nu er kommet en ny række i rapporten
Der kommer en del nye rækker

5. Brug kommandoen ausearch -i -k user_change. Her skulle du gerne kunne finde en log som ligner den nedstående.

### Tilføj audit regel med audit regel konfigurations file

1. Åben filen /etc/audit/audit.rules. Den skulle gerne se ud som vist nedenunder. 

regel filen bliver indlæst ved opstart af auditd og alle regler heri bliver de gældende audit regler

Dette er auditd's primær konfigurations file, som indeholder alle regel konfigurationerne. men bemærk kommentaren øverst i filen. Den fortælle at denne file bliver autogeneret ud fra indholdet af /etc/audit/audit.d directory (ikke en synderlig intuitiv måde at lave konfigurations opsætning på).

2. Opret en ny file som indeholder reglen, med kommandoen sh -c "auditctl -l > /etc/audit/rules.d/custom.rules"
For at kommandoen virker skal reglen der blev lavet trin 1 forrige afsnit stadig være gældende, kontroller evt. med auditctl -l inden trin 2 udføres

3. Genstart auditd med kommandoen systemctl restart auditd

4. Udskriv indholdet af /etc/audit/audit.rules. Resultatet bør ligne det som vises på billedet nedenunder.

Slet filen /etc/audit/rules.d/custom.rules efter øvelsen

### Audit alle file ændringer.

1. Eksekver kommandoen aureport -f. Med denne bør du få en udskrift der ligner nedstående.


Den første række med nummeret 1. blev indsat 27 Marts 2023. kl. 20:11. begivenheden som udløste loggen var en udskrift af filen tmp.txt. udskrivning blev lavet af brugeren med id 1000. Og udskrivningen blev eksekveret med kommandoen cat

2. Ændre kommandoen fra trin 1 således den skriver brugernavn i stedet for bruger id

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
