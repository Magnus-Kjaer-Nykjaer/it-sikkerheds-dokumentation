# Opgave beskrivelse

## Instruktioner

1. Opret et nyt directory

2. Opret en audit regel med kommandoen auditctl -w <Directory path> -k directory_watch_rule

3. Bemærk at permission er bevist undladt

4. Brug auditctl til at udskrive reglen. Bemærk hvilken rettigheder der overvåges på. Dette er pga.´-p´ muligheden blev undladt.

5. Brug chown til at give root ejerskab over directory(fra trin 1), og brug chmod til at give root fuld rettighed og alle andre skal ingen rettigheder havde.

6. Med en bruger som ikke er root, eksekver kommandoen ls <Directory path> (directory er fra trin 1)
eksekver kommandoen ausearch -i -k directory_watch_rule. Dette resulter i en log som ligner nedstående.

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
