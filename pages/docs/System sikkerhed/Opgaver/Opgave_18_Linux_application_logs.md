# Opgave beskrivelse

## Instruktioner

1. Installer apache2 web server med kommandoen sudo apt-get install apache2

2. Verificer at servicen er startet med kommandoen systemctl status apache2

3. I en browser, gå ind på url'en . Verificer at det er apache's default side der kommer frem

4. Udskriv indholdet af apache's adgangs log som findes i /var/log/apache2/access.log

5. Verificerer at den sidste log i filen viser at den sidste maskine som har tilgået web serveren er den som du brugte til at udføre trin 3

6. Eksekver kommandoen tail -f access.log

7. Udfør trin 3 et par gange igen, og verificer at der kommer en ny log entry hvergang.

</br>

# Udføresle af opgave

![Apache2 Access log](../Billeder/Apache2AccessLog.png)

</br>

# Kilder og brugbar links 
