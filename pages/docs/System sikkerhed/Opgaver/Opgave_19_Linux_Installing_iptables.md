# Opgave beskrivelse

## Instruktioner

1. Installer iptables med kommandoen sudo apt install iptables

2. Udskriv version nummeret med kommandoen sudo iptables -V

3. Installer iptables-persistent med kommandoen sudo apt install iptables-persistent

4. Udskriv alle firewall regler(Regel kæden) med kommandoen sudo iptables -L

5. Reflekter over hvad der menes med firewall regler, samt hvad der menes med "regel kæden".

</br>

# Udføresle af opgave

![Ubuntu Host firewall regler](../Billeder/HostFirewallRegler.png)

Med regel kæde menes der hvilken for for kald det er i kæden reglen handler om, så om det er ud, ind eller videre.

</br>

# Kilder og brugbar links 
