# Opgave beskrivelse

## Instruktioner

1. Tillad forbindelser fra loopback interfacet med kommandoen sudo iptables -I INPUT 1 -i lo -j ACCEPT

</br>

# Udføresle af opgave

![Firewall regel accept alt ikke relateret](../Billeder/FirewallRegelAcceptAlt.png)

</br>

# Kilder og brugbar links 
