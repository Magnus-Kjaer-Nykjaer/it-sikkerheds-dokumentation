# Opgave beskrivelse

## Instruktioner

1. Åben filen /etc/logrotate.conf

2. Sæt log rotationen til daglig rotation

3. Sæt antallet af backlogs til 8 uger.

</br>

# Udføresle af opgave

![Før rettelse](../Billeder/FoerRettelse.png)

Var nød til at køre nano kommandoen med sudo for at jeg havde rettigheder til at rette i filen

![Efter rettelse](../Billeder/EfterRettelse.png)

</br>

# Kilder og brugbar links 
