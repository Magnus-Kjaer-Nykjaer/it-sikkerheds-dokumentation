# Opgave beskrivelse

## Instruktioner

1. Vælg agents i Wazuh dashboard menuen.

2. Vælg Ubuntu som OS

3. Vælg ubuntu 15+ som OS

4. Vælg x86_64 som arkitektur

5. Indtast ip adressen til Wazuh server VM'en

6. Tast Target-host-1 som navnet på agenten, og vælg gruppen default

7. Kopier kommandoen fra Install and enroll og Paste kommandoen ind i det VM OS hvor agenten skal overvåge

8. Eksekver kommandoerne vist i Start the agent

9. Eksekver kommandoen systemctl status wazuh-agent og bekræft at agenten er aktiv

10. Bekræft i Wazuh dashboard at agent er forbundet og er aktiv

</br>

# Udføresle af opgave

    xubunutuklient@xubunutuklient-virtual-machine:~$ curl -so wazuh-agent.deb https://packages.wazuh.com/4.x/apt/pool/main/w/wazuh-agent/wazuh-agent_4.4.1-1_amd64.deb && sudo WAZUH_MANAGER='192.168.17.139' WAZUH_AGENT_GROUP='default' WAZUH_AGENT_NAME='Target-host-1' dpkg -i ./wazuh-agent.deb
    Selecting previously unselected package wazuh-agent.
    (Reading database ... 193324 files and directories currently installed.)
    Preparing to unpack ./wazuh-agent.deb ...
    Unpacking wazuh-agent (4.4.1-1) ...
    Setting up wazuh-agent (4.4.1-1) ...
    xubunutuklient@xubunutuklient-virtual-machine:~$ sudo systemctl daemon-reload
    sudo systemctl enable wazuh-agent
    sudo systemctl start wazuh-agent
    Synchronizing state of wazuh-agent.service with SysV service script with /lib/systemd/systemd-sysv-install.
    Executing: /lib/systemd/systemd-sysv-install enable wazuh-agent
    Created symlink /etc/systemd/system/multi-user.target.wants/wazuh-agent.service → /lib/systemd/system/wazuh-agent.service.
    xubunutuklient@xubunutuklient-virtual-machine:~$ sudo systemctl daemon-reload
    xubunutuklient@xubunutuklient-virtual-machine:~$ sudo systemctl enable wazuh-agent
    Synchronizing state of wazuh-agent.service with SysV service script with /lib/systemd/systemd-sysv-install.
    Executing: /lib/systemd/systemd-sysv-install enable wazuh-agent
    xubunutuklient@xubunutuklient-virtual-machine:~$ sudo systemctl start wazuh-agent
    xubunutuklient@xubunutuklient-virtual-machine:~$ systemctl status wazuh-agent
    ● wazuh-agent.service - Wazuh agent
        Loaded: loaded (/lib/systemd/system/wazuh-agent.service; enabled; vendor preset: enabled)
        Active: active (running) since Tue 2023-04-25 11:11:02 CEST; 1min 37s ago
        Tasks: 31 (limit: 4584)
        Memory: 331.4M
            CPU: 31.006s
        CGroup: /system.slice/wazuh-agent.service
                ├─37785 /var/ossec/bin/wazuh-execd
                ├─37860 /var/ossec/bin/wazuh-agentd
                ├─37872 /var/ossec/bin/wazuh-syscheckd
                ├─37884 /var/ossec/bin/wazuh-logcollector
                └─37898 /var/ossec/bin/wazuh-modulesd

    Apr 25 11:10:54 xubunutuklient-virtual-machine systemd[1]: Starting Wazuh agent...
    Apr 25 11:10:54 xubunutuklient-virtual-machine env[37145]: Starting Wazuh v4.4.1...
    Apr 25 11:10:56 xubunutuklient-virtual-machine env[37145]: Started wazuh-execd...
    Apr 25 11:10:57 xubunutuklient-virtual-machine env[37145]: Started wazuh-agentd...
    Apr 25 11:10:58 xubunutuklient-virtual-machine env[37145]: Started wazuh-syscheckd...
    Apr 25 11:10:59 xubunutuklient-virtual-machine env[37145]: Started wazuh-logcollector...
    Apr 25 11:11:00 xubunutuklient-virtual-machine env[37145]: Started wazuh-modulesd...
    Apr 25 11:11:02 xubunutuklient-virtual-machine env[37145]: Completed.
    Apr 25 11:11:02 xubunutuklient-virtual-machine systemd[1]: Started Wazuh agent.


</br>

# Kilder og brugbar links 
