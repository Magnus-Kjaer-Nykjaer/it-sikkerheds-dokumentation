# Opgave beskrivelse

1. Eksekver kommandoen apt --help.
2. via apt --help skal du nu finde ud af hvordan du udskriver en liste til konsolen, som viser alle installeret pakker.
3. Eksekver kommandoen apt -h.
4. Eksekver kommandoen man apt og scroll ned i bunden af konsol outputtet(pgdwn)
5. Eksekver kommandoen man ls og scroll ned i bunden af konsol outputtet
6. Eksekver kommandoen man man og skim outputtet, hvilken information kan du finde der?
7. Eksekver kommandoen help

</br>

# Udføresle af opgave

1. apt --help    -- Printer ud hvad apt er og giver en god forklaring om hvordan der virker.

2. apt list     -- viser aller installeret filer.

3. apt -h       -- Samme som help

4. man apt      -- Skriver manualen ud på apt.

5. man ls       -- Skriver manualen ud på ls.

6. man man      -- Skriver manualen ud på man.

7. help         -- Eksistere ikke.

</br>

# Kilder og brugbar links 