# Opgave beskrivelse

### Filer tilknyttet bruger systemet - opbevaring af bruger kontoer
I linux er der som udgangspunkt to filer som er intressant ift. bruger systemet. Den ene er filen passwd. I de fleste distributioner kan denne findes i stien /etc/passwd. passwd indeholder alle informationer om bruger kontoer(undtaget passwords).

I denne opgave skal du udforske passwd filen.

1. Se rettighederne for passwd med kommandoen ls <filenavn> -al.
2. Overvej om rettighederne som udgangspunkt ser hensigtmæssige ud? og hvorfor der er de rettigheder som der er.
3. Udskriv filens indhold Hvis du har glemt hvordan så kig i dit cheat sheet. øvelsen blev udført i opgave 7
4. Samhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede)

        Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell


### Filer tilknyttet bruger systemet - passwords
Filen shadow opbevare passwords til alle bruger kontoer. Alle passwords i shadow er hashes, som sikring mod udvekommende adgang til bruger kontoer.

I opgaven skal du udforske filen shadow.

1. udskriver rettighederne for filen shadow
2. Overvej rettighederne i samhold med Privilege of least princippet.
3. Udskriv filens indhold.
4. Samhold filens indhold med nedstående format.

        :brugernavn:password(hashet):Sidste ændring af password:Minimum dage ind password skift:Maksimum dage ind password skift:Varslings tid for udløbet password:Antal dage med udløbet password inden dekativering af konto:konto udløbs dato

### Sårbarhed ved svage passwords.
Tiltrods for at yescrypt som udgangspunkt er en stærk algoritme. Så yder hashet kun begrænset beskyttelse til svage kodeord. Dette skal der eksperimenteres med i denne opgave. I de følgende trin skal der oprettes en bruger med et svagt kodeord. Herefter skal vi trække kodeord hashet ud fra shadow filen, og forsøg genskabe passwordet ud fra hash værdien.

Alle kommandoer skal eksekveres mens du er i dit hjemme directory
1. installer værktøjer john-the-ripper med kommandoen <code>sudo apt install john</code>
2. Her efter skal du downloade en wordlist kaldet rockyou med følgende kommando <code>wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt</code>.
3. Opret nu en bruger ved navn misternaiv og giv ham passwordet password
4. Hent nu det hashet kodeord for misternaiv med følgende kommando <code>sudo cat /etc/shadow | grep misternaiv > passwordhash.txt</code>
5. udskriv indholdet af filen passwordhash.txt, og bemærk hvilken krypterings algoritme der er brugt.
6. Eksekver nu kommandoen <code>john -wordlist=rockyou.txt passwordhash.txt </code>.
7. Kommandoen resulter i No password loaded. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.
8. Eksekver nu kommandoen: <code>john --format=crypt -wordlist=rockyou.txt passwordhash.txt</code>.
format fortæller john the ripper hvilken type algoritme det drejer sig om
9. Resultat skulle gerne være at du nu kan se kodeordet, som vist på billede nedenunder.

</br>

# Udføresle af opgave

### Opbevaring af bruger kontoer



</br>

# Kilder og brugbar links 