# Opgave beskrivelse

## Instruktioner

1. Eksekver kommandoen sudo iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

2. Udskriv reglen listen.

</br>

# Udføresle af opgave

![Firewall regel accept all relateret trafik](../Billeder/FirewallRegelAcceptAllRelateretTrafik.png)

</br>

# Kilder og brugbar links 
