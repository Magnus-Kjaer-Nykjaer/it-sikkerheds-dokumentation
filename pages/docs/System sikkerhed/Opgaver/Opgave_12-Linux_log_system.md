# Opgave beskrivelse

### Instruktioner

1. Udskriv indholdet af syslog filen

2. Studer log formattet, og skriv et "generisk" fromat ind i dit Linux cheat sheet.

3. Noter hvilken tids zone loggen bruger til tidsstemple, er det UTC?

#### Authentication log

1. Udskriv indholder af auth.log

2. Skift bruger til f.eks. root

3. Udskriv indholder af auth.log igen, og bemærk de nye rækker

4. Skift bruger til din primær bruger igen (som naturligvis ikke er root)

5. Udskriv indholder af auth.log igen, og bemærk de nye rækker.

</br>

# Udføresle af opgave

### Syslog
![Cat syslog](../Billeder/Syslog.png)

Har ikke nogen idé om hvilket format det er i.

Men ellers så skriver den tids format i det samme som systemet så dansk tid så GMT +1.

### Authlog 

![Login bruger auth log](../Billeder/AuthLogsSomLoginBruger.png)

Man kan se at jeg har fejlet et par login forsøg og at jeg skriftede bruger
![root auth log](../Billeder/rootAuthLog.png)

Man kan se at jeg gik tilbage til min login bruger
![login auth log](../Billeder/TilbageTilLoginBruger.png)


</br>

# Kilder og brugbar links 