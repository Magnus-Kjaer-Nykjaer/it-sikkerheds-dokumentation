# Opgave beskrivelse

## Instruktioner

1. Udskriv regel kæden, og noter dig hvordan den ser ud. (Regel kæden er tidligere blevet udskrevet i opgave 19, trin 4)

2. Lav en regel i slutningen af regelkæden som dropper alt trafik med kommandoen sudo iptables -A INPUT -j DROP

3. Udskriv regel kæden igen, og noter forskellen fra trin 1.

</br>

# Udføresle af opgave


![Ubuntu Host firewall regler](../Billeder/HostFirewallRegler.png)

![Firwall regel drop alt trafik](../Billeder/FirewallRegelDropAltTrafik.png)

Der er blevet tilføjet en linje med DROP    all --  anywhere .
på slutningen af input reglerne. 

</br>

# Kilder og brugbar links 
