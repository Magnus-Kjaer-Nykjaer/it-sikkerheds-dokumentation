# Opgave beskrivelse

## Instruktioner

### Opsætning af Wazuh agent

Alle kommandoer eksekveres på den overvåget host
1. Opdater apt databasen med kommandoen apt update 2. Installer web serveren apache med kommandoen apt install apache2 3. Verificer at apache er aktiv med kommandoen systemctl status apache2 4. test apache med kommandoen <code> curl http://<ubuntu ip> </code> fra en anden host. 5. Konfigurer Wazuh agenten til at monitorer apache2 loggen /var/ossec/etc/ossec.conf


        <ossec_config>  
            <localfile>  
            <log_format>apache</log_format>  
            <location>/var/log/apache2/access.log</location>  
            </localfile>  
        </ossec_config>

### Udfør angreb

Alle kommandoer eksekveres på den angribende host
1. curl -XGET "http:///users/?id=SELECT+*+FROM+users"

### Wazuh dashboard

1. Gå til security events

2. Filtrer på rule.id:31103

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
