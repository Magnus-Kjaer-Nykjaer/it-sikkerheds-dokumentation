# Opgave beskrivelse

Formålet med denne opgave er at introducerer de grundlæggende linux kommandoer.

I opgaven skal der eksperimenteres med Linux CLI kommandoer i BASH shell. Fremgangsmåden er at du bliver bedt om at eksekverer en kommando, og herefter noter hvad der sker. Målet med disse øvelser er at du skal bygge et Cheat sheet med linux kommandoer i dit gitlab repositorier, og få en genral rutinering med grundlæggende Linux Bash kommandoer. Det betyder følgende for alle trin i opgaven:

1. Udfør kommandoen.

2. Observer resultatet, og noter det herefter i dit Cheat sheet Altså efter hver eksekveret kommando, skal du kunne redegøre for hvad den gjorde

</br>

# Udføresle af opgave

1. pwd -- viser hvor man er i fil stien
2. cd.. -- bevæger dig et directory tilbage
3. cd / -- går til / directoriet
4. /    -- er hoved directoriet
5. cd /etc/ca-certificates/ -- Går til ca-certificates direcoriet
6. To
7. går til root
8. 0
9. Går til ~ lokationen, home directory
10. ~ er en genvej til /home/kali 
11. Man kan se alle de base directies styrresystemet har 
12. Lavere filen helloworld.txt
13. Lavere filen helloworld.txt men den er usynlig så man skal bruge ls -a for at se den.
14. 8
15. 13 måske 14 kan ikke finde ud af om .face.icon er et directory
16. Laver en liste med directories og deres stats
17. Laver liste over alt, også de usynlige filer
18. Laver directoriet helloWorld
19. Viser en liste over alle de ting i home som er directories
20. Viser en liste over alt i home uden filter.

</br>

# Kilder og brugbar links 