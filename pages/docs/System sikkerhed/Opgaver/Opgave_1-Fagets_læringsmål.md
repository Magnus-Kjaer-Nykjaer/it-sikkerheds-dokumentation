# Opgave beskrivelse
1. Læs og refleketer over læringsmålene individuelt
   Tids estimate: 15 minutter
   Hvert team medlem skal individuelt læse og reflekter over studieordningens læringsmål for faget System sikkerhed.

2. Noter individuelt et konkret eksempel på hvert læringsmål.
   Tids estimate: 15 minutter
   Hvert team medlem noter individuelt et konkret eksempel som taler ind til hvert læringsmål. F.eks. læringsmålet Sikkerhedadministration i Database management system -> Credentials for hver database
   Som 1.Semester studerende kan dette være meget svært, men der er ikke noget rigtig og forkert, blot hvad du allerede ved, eller kan søge dig til

3. Lav en fælles forståelse af læringsmålene.
   Tids estimate: 30 minutter
   Benyt CL strukturen Møde på midten til at udarbejde en fælles formulering af konkrette eksempler som taler ind til hvert læringsmål(gerne flere konkrete eksempler for hver læringsmål).
   Punkt 1 & 2 i møde på midten strukturen blev opfyldt i forrige opgave

4. Efter pausen Noter læringsmål med konkret eksemepl i padlet.
   Tids estimate: 10 minutter
   Teamet skal nu skrive læringsmålene og de konkrette eksempler ind i den fælles padlet.

# Udføresle af opgave

Læringsmål:

Viden

Den studerende har viden om:

- Generelle governance principper / sikkerhedsprocedurer   -- GDPR og ISO.
- Væsentlige forensic processer   -- Fremgangs måder til at studere angreb.
- Relevante it-trusler   -- Angreb indenfor Systemer.
- Relevante sikkerhedsprincipper til systemsikkerhed   -- ???
- OS roller ift. sikkerhedsovervejelser   -- roller i OS, så administrator og bruger osv?
- Sikkerhedsadministration i DBMS.   -- Bruger niveauer og Credentials i database systemer.

Færdigheder

Den studerende kan:

- Udnytte modforanstaltninger til sikring af systemer   -- Firewall, honeypot osv.
- Følge et benchmark til at sikre opsætning af enhederne    -- ???
- Implementere systematisk logning og monitering af enheder   -- Eventlog samt andet monitorerings software eller hardware.
- Analysere logs for incidents og følge et revisionsspor   -- Denne er i samarbejde med foresic processer, da det handler om at kunne finde frem til hvordan angrebet forgik.
- Kan genoprette systemer efter en hændelse.    -- System resotration og andre framgangsmåder til at gennoprette systemer til drift klar stand.

Kompetencer

Den studerende kan:

- Håndtere enheder på command line-niveau    -- At kunne bevæge sig rundt uden en GUI og håndtere servere.
- Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler    -- endpoint og api sikkerhed som en helhed.
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser. -- Have nok viden om systemsikkerhed til at kunne udvælge det bedste hardware og software til et specifikt system.
- Håndtere relevante krypteringstiltag    -- Viden om kryptering og brugen af det.


# Kilder og brugbar links 
<a href="https://esdhweb.ucl.dk/D22-1980440.pdf?_ga=2.115552518.1057771612.1675433091-1226966722.1675433091">National Studieordning