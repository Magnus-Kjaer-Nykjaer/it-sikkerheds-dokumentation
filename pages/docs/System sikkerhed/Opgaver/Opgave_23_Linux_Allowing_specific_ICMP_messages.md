# Opgave beskrivelse

## Instruktioner

1. Tillad ICMP pakker af type 3 med kommandoen sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 3 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT

2. Tillad ICMP pakker af type 11 med kommandoen sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 11 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT

3. Tillad ICMP pakker af type 12 med kommandoen sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 12 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT

4. De ICMP pakker der er blevet tilladt, er hver især af en bestemt type. Undersøg hvad hver af de 3 typer betyder.

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
