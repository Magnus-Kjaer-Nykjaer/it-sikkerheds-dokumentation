# Opgave beskrivelse

## Instruktioner
### Opsætning af locate til søgning.

1. Installer locate med kommandoen sudo apt install locate

2. Opdater "Files on disk" databasen, sudo updatedb

### Dan overblik over Rsyslog logfilerne på operativ systemet.

1. Brug locate til at finde alle filer med ordet rsyslog

2. Dan dig et generelt overblik over filerne. Er der mange tilknyttet filer?

### Rsyslog konfigurations file.

1. Brug locate til at finde rsyslog filen rsyslog.conf

2. Åben filen med nano

3. I konfigurations filen, find afsnittet Set the default permissions for all log files.

4. Noter hvem der er file ejer, og hvilken gruppe log filerne er tilknyttet.

5. Udforsk de andre områder af filen.

</br>

# Udføresle af opgave

### Opsætning

Havde ingen problemer med dette.

### Overblik

Der er mange filer der har med det samme at gøre og der ligger mange rsyslog mapper rundt omkring

![Alle filer med rsyslog i](../Billeder/AlleFIlerMedrsyslog.png)

### Konfig filer

![Log file permissions](../Billeder/LogfilePermissions.png)


</br>

# Kilder og brugbar links 
