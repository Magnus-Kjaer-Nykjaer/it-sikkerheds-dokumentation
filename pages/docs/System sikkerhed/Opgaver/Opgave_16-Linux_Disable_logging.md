# Opgave beskrivelse

## Instruktioner

1. Eksekver kommandoen service rsyslog stop Efter dette vil der ikke længere blive genereret logs i operativ systemet.

2. Eksekver kommandoen service rsyslog start

</br>

# Udføresle af opgave

![service rsyslog stop](../Billeder/rsyslogStop.png)

Efter jeg havde kørt denne kommando kørte jeg start kommandoen med en sudo foran og så kørte det igen. 

</br>

# Kilder og brugbar links 
