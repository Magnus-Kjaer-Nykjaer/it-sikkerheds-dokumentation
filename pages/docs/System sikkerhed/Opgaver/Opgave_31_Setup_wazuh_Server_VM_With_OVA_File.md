# Opgave beskrivelse

## Instruktioner

### Yderlige forberedelse af Wazuh WM

1. Start den importeret VM. Her mødes du af en forspørgelse på credentials.

2. Log ind med credentials (Bemærk at tastatur indstillingerne er engelsk, så bindestregen er ikke hvor den plejer at være)

3. Sæt tastatur layout til dansk med kommandoen localectl set-keymap dk

4. Noter VM'ens ip adresse med kommandoen ip a Det er ip 4 adressen fra interfacet eth  0 som skal bruges

5. log ud med kommandoen logout (Ikke slukke)

### Afprøv Wazuh

1. I en browser på din host(lokal host) indtast url'en https:// Wazuh VM IP

2. Autentificer dig selv med brugernavnet admin og kodeordet admin (Igen default password)

</br>

# Udføresle af opgave

Wazuh tilgåes via <https://192.168.17.139/>

Den er pænt langsom til at starte op men der er sat en agent op med Ubuntu 2 vmen

</br>

# Kilder og brugbar links 
