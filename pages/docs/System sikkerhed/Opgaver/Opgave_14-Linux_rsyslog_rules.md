# Opgave beskrivelse

## Instruktioner
1. I ryslog directory, skal filen 50-default.conf findes.

2. Åben filen 50-deault.conf

3. Dan et overblik over alle de log filer som der bliver sendt beskeder til.

4. Noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen info , warn og err

</br>

# Udføresle af opgave

![50-deault.conf filen](../Billeder/50-deault-conf.png)

Mail sender til disse filer

    #mail.info                      -/var/log/mail.info
    #mail.warn                      -/var/log/mail.warn
    mail.err                        /var/log/mail.err


</br>

# Kilder og brugbar links 
