# Opgave beskrivelse

1. I Home directory, eksekver kommandoen touch minfile.txt
2. I Home directory, eksekver kommandoen cp minfile.txt kopiafminfile.txt
3. I Home directory, eksekver kommandoen mkdir minfiledir
4. I Home directory, eksekver kommandoen, mv minfile.txt minfiledir
5. I Home directory, eksekver kommandoen, rm -r minfiledir

I de næste trin skal der arbejdes med oprettelse af filer med tekst indhold, her bliver redirect operatoren introduceret (>). Operatoren tager outputet fra kommandoen på venstre side og skriver til filen på højre side.

1. I Home directory, eksekver kommandoen echo "hej verden" > hejverdenfil.txt
2. I Home directory, eksekver kommandoen cat hejverdenfil.txt
3. I Home directory, eksekver kommandoen echo "hej ny verden" > hejverdenfil.txt
4. I Home directory, eksekver kommandoen cat hejverdenfil.txt
5. I Home directory, eksekver kommandoen echo "hej endnu en ny verden" >> hejverdenfil.txt
6. I Home directory, eksekver kommandoen cat hejverdenfil.txt

I de næste trin introduceres pipe operatoren (|). Den tager outputtet fra kommandoen på venstre side, og giver det videre til kommandoen på højre side

1. I /etc/, eksekver kommandoen cat adduser.conf.
2. I /etc/, eksekver kommandoen cat adduser.conf | grep 1000.
3. I /etc/, eksekver kommandoen cat adduser.conf | grep no.
4. I /, eksekver kommandoen grep no /etc/adduser.conf
5. I /, eksekver kommandoen ls -al | grep proc
6. I /etc/, eksekver kommandoen ls -al | grep shadow

</br>

# Udføresle af opgave

1. laver filen minfile.txt
2. laver en kopi af minfile.txt der hedder kopiafminfile.txt
3. laver mappen minfiledir
4. rykker filen minefile.txt ind i mappen minfiledir
5. fjerner mappen minfiledir og alt i den

næste del

1. Skriver "hej verden" i hejverdenfil.txt
2. printer indholdet af hejverdenfil.txt ud
3. overskriver de der stod før med "hej ny verden"
4. printer indholdet af hejverdenfil.txt ud
5. tilføjer en ny linje med "hej endnu en ny verden"
6. printer indholdet af hejverdenfil.txt ud

næste del

1. fremviser indholdet i adduser.conf filen
2. fremviser indholdet i adduser.conf filen og søger filerne i etc igennem for tallet 1000.
3. fremviser indholdet i adduser.conf filen og søger filerne i etc igennem for ordet no.
4. Leder filen adduser.conf igennem for ordet no
5. viser en liste over alt og leder dem igenne for ordet proc
6. viser en liste over alt og leder dem igenne for ordet shadow

</br>

# Kilder og brugbar links 