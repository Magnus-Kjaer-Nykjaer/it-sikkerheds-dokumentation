# Opgave beskrivelse

1. I Home directory, eksekver kommandoen find
2. I Home directory, eksekver kommandoen find /etc/
3. Eksekver kommandoen sudo find /etc/ -name passwd sudo foran kommandoen betyder at kommandoen eksekveres med de rettigheder som sudo gruppen har
4. Eksekver kommandoen sudo find /etc/ -name pasSwd Husk stort S
5. Eksekver kommandoen sudo find /etc/ -iname pasSwd Husk stort S
6. Eksekver kommandoen sudo find /etc/ -name pass*

De næste kommandoer bruges til at finde filer baseret på deres byte størrelse. De to første trin bruges blot til at generer de to filer som skal findes

1. I Home directory, eksekver kommandoen truncate -s 6M filelargerthanfivemegabyte
2. I Home directory, eksekver kommandoen truncate -s 4M filelessthanfivemegabyte
3. I roden(/), eksekver kommandoen find /home -size +5M
4. I roden(/), eksekver kommandoen find /home -size -5M
5. I Home directory, opret to directories. et der hedder test og et andet som hedder test2
6. I test2, skal der oprettes en file som hedder test
7. I Home directory, eksekver kommandoen find -type f -name test
8. I Home directory, eksekver kommandoen find -type d -name test

</br>

# Udføresle af opgave

1. laver en liste af lle de ting der er i home directory
2. finder alt i /etc/ og printer en liste med det
3. Finder alle filerne i etc der har med passwd at gøre
4. Finder ikke noget
5. Finder det samme som 4.
6. Finder alle filer med pass i navnet

Anden del

1. virker ikke
2. virker ikke
3. virker ikke
4. Finder alle filer mindre end 5 megabyes 
- finder test filen
- Finder test mappen

</br>

# Kilder og brugbar links 