# Opgave beskrivelse

## Instruktioner

1. Installer hashalot med apt.

2. Opret en file som indeholder teksten Hej med dig.

3. Lav en checksum af filen med kommandoen sha256sum <path to file>.

4. Lav endnu en checksum af filen, og verificer at check summen er den samme.

5. Tilføj et f til teksten i filen.

6. Lav igen en checksum af filen.

7. Verificer at checksum nu er en helt anden.

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
