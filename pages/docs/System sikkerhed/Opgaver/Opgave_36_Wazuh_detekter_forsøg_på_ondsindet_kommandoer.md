# Opgave beskrivelse

## Instruktioner

### Opsætning af Wazuh agent

1. tilføj følgende to audit regler i bunden af Auditd's regel file /etc/audit/audit.rules:

        -a exit,always -F auid=1000 -F egid!=994 -F auid!=-1 -F arch=b32 -S execve -k audit-wazuh-c

        -a exit,always -F auid=1000 -F egid!=994 -F auid!=-1 -F arch=b64 -S execve -k audit-wazuh-c

2. Genstart Auditd med kommandoen sudo auditctl -R /etc/audit/audit.rules

3. Kontroller at reglerne er indlæst med kommandoen sudo auditctl -l

4. Under kommentaren <code> <!-- Log analysis --> </code> i Wazuh agenten's konfigurations file, tilføj følgende:


        <localfile>  
        <log_format>audit</log_format>  
        <location>/var/log/audit/audit.log</location>  
        </localfile>

5. Genstart Wazuh agenten med kommandoen sudo systemctl restart wazuh-agent

### Opsætning af Wazuh agent

1. Åben CLI'en på Wazuh serveren. Serveren er CentOs, så nogle af værktøjerne er lidt anderledes i dem vi hidtil har arbejdet med

2. CentOs bruger Yum som package manager (Ubuntu bruger apt). opdater package managerens database med kommandoen sudo yum update -y

3. Installer nano med kommandoen sudo yum update -y

4. Opret en ny file ved navn suspicious-programs i directoriet /var/ossec/etc/lists/

5. Åben filen suspicious-programs med nano, og tilføj følgende linjer:

        ncat:yellow  
        nc:red  
        tcpdump:orange  

6. Tilføj blokken <list>etc/lists/suspicious-programs</list> til konfigurations filen /var/ossec/etc/ossec.conf i blokken ruleset for at generer et regelsæt suspicous 

7. Opret en regel ved at tilføje følgende blok til filen /var/ossec/etc/rules/local_rules.xml


        <group name="audit">
            <rule id="100210" level="12">
                <if_sid>80792</if_sid>
            <list field="audit.command" lookup="match_key_value" check_value="red">etc/lists/suspicious-programs</list>
                <description>Audit: Highly Suspicious Command executed: $(audit.exe)</description>
                <group>audit_command,</group>
            </rule>
        </group>

8. genstart Wazuh manager med kommandoen sudo systemctl restart wazuh-manager

### Udfør angreb.

1. På den overvåget host, eksekver kommandoen sudo apt install netcat

2. Eksekver herefter kommandoen nc -v

3. Gå ind under Security events på Wazuh dashboard og filterer med data.audit.command:nc

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
