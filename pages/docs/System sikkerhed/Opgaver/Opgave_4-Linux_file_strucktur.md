# Opgave beskrivelse

File strukteren i Linux ser ud som følger:

    /
    |_bin 
    |_boot
    |_dev
    |_etc
    |_home
    |_media
    |_lib
    |_mnt
    |_misc
    |_proc
    |_opt
    |_sbin
    |_root
    |_tmp
    |_tmp
    |_usr
    |_var

Hver directory har pr. konvention et specifikt formål, altså hvad indeholder de. Det skal der arbejdes med i denne opgave, først individuelt og siden i teamet.

Individuelt undersøg og noter i linux cheats sheet hvad det er for nogle filer som er tiltænkt hver enkelt directory. Tids estimat: 25 minutter

I teamet Teamet skal nu samligne resultater, og lave en fælles konklusion af hvad hvert enkelt directory er tiltænkt af indeholde. Tids estimat: 10 minutter

</br>

# Udføresle af opgave

- /   -- er stien til root directoriet
- bin -- er der hvor alle kommandoerne ligger
- boot -- Der hvor boot loader filer ligger så som kernels
- dev  -- Er der hvor device file ligger så som log mappen terminal filer og andet.
- etc  -- config filer, passwords, python installation og andet der ikke rigtigt giver mening at have andre steder.
- home -- home directoriet, så der hvor alle brugerens ting ligger
- media -- Er den mappe der indeholder cdrom directoriesne
- lib   -- Libraries som bruges af tingene i bin
- mnt   -- Indeholder . og .. directories og andre midlertidige filer
- misc  -- findes ikke
- proc  -- Indeholder drivers og andet info til kernels og processer.
- opt   -- En mappe der indeholder microsoft mappe og andre add-on ting.
- sbin  -- Filer og mapper til styrresystemet
- root  -- Home directory for root brugeren
- tmp   -- Bruges til opbevaring af middlertidig filer
- usr   -- Bruges til bruger data og applikation data imellem brugere
- var   -- Variable filer: filer, hvis indhold forventes at ændre sig konstant under normal drift af systemet, såsom logfiler og andet.

</br>

# Kilder og brugbar links 
<https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard>