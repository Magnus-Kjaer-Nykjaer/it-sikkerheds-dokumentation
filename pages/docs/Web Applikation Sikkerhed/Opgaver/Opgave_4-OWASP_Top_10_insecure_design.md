## Information
I denne øvelse skal du arbejde med <a href="https://owasp.org/Top10/A04_2021-Insecure_Design/">OWASP top 10 number 4 - Insecure design</a>. Inde på siden(og dertil hørende links), skal du forsøge at finde svar på de 3 nedstående spørgsmål.

## Instruktioner

1. Hvad bliver overordnet beskrevet i afsnittet Requirements and Resource management, og kender du en tilgang som dækker dele af det beskrevet?

- CIA virksomheder skal beskytte den data de indsamler. Så skal virksomheden overveje hvor stor sandsinlighed der er for at netop de vil blive angrabet, og lave en budget så der er mulighed for at design og test ens aplikation


2. Hvad menes der med Secure design?
- Få overbevidst ledelsen om sikkerhed er vigtig
- Få dækkert CIA ind

3. I afsnittet Secure design bliver der nævnt en type modellering som bør indgår i en udviklings process. Hvilken type modellering er der her tale om?