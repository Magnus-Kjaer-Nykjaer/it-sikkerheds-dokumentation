User1
mkn@eoz.dk|PassWord#4
37.7775112,-122.3970889
3e6f75f3-5efe-49a5-9135-b3c1db33f5da
Vin:0BNYK23UHXY138356

User2
test@test.test|Test1!Test
37.4850772,-122.1504711
c9d79ff9-91d4-4bc8-bab7-57d3e15770a8
Vin:4QNBM60QAYH597942

Øvelse 1 - BOLA
Jeg gik ind med burp suit og fandt hver users kordinat lokation og indsatte det som den anden users lokation er.
Lokationen er den første request der bliver lavet til maps integrationen som vist på billedet.

![Users Maps lokation](../Billeder/BOLAMapsLokation.png "Users Maps lokation")

Øvelse 2 - Excessive Data exposure
Når man laver en order og vil se detalierne på den given order bliver den givne order printet ud i urlen med id.
<http://localhost:8888/orders?order_id=3>
Dette id kan man så skrifte ud og tilgå andre brugeres ordre.

Øvelse 3 - brug 1 og 2
I Burp Suit er der en http history man kan bruge til at se responsen af http kaldene.
I denne history kan man se brugernes vehicle id under community sidens http response.
![Users vehicle Id](../Billeder/HttpHistoryVehicleId.png "Users vehicle Id")

Øvelse 4 - 


Øvelse 5 - Get access to other customers mechanic reports.
Ved at gå ind i http history kan vi få en direkte URL til en bestemt users mechanic report, denne URL kan man så indsætte i browseren og give den en JWT token som autherizer kaldet.
![Bola mechanic](../Billeder/BOLAMechanic.png "Bola mechanic")