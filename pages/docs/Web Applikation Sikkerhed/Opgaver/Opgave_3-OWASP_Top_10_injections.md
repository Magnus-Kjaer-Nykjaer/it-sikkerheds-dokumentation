# Information

I denne øvelse skal du arbejde med <a href="https://owasp.org/Top10/A03_2021-Injection/">OWASP top 10 number 3 - injection</a>. Inde på siden(og dertil hørende links), skal du forsøge at finde svar på de 3 nedstående spørgsmål.

# Instruktioner

1. Læs Beskrivelsen af injection sårbarheden, og beskriv med dine egne ord hvad denne type sårbarhed dækker over.

- Injection sårbarheder handler om at brugere kan se data de ikke burde kunne se. Disse data bliver ofte vist hvis udviklerene af programmet ikke har lavet det ordentligt og kommer til at sætte det op på en forkert måde.

2. I afsnittet how to prevent bliver en af forstaltningerne mod injection anbgreb, beskrevet som positive Server side input validation , hvad betyder det?

- Det betyder at man ikke stoller inputtet som en bruger sender til serveren og man derfor sørger for at validere det input som brugeren kommer med.

3. I afsnittet List of mapped CWEs fremgår CWE-20, hvad beskriver denne CWE?

- Den snakker om at ikke ordentligt input validering, hvilket kan føre til usikker brug af brugerens input.