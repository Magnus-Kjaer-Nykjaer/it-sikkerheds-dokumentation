## Information

Formålet med denne øvelse er at gruppen laver en opsamling på den viden hvert medlem har opnået gennem forberedelsen til idag.
Hvis man blot læser, arbejder eller ser noget en enkelt gang er det ofte ikke nok til at opnå en forståelse for emnet. Hvis man taler om emnet med ligesindet efter man har læst noget, vil der typisk opnåes en endnu større forståelse for emnet. Herudover træner det brugen af fag termer og terminologier.

## Instruktioner

Gruppen skal sammen afklarer følgende spørgsmål: - Hvad er et HTTP request?
- hvad er en HTTP metode?
- Hvor kan man finde en oversigt over HTTP metoders semantiske betydning?
- Hvad er et REST api?
- Hvad betyder at et REST api er stateless?


<https://docs.google.com/document/d/1xb4ZCmOmdU711_YDfCoFxoopmWBSMyEx6GZuq0Pzslk/edit?pli=1>