## Instruktioner

- Udfør øvelsen <a href="https://github.com/mesn1985/HackerLab/blob/main/crAPI/6_Basic_Token_Attacks.md#getting-to-know-the-jwt-token">Getting to know the JWT token</a>

- Udfør øvelsen <a href="https://github.com/mesn1985/HackerLab/blob/main/crAPI/6_Basic_Token_Attacks.md#getting-to-know-the-jwt-token">Manipulating the payload</a>

- Udfør øvelsen <a href="https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/6_Basic_Token_Attacks.md#manipulating-and-abusing-unsigned-tokens">Abusing unsigned tokens Juice shop</a>



![Alt text](image.png)


![Alt text](image-1.png)


![Alt text](image-2.png)