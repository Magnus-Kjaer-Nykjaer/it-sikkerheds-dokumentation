# Information

Kildekoden til en applikation i en offentligt repository på Github, udstiller uhensigtsmæssigt et stykke information for meget. Kan du finde den information?

Link til repo: https://github.com/mesn1985/DotNetApplicationWithToMuchInformation


AWS secretname står i plain tekst

![Key Secret](../Billeder/Key_Secret.png)


Der ligger en Api Token i plain tekst i appsettings.json

![Apitoken](../Billeder/Apitoken.png)