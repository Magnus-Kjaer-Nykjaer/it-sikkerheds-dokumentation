## Information

I denne øvelse skal du besvare en række spørgsmål om <a href="https://github.com/OWASP/ASVS/tree/v4.0.3#latest-stable-version---403">OWASP Application Security Verification Standard</a>. (Du kan finde den seneste udgave ved at klikke på linket)

## Instruktioner

1. I hvilket afsnit kan man finde et overblik over hvad man bør sikre sig ift. input validering?

- V5 Validation, Sanitization and Encoding

2. Hvad beskrives der i afsnit V1 Architecture, Design and threat modelling.

- Hvordan man implementere sikkerheds arkitekture i software design.

3. I nogen afsnit referes der til NIST , hvad er NIST?

- National Institute of Standards and Technology