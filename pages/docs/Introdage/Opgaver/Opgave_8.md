# Opgave beskrivelse

Opgave 8 del 9

Oprettelse af Dokumentations hjemmeside

# Udføresle af opgave

Det første jeg startede med var at oprette selve projeket og give den nogle standard filer for at kunne køre, disse filer kom fra den mkdocs tutorial som vi kørte igennem. Da selve grund projektet var oprette, gik jeg ind i GitLab og oprettede en bruger for at lave et repository som jeg kunne uploade mit projekt til. Denne kommunikation mellem min pc og og mit GitLab repository skere via en SSH key som jeg også oprettede og fik til at virke. Lige så snart jeg havde formået at upload mit projekt til mit repository satte jeg mig for at implementer CI/CD i repoitoriet, dette gjorde jeg ved hjælp af noget YAML kode som vi fik udleveret til at lave en pipeline til brug af CI/CD. Da jeg havde valideret min konto køret jeg min pipeline igennem for at se at den passed og alt virkede som det skulle. 

# Problemer jeg stødte på i løbet af opgaven

Problemer:

    1. Forbindelses problemer mellem pc og repository

    2. Problemer med at få auto connect feature til at virke i Git Bash

    3. Problemer med pipeline

Løsninger:

    1. Første gang jeg jeg prøvede at connect mit repository til min SSH key kom jeg til at gøre det i en forkert række følge hvilket gjorde at den ikke kune få forbindelse til GitLab, så jeg var nød til at gøre det forfra.

    2. Gav op på at løse dette problem da det ikke er nødvindigt at løse det.

    3. Jeg fandt ud af at det pipeline kode vi fik udleveteret havde en syntax fejl i sig, så den rettede jeg og fik det til at virke.

# Kilder og brugbar links 

<https://ucl-pba-its.gitlab.io/exercises/intro/3_intro_opgave_gitlab_pages/#instruktioner>

<https://www.mkdocs.org/>

<https://eal-itt.gitlab.io/gitlab_daily_workflow/index.html#2>