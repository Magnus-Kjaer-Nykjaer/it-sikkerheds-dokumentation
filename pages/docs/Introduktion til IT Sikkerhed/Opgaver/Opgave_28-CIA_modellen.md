# Opgave beskrivelse

1. Læs om CIA modellen i bogen "IT-Sikkerhed i praksis, en introduktion" kapitel 2.

2. Vælg et af følgende scenarier som i vil vurdere i forhold til CIA:

    - En password manager (software)

    - En lægeklinik (virksomhed)

    - Dine data på computere og cloud (...)

    - Energileverandør (kritisk infrastruktur)


3. Vurder, prioitér scenariet i forhold til CIA modellen. Noter og begrund jeres valg og overvejelser.

4. Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)

5. Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)

6. Hver gruppe fremlægger det de er kommet frem til og alle giver feedback.


# Udføresle af opgave

### CIA modellen 

![CIA Modellen](./Billeder/CIA_Model.png "CIA Modellen")

1. Læs om CIA modellen i bogen "IT-Sikkerhed i praksis, en introduktion" kapitel 2.


2. Vælg et af følgende scenarier som i vil vurdere i forhold til CIA:
    - En password manager (software)
    - En lægeklinik (virksomhed)
    - Dine data på computere og cloud (...)
    - Energileverandør (kritisk infrastruktur)


    Lægeklinik: 

    Har rigtig meget persondata så confidentiality er hovedpunktet. 

    Integrity er også meget vigtigt da det er lægeklinikkerne der håndtere ting som medicin recepter og det er derfor vigtigt at sørger for at ingen andre skal kunne pille ved det.

    Availability er vigtigt fordi lægeklinikken ofte har med patienter med livstruende sygdomme som kræver medicin for at leve og det er derfor vigtigt at patienterne har adgang til deres recepter. Det er også vigtigt at lægeklinikken er tilgængelig og muligt at bestille tider ved, da det ofte kan have indflydelse på hvor længe patienter går med en farlig sygdom.


3. Vurder, prioitér scenariet i forhold til CIA modellen. Noter og begrund jeres valg og overvejelser.
    - Availability
    - Confidentiality
    - Integrity

4. Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)

    DDos og Ransomware/phishing.  Da det er de former for angreb der har mest indflydelse på tilgængeligheden af klinikken.

5. Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)

    Awareness kampagner for at folk ikke falder for phishing angreb.

    Kryptering/hasing af data

    Adgangskontrol til lægeklinikkens system

    Logging f.eks når der sker ændringer i lægejournaler eller når man tilgår noget data

    Skalerbare systemer der hjælper på DDos angreb.

6. Hver gruppe fremlægger det de er kommet frem til og alle giver feedback.


# Kilder og brugbar links 
