# Opgave beskrivelse

1. Rediger xubuntu1 til midlertidigt at bruge nat i vm settings, fjern også ip4 adressen i xubuntus netværksindstillinger og sæt forbindelsen til dhcp i stedet for manual.
Du skal muligvis disable og enable ethernet forbindelsen på maskinen. Ping 8.8.8.8 for at se om du har internet forbindelse

2. Opdater apt listen med sudo apt-get update

3. Installer Damn Vulnerable Web Application (DVWA) på en af xubuntu maskinerne https://github.com/digininja/DVWA ved at følge denne guide

4. Når DVWA virker så ret xubuntu maskinens netværks konfiguration tilbage til tidligere konfiguration og sæt vm netværksinterfacet tilbage til vmnet.
Du skal muligvis disable og enable ethernet forbindelsen på maskinen. Ping noget på netværket for at se om din forbindelse virker. Ping 8.8.8.8 for at verificere at du ikke har internet adgang.

5. Gå til din kali maskine og naviger til DVMA (erstat 127.0.0.1 med dvma maskinens ip)

6. Kig på de forkellige øvelser på https://github.com/mrudnitsky/dvwa-guide-2019 og prøv et par stykker af dem.
Husk at skrive dine erfaringer ned når du laver øvelserne i dvwa, det hjælper ved fejlfinding og fordrer dybdelæring.

# Udføresle af opgave

Jeg startede ud med at tage en af mine XUbuntu maskiner og installer de DVWA på.

Jeg fulgte github guiden samt youtube videon til hvordan det hele skulle gøres.

For at få den til at starte siden skal du køre kommandoen

    sudo service apache2 start

Når den er oppe at køre skal man bare gå ind på http://localhost/DVWA eller http://192.168.11.2/DVWA

## Opgave forsøg
Jeg forsøgte lidt forskeligt

### Brute force
Jeg startede med at få wordlist til at give mig rockyou.txt filen og der efter fik jeg hydra til at prøve alle de forkselige logins i rockyou.txt filen

For at hydra kan finde rockyou filen skal man cd ind i det directory hvor den ligger og så kan man bede hydra om at begynde.

Den hydra commando jeg kørte er som følge:

    hydra 192.168.11.2 http-form-post "/DVWA/vulnerabilities/brute/:username=^USER^&password=^PASS^&Login=submit:Username and/or password incorrect." -L rockyou.txt -P rockyou.txt


# Kilder og brugbar links 
