# Opgave beskrivelse

1. Lav 2 xubuntu VM maskiner i vmware, brug xubuntu 20.04 LTS som image.

2. Xubuntu image download <https://xubuntu.org/download/>

3. Hjælp til at installere en ny vm <https://kb.vmware.com/s/article/1018415>

4. Konfigurer de 2 linux maskiner's netværk ifølge netværks diagrammet

5. Åbn vm indstillinger for hver xubuntu maskine. Sæt xubuntu 1 til vmnet1 og xubuntu 2 til vmnet2 (hvis du ikke har vmnet2 så lav det i vmware virtual network editor)

# Udføresle af opgave

Credentials for Maskinerne

XUbuntu server

Username: xubunutuklient

Password: XUbunutuKlient
<br><br>
Først gik jeg ind og downloaded iso file for XUbuntu LTS 22.04.

Når de var downloaded gik jeg ind i VMWare og lavede en ny virtual maskine.

Jeg valgte at køre installationen igennem med standard indstillinger.