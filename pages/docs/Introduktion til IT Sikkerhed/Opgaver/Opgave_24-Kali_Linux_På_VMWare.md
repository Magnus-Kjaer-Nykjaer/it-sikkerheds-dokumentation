# Opgave beskrivelse

Instruktioner
1. Download VMWare Workstation Pro Trial version https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html

2. Installer VMware workstation pro

3. Download Kali Linux til VMware https://www.kali.org/get-kali/#kali-virtual-machines

4. Installer Kali Linux ved hjælp af denne guide https://www.kali.org/docs/virtualization/import-premade-vmware/

5. Start Kali Linux VM og log ind, Default credentials er kali/kali

6. Åbn en terminal og ping 8.8.8.8 for at sikre at du har forbindelse til internettet

# Udføresle af opgave

Jeg startede med at installere WMWare Workstation Pro og aktivere det i Trial versionen imens jeg ventede på at få tilsendt license nøglen.
Når jeg har det installeret downloader jeg Kali Linux og extractor filerene så jeg kan bruge dem i WMWare.
Når det er extracted får jeg WMWare til at installer og åbne Kali, når det er åbent går jeg ind i terminalen og pinger den given ip adresse.

# Kilder og brugbar links 
