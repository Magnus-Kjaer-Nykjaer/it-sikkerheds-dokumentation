# Opgave beskrivelse

Instruktioner
1. Lav et dokument som du bruger til at få et overblik over fagets læringsmål, uge for uge.
For hver uge laver du en checkliste over de læringsmål der gælder for ugen samt en checkliste for de opgaver der er stillet i ugen.

2. Når du har en liste for alle uger i faget skal du vurdere hvor du selv er ifht. læringsmål og opgaver.
Her krydser du de læringsmål og opgaver af som du mener du har styr på, det vil sige du ender med et overblik over læringsmål og opgaver som du skal genbesøge i din repetition.
3. Afsæt den nødvendige tid og lav opgaver + genbesøg materialer, så du opnår viden, færdigheder og kompetencer i overenstemmelse med fagets læringsmål.

</br>

# Udføresle af opgave

Jeg har valgt at sætte x ved dem jeg er pænt sikre på og l ved dem jeg skal læse op på

# Uge 6 - Introduktion til faget og opsætning af værktøjer
    - Læringsmål:
        - [x] Den studerende kender fagets læringsmål og fagets semester indhold: 
        - [x] Den studerende kan vurdere egne faglige evner i relation til fagets læringsmål
        - [x] Den studerende har viden om virtuelle maskiner 
        - [x] Den studerende har viden om VPN opsætning
    - Opgaver:
        - [x] Opgave 4: Studieordning og læringsmål 
        - [x] Opgave 24: Kali Linux på vmware
        - [x] Opgave 25: TryHackMe
        - [x] THM: Learning cyber security
        - [x] THM: Linux Fundamentals Part 1
        - [x] THM: Linux Fundamentals Part 2
        - [x] THM: Linux Fundamentals Part 3

# Uge 7 - GIT, grundlæggende netværk 
    - Læringsmål:
        - [x] Den studerende kender CIA modellen
        - [x] Den studerende ved hvad OSI modellen og dens lag er
        - [l] Den studerende kender TCP/IP modellen, dens lag og hvordan 3 way handshake fungerer
        - [l] Den studerende kan anvende ping, hvilken protokol ping anvender og relevante ping parametre
        - [x] Den studerende ved hvad traceroute kan anvendes til og hvordan det bruges
        - [x] Den studerende ved hvad whois er og kan anvende det via CLI
        - [x] Den studerende har viden om hvordan DNS systemet er opbygget og hvordan dig kan bruges til at forespørge DNS servere
        - [x] Den studerende ved hvad et netværk er herunder hvad IP adresser og MAC adresser er
        - [x] Den studerende kender til MAC spoofing
        - [ ] Den studerende kender de forskellige netværks topologier og deres svagheder
        - [ ] Den studerende ved hvad hhv. en switch og en router anvendes til
        - [ ] Den studerende har viden om ARP protokollen
        - [ ] Den studerende kender til DHCP og hvordan protokollen fungerer
    - Opgaver:
        - [x] Opgave 28: CIA Modellen  
        - [x] Opgave 2: Git/gitlab og ssh nøgler
        - [x] Opgave 3: Gitlab pages
        - [x] Opgave 1: Markdown
        - [x] Opgave 98: Dokumentation af opgaver
        - [x] THM: Introductory Networking
        - [x] THM: What is Networking?
        - [x] THM: Intro to LAN
        

# Uge 8 - Sikkerhed i netværksprotokoller
    - Læringsmål:
        - [x] Mestre forskellige netværksanalyse tools
        - [x] Opsætte et simpelt netværk 

    - Opgaver: 
        - [x] Opgave 26 - Wireshark analyse
        - [x] Opgave 16 - Simpelt netværk: Opsætning af virtuelle maskiner i vmware
        - [x] Opgave 17 - Simpelt netværk: Opsætning af vsrx virtuel router
        - [x] Opgave 18 - Simpelt netværk: Seriel forbindelse til vsrx virtuel router
        - [x] Opgave 19 - Simpelt netværk: konfigurer vsrx password og navn
        - [x] Opgave 20 - Simpelt netværk: konfigurer vsrx routing
        - [l] THM: Nmap
        - [l] THM: Wireshark: The Basics

# Uge 9 - Netværksanalyse
    - Læringsmål:
        - [l] Grundlæggende netværksprotokoller
        - [x] Sikkerhedsniveau i de mest anvendte netværksprotokoller 
        - [x] Opsætte et simpelt netværk
    - Opgaver:
        - [x] Opgave 21 - Simpelt netværk: tilføj interface til vsrx
        - [x] Opgave 22 - Simpelt netværk: Kali linux vm på netværket
        - [x] Opgave 23 - Simpelt netværk: Damn Vulnerable Web Application (DVWA)        
        - [l] THM: Juicy Details

# Uge 10 - Python programmering
    - Læringsmål:
        - [x] Grundlæggende programmeringsprincipper
        - [x] Anvende primitive og abstrakte datatyper
        - [x] Læse andres scripts samt gennemskue og ændre i dem
    - Opgaver:
        - [l] Opgave 8 - Python recap 
        - [l] THM: Python basics

# Uge 11 - Scripting i Bash og Powershell
    - Læringsmål:
        - [x] Grundlæggende programmeringsprincipper
        - [x] Anvende primitive og abstrakte datatyper
        - [x] Læse andres scripts samt gennemskue og ændre i dem
        - [x] Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv
    - Opgaver:
        - [x] Opgave 10 - bash scripting basics
        - [l] Opgave 11 - Forstå bash script + tilrette script
        - [x] Opgave 14 - Powershell basics
        - [x] Opgave 15 - Powershell script
        - [l] THM: Bash Scripting

# Uge 12 - Programmer der kan bruge netværk
    - Læringsmål:
        - [ ] Konstruere simple programmer der kan bruge netværk
        - [ ] Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik
        - [ ] Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv
    - Opgaver:
        - [ ] Opgave 12 - Python netværks programmering
        - [ ] THM: HTTP in detail
        - [ ] THM: TShark

# Uge 13 - Programmer der kan bruge SQL databaser
    - Læringsmål:
        - [X] Konstruere simple programmer der bruger SQL databaser
        - [ ] Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv
    - Opgaver:
        - [ ] Opgave 9 - Programmering med database
        - [ ] THM: SQL Injection Lab
        - [ ] THM: SQLMAP

</br>

# Kilder og brugbar links 
