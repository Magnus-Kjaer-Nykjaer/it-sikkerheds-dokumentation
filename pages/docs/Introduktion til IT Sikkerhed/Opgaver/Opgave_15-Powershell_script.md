# Opgave beskrivelse

Instruktioner
1. Installer nmap i windows, kan findes her https://nmap.org/

2. Omskriv nmapscan scriptet fra sidste uge til pseudo kode som alle i gruppen forstår.

3. Omskriv bash scriptet til et powershell script.

![Bash Script](../Opgaver/Billeder/BashScript.png)

</br>

# Udføresle af opgave


        !/bin/bash      Siger at det er en bash fil

        RED='\033[0;31m'            Bestemmer den røde farve
        WHITE='\033[0;38m'          Bestemmer den hvide farve
        NC='\033[0m'                Bestemmer ingen farve

        echo -e "${RED} RUN THUS SCRIPT AS SUDO USER${WHITE}"   Skirver det ud som står i den -e flaget giver den mulighed for at have interpretation, så ting som ${RED} er interpretation.

        if [ $# -eq 1]; then        Venter til $# er ligemed 1 og hvis den er 1 så skriver den target ipen ud
            echo -e "Target ${RED}IP:$1${WHITE}"
        else                        Ellers skriver den at det er forkert og lukker scriptet ned
            echo "Yor command line is incorrect"
            echo -e "Usage example: ${RED} sudo ./nmapScans.sh 10.10.10.121 ${WHITE}"
            exit 1
        fi                          Gør det der er under tilsidst


        DATE='date +%Y-%m-%d'       Giver den nuværende dato
        filename=$1_$DATE           Giver fil navnet den 1 og den nuværende dato

        echo -e "Running a ${RED} quick scan ${WHITE}..."       
        nmap -T4 -F $1 > qucik_nmapscan_$filename.txt           Køre nmap quick scan og skriver det til en fil
        echo -e "Results -> ${RED}qucik_nmapscan_$filename.txt${WHITE}"

        exit 0      Lukker scriptet

</br>

# Kilder og brugbar links 
