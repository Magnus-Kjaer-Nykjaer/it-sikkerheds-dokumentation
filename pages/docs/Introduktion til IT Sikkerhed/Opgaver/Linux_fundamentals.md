# Part 1

Part et af THM Linux rummet består af meget simpelt information.

Det første der blev snakket om var en meget basic forklaring af hvor Linux er brugt så meget i dag.

Efter den intro var det en opgave med de mest base kommandoer til brug i Linux, disse kommandoer var echo og whoami.

Der efter var det en intro til navigering rundt i fil systemer i Linux og hvordan man nemmest finder frem til det man skal bruge, disse kommandoer var ls, cd, cat og pwd som de mest basle. Der efter lærte man find kommandoen og grep kommandoen som er lidt mere avanceret kommandoer

Til sidst lærte jeg om operationer og hvad de gav mulighed for i Linux, disse operatoere var som følge: &, &&, >, >>.

#### Kommandoer

- echo -- skriv ting til konsol eller fil
- whoami
- ls -- laver en liste om hvad der er i den nuværende sti
- cd -- bevæger sig til den valgte sti
- cat -- fremviser indholder af en fil
- pwd -- viser hvor man er i fil stien
- find -- finder det man leder efter
- grep -- kigger den specificeret fil igennem for det ord eller tal man giver den.
- &
- &&
- '>
- '>>

# Part 2

Part to af THM Linux rummet, handler om login og bevægelsen rundt i andre maskiner.

Først skulle jeg forbinde til en anden maskine på THMs netværk, i dette tilfælde var det via THMs attackbox og derfor en anden maskine på THMs netværk.

### Task 3

Efter at man har forbundet til den anden maskine på netværket, bliver man introduceret til falgs og switches.
Ud over det bliver man også introduceret til hjælpe kommandoer så --help og man "kommando", disse bruges til at finde dokumenations information frem for på den kommandom man gerne vil vide noget om.

### Task 4

Den næste del af part 2 opgaverne, går ud på at behandler filer og mapper.
Det første man bliver introduceret til er standard touch og mkdir, som er kommandoer der laver enten filer eller mapper.
Der efter bliver man introduceret til at slette disse filer eller mapper via rm kommandoen, for at slette mapper skal man dog huske -R switchen for at den ved det er en mappe.

Efter at bliver introduceret til at lave og slette elementer handler det om at kopiere og flytter elementer, via cp og mv kommandoerene.
Både cp og mv skal bruge to fil navne for at vide hvor fra den skal tage data og hvor til den skal sende det. Mv er også den kommando der bliver brugt til at rename filer.
Til sidst bliver man introduceret til file kommandoen som giver en mulighed for at inspicere en fil og hvilken fil type den er i.

### Task 5

Starten af opgave fem går ud på at frem vise mere data når man køre ls kommandoer.
Der efter handler vises der frem hvordan man skifter til andre brugere på maskinen, dette gøres via 'su' kommandoen. Hvis man er en root bruger skal man bruge to ting når man vil skrifte bruger nemlig, hvilken bruger man vil skifte til og deres password.

### Task 6

Opgave seks handler om brug bare directories og hvad de hedder. 
Det første directory som der snakkes om er /etc, dette er et directory som maskinen bruger til en masse maskin specefike filer.
Den næste er /var som er et directory som indeholder en filer som programmer tilgår så som, log osv.
Den næste er /root som er home directoriet for root brugeren og er der hvor den bruger har sine ting.
Den sidste er /tmp som er en temporaty directory hvor filer ikke ligger mere end et par dage og er god til at opbevare pentesting scripts i eller andre temp filer.

#### Flag og switch

- --help
- man "kommando" -- Frem viser manualen for den valgte kommando
- -l eller -lh  -- Frem viser mere data i ls kommandoen

#### Kommandoer

- ssh "brugernavn"@"IpAdresse"
- ls -a
- touch  touch   Create file
- mkdir	 make  directory Create a folder
- cp  copy  Copy a file or folder
- mv  move  Move a file or folder
- rm  remove  Remove a file or folder
- file  file  Determine the type of a file
- su su  Switches to another user

# Part 3

Part tre af Linux fundamentals handler om hvordan man tilføjer og laver tekst filer med flere linjer og mere tekst. 

Ud over det så handler det også om automatisering af processer.

### Task 3 -- Terminal Text Editors

Det første der bliver introduceret i task tre er kommandoerne nano og VIM, disse at kommandoer som giver mulig for at rette i filer og tilføjer flere linjer af gangen.

### Task 4 -- General/Useful utilities

Den første kommando vi bliver introduceret til er wget som giver mulighed for at downloade filer vi HTTP, så hvis man har linket til en fil kan den downloade den som om man var i en browser.

Den næste kommado som vi bliver introduceret til er scp, som er en kommando der giver mulighed for at sende filer imellem to maksiner med SSH.

Til sidst bliver vi introduceret til python3 -m http.server som er en kommando der laver en HTTP server som man kan bruge til at hente filer fra via wget.

### Task 5 -- Processes 101

Denne task handler om processer og hvordan man kan se og bruge dem, derfor er den første kommando vi bliver introduceret til ps, som giver mulighed for se de processer som den nuværende bruger køre.
Man kan derfor vælge at puttet aux på sin ps kommando og så får man fremvist alle de processer som maskinen køre.

Den næste kommado vi bliver introduceret til er top kommandoen som giver mulighed for at få fremvist alle de nuværende processer i en pænner måde og den updater også automatisk.

Der efter bliver vi introduceret til kill kommandoen, som giver mulighed for at dræbe den process som man skriver den skal dræbe, man har mulighed for at giver den nogle måder at den skal dræbe processen på, via de tre switches SIGTERM, SIGKILL og SIGSTOP.

Efter kill kommandoen bliver vi introduceret til kommandoen systemctl som giver mulighed for at fortælle systemd at den skal tænde for en bestemt process ved boot, man kan også bede den om at stoppe, enable eller disable processer.

Til sidst bliver der snakket lidt om backgrounding og foregrounding af processer.

Hvis man beder linux om at echo noget er det en forgrunds process, men hvis vi sætter et & tegn bag ved så retunere den bare et process id, dette gøres fordi vi beder den køre det i bagrunden.

Hvis vi har et script der køre kan vi og bruge ´´´Ctrl + Z´´´ for at smide processen i baggrunden så den ikke længere skriver i forgrunden.

Til sidst bliver vi introduceret til fg kommandoen som giver mulighed for at få en process til at skrive i forgrunden igen.

### Task 6 -- Maintaining Your System: Automation

Denne opgaver handler om hvordan man automatisere processer så som at backup ting eller åbner programmer, dette gøre via cron/crontabs

Semantiken for at sætte et cron job i gang er "MIN HOUR DOM MON DOW CMD", når man har sat det om køre cron jobbet så den given kommando ved den givet tidspunkt.

For at kunne se hvilke cron jobs der eksistere, kan man bruge crontab -e som åbner crontabsne i nano.

### Task 7 -- Maintaining Your System: Package Management

Dette afsnit er bare en basic gennemgang af hvordan man installer ting i linux vi command line.

### Task 8 -- Maintaining Your System: Logs

Dette afsnit handler om logs og om hvor de ligger og hvordan man ser hvad de indeholder

#### Kommandoer

- nano      -- Er en simple text editor/måde at skrive mere tekst på en gang.
- VIM       -- Er en meget mere avanceret tekst editor tilforhold nano.
- wget      -- Giver mulighed for at downloade filer fra internetet via HTTP
- scp       -- Giver mulighed for at sende filer sikker og SSH mellem to maskiner.
- python3 -m http.server    -- laver en http server som giver mulighed for at sende filer over http protokolen.
- ps        -- En måde at se de kørende processer i linux
- ps aux    -- Denne kommado giver mulighed for at se alle processer som køre på maskinen, frem for kun dem som den nuværende bruger køre.
- top       -- Fanciere version af ps.
- kill      -- Dræber den process man skriver at den skal dræbe
- systemctl [option] [service]  -- detter en kommando der giver mulighed for at håndtere processer når systemd starter dem ved maskin start.
- Ctrl + Z  -- bringer en process i baggrunden.
- fg        -- bringer en process i forgrunden.
- crontab -e    -- giver mulighed for at åbne sine crontabs en en editor som nano for rette dem.
- apt       -- Downloader og installere ting