# Opgave beskrivelse



# Udføresle af opgave


### Task 2 -- The OSI Model: An Overview
Hurtig gennem af hvad OSI modelen er

TCP (Transmission Control Protocol) and UDP (User Datagram Protocol)
### Task 3 -- Encapsulation
Denne task handler om hvordan data sendes igennem OSI modellen når det sende ud af maskinen eller modtages af maskinen.
For data der skal sendes starter der i lag 7 (Application) og sende hele veje ned igennem lagen til lag 1 (Physical) og det omvendte sker selvfølgelig når dataen modtages, dette kaldes for de-encapsulation.

![Encapsulation Modellen](./Billeder/EncapsulationModel.jpeg "Encapsulation Modellen")

### Task 4 -- The TCP/IP Model
Denne task forklar noget om TCP/IP modellen.

![OSI/TCPIP Modellen](./Billeder/OSIogTCPogIPogModel.png "OSI/TCPIP")

### Task 5 -- Ping
Ping kommandoen er en kommando der giver mulighed for at ping en server via er link som f.eks. google.com

Denne kommando bruger man for at se om der er en forbindelse mellem din maskine og serveren.

### Task 6 -- Traceroute
Traceroute er en kommando som giver mulighed for at se den rute som dit kald til en webserver følger. På windows hedder den tracert

### Task 7 -- WHOIS
WHOIS er en kommando der giver mulighed for at finde basal data på et domain name, ting som hvem ejer domainet hvornår det udløber osv.

### Task 8 -- Dig
Dig er en kommando der manuelt kalder DNS server for den information som de har på et domain.


# Kilder og brugbar links 
