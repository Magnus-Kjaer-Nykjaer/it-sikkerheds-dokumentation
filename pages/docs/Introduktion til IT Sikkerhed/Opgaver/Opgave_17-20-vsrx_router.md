# Opgave beskrivelse

Dette er en opsummering af opgave 17 til 20 da de har med hinanden at gøre.

Opgaverne går ud på at opbygge et netværk og få nogle maskiner til at snakke med hinanden over dette netværk.

# Udføresle af opgave

Jeg startede ud med at download og konfigurer vsrx routeren.

Jeg valget at bruge de VMnet som jeg allerede havde opsat fra en tidligere opgave.

Der efter downloaded og installerede jeg Putty.

Efter jeg prøvede et par gange med at følge opgave 18 og opsætte min putty med en serial forbinedeles, hvor den der blev ved med at give en fejl besked om at den ikke kunne finde baudrate filen.

Jeg googlede lidt rundt for at finde en løsning, hvilket jeg ikke fandt.

![Putty error](./Billeder/PuttyError.PNG)

Putty fejlen var at jeg ikke havde sat den rigtige sti ind i putty for at den vidste hvad den skulle ramme

Valgte jeg at prøve at boot vsrx routeren for at se om jeg kunne få den til at virke, jeg løb der ind i et problem hvor den efter jeg loggede ind ikke gad at tage imod mine inputs.

Fejlen med routeren var at jeg havde sat network adapteren 1 til at peje på en hoste only, hvor det skulle være til et NAT netværk.

Under første start af routeren var jeg kommet til at lukke vmen, hvilket resulteret i at den ikke blev konfigureret ordentligt. Derfor valgte jeg at geninstallere routeren så den blev konfigureret ordentligt.

Efter at jeg endelig havde en virkende router, kørte jeg resten af opgave 18 og 19 igennem.

login: root

Password: vSRXRouterPassword

I opgaven bruges der nogle andre vmnet end dem jeg har valgt at bruge. Dem jeg burger er under:

    VMnet11 -- 192.168.111.0
    VMnet12 -- 192.168.112.0

Da jeg skulle oprttte disse interface løb jeg ind i den fejl under.

    [edit interfaces]
    root@vSRXRouter# set ge-0/0/1 unit 0 family inet address 192.168.111.1/24

    [edit interfaces]
    root@vSRXRouter# commit
    [edit interfaces ge-0/0/1 unit 0 family inet]
    'address 192.168.111.0/24'
        Cannot assign address 0 on subnet
    error: configuration check-out failed

    [edit interfaces]

Den fejl jeg stødte ind i var at jeg havde mine VMnet til at være 192.168.111.1 og 112.1 hvilket det ikke må være. Så derfor ændrede jeg det til 11.1 og 12.1 og så fik jeg lov til at commit.

    [edit interfaces]
    root@vSRXRouter# run show route terse

    inet.0: 4 destinations, 4 routes (4 active, 0 holddown, 0 hidden)
    + = Active Route, - = Last Active, * = Both

    A Destination        P Prf   Metric 1   Metric 2  Next hop         AS path
    * 192.168.11.0/24    D   0                       >ge-0/0/1.0
    * 192.168.11.1/32    L   0                        Local
    * 192.168.12.0/24    D   0                       >ge-0/0/2.0
    * 192.168.12.1/32    L   0                        Local

Der efter følgte jeg opgaven videre til at opsætte secutrity zones på dem.

Denne del af opgaven kom jeg igennem uden problemer.

    [edit security zones]
    root@vSRXRouter# show
    security-zone trust {
        tcp-rst;
        interfaces {
            ge-0/0/1.0 {
                host-inbound-traffic {
                    system-services {
                        ping;
                    }
                }
            }
            ge-0/0/2.0 {
                host-inbound-traffic {
                    system-services {
                        ping;
                    }
                }
            }
        }
    }
    security-zone untrust;

Efter at have sat security zones op så de var trusted, åbnede jeg begge ubuntu vm maskiner og forbandt dem til deres givne IP-adresser.

Da jeg så skulle ping de forskeliger ip adresser løb jeg ind i at de begge godt kunne ping deres router ip, men ikke de andre maskiner på netværket.