# Opgave beskrivelse

1. Opret en konto på https://tryhackme.com/ - det anbefales at bruge din private email adresse fordi din skole email forsvinder efter endt uddannelse.

2. Start din Kali Linux VM, hvis du ikke har en så følg Opgave 24 - Kali Linux på vmware

3. Følg guiden fra THM (kræver at du er logget på THM) https://tryhackme.com/access?o=vpn

4. Gennefør rummet Tutorial på THM https://tryhackme.com/room/tutorial - Husk at du IKKE skal bruge Attackbox fordi du er forbundet via VPN.

# Udføresle af opgave

Startede ud med at lave min THM konto med min private mail.
Der efter startede jeg min Kali Linux Vm og fulgte VPN linkets guide, men stødte i problemer med at oprette forbindelse til THMs VPN da skolens netværk stopper os for at oprette forbindelse til THMs VPN.

Grundet dette problem med at hoppe på THMs VPN, valgte jeg at gå over og køre Attackbox turoialsene igennem.

Fandt så ud af at netværket ikke blokker THMs VPN og det derfor bare krævede et forsøg igen på at forbinde og så virkede det.

Efter jeg fik forbundet til THMs VPN valgte jeg har begynde på THMs roomet Tutorial.

Denne opgave gik ud på at forbinde til THMs Attackbox via deres VPN, det var ikke en særlig svær opgave at finde ud af.

Efter jeg fik dette til at virke valgte jeg at begynde på Linux Fundamentals Part 1.

# Kilder og brugbar links 
