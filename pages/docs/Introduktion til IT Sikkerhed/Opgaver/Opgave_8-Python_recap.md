# Opgave beskrivelse

Del 1

1. Start med denne lille Python quiz: https://www.w3schools.com/python/python_quiz.asp

2. Læs om hvad Python er https://www.python.org/doc/essays/blurb/ og kig på sammenligning med andre sprog https://www.python.org/doc/essays/comparisons/

3. Lav et fælles gitlab repo til det i laver i denne opgave, klon det til jeres kali linux vm's.

4. Analyser sammen simple_calculator.py.
For hver linje i programmet noterer i hvad den enkelte linje gør. I må bruge alle hjælpemidler til at analysere programmet, husk at der er links herunder. Husk også at køre programmet så i dels ved at i kan afvikle python programmer, dels ved hvordan programmets funktionalitet er og at der ikke er fejl i programmet...
Læg mærke til datatyper, kontrol strukturer, variabler, funktioner, indentering osv.
Det er vigtigt at i snakker med hinanden og bruger hinandens erfaring med programmering, i en god dialog vil i også få snakket om forskellene på de forskellige sprog i kender.

5. På klassen laver vi code review ud fra jeres analayse, formålet er at alle har forstået koden og hvordan Python fungerer.

Del 2



1. Omskriv så meget af programmet som muligt så det har så få gentagelser som muligt, altså abstraher så meget som muligt til funktioner.

2. Udvid lommeregner programmet så det kan logge input, resultater og fejl til en .log fil.
Brug logging modulet i Python https://realpython.com/python-logging/

3. Omskriv programmet så regne funktionerne er i en klasse for sig selv og logging funktionalitet er i en anden klasse. Funktionaliteten skal være den samme som i punkt 5+6.

4. Lav en python fil der importerer og bruger jeres klasser så programmet afvikles som det oprindelige program.

# Udføresle af opgave

