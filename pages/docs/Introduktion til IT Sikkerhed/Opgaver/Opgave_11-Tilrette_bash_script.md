# Opgave beskrivelse

1. Diskuter nedenstående script med dit team. Hvad sker der på hver linje i dette script? – I skal snakke det igennem FØR I kører koden. Ideen er at forstå koden.

2. Implementer dette script i din linux VM. Når du har fået det til at virke, så tilret således nmap resultatet ikke skrives til en fil, men at alle de åbne porte printes direkte i konsolvinduet (uden anden output).

![Bash Script](../Opgaver/Billeder/BashScript.png)

</br>

# Udføresle af opgave

Har prøvet at køre det i kali men den kan ikke lide slutningen af filen.

</br>

# Kilder og brugbar links 
