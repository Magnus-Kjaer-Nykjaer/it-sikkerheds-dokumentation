#!/bin/bash
RED='\033[0;31m'
WHITE='\033[0;38m'
NC='\033[0m'

echo -e "Running a ${RED}silent scan ${WHITE} on target ${RED}${1}"
nmap -sS $1 > silent_nmapscan_$1.txt
echo -e "Results -> ${RED}silent_nmapscan_$filename.txt${WHITE}"


# Køre commandoen ./TilrettetBashScript.sh 192.168.87.145 for at det virker