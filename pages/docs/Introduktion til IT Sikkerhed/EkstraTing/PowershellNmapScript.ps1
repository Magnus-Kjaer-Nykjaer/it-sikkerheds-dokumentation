Read-Host "Tryk enter for at startet processen"

$IP = (Get-NetIPAddress -InterfaceIndex 5).IPAddress

$Dato = Get-Date -Format "dd/MM/yyyy_HH_mm"

$filnavn = $Dato

Write-Output "Køre en nmap scanning på ip adressen:  $IP "

$fil = "C:\Users\mkn\Downloads\quick_nmapscan_$filnavn.txt"

Write-Output $fil

$ProgramName = "C:\Program Files (x86)\Nmap\nmap.exe"
$ArgumentList = "-sS $IP"

$NMAP_Process = Start-Process $ProgramName -ArgumentList $ArgumentList -wait -NoNewWindow -PassThru  | Tee-Object -FilePath $fil

