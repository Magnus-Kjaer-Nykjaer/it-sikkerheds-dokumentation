import sqlite3
from pathlib import Path
files_path = Path('C:/Users/mkn/Downloads/chinook')
print(files_path)

# Create and connect to database
conn = sqlite3.connect(files_path / 'chinook.db')

cur = conn.cursor()

input = "10 OR 1=1"

with conn:
    cur.execute('SELECT CustomerId, FirstName, LastName, Country From customers Where CustomerId = {}'.format(input))

# print out the values
for row in cur:
    print(row)

# Close database connection
conn.close()

