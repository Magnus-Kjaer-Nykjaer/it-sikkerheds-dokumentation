

![Alt text](../Billeder/GDPR_Sagsbehandler_spørgsmål.png)


- 1 måned, hvis der ingen komplekse anmodninger er. 2 måneder hvis der er mange og komplekse anmodninger.

- Elektronisk anmodning = så vidt muligt elektronisk svar, og så svare gennem en troværdigt medie såsom e-boks.

- Man skal gøre det let forståeligt og let tilgængelig ifølge artikel 12, stk. 1.

- Ja, fordi de er særligt underlagt en regel i artikel 9.

- Der står i artikel 12 stk5. at det er nogle omstændigheder for gebyr ved andmodninger, HR: Gratis for registrerede U1: Kværulantregel - Enten rimeligt gebyr, eller afvise at efterkomme (hvis de bliver ved)


![Alt text](../Billeder/GDPR_spørgsmål_artikel_13_14.png)

1. Ja de har tænkt meget over at man ikke må være lusket/slesk omkring hvordan man informere brugerne om samtykke. Dette kan ses på den måde som hjemmesider prompter bruger omkring samtykke, da de gør det tydeligt og transparent at man giver samtykke. 

2. Fynbus I/S har ikke givet oplysningerne med det samme og i stedet lagt dem et ikke gennemsigtigt sted på deres side og har ikke givet tilstækkeligt mængde oplysniger. 

3. 



![Alt text](../Billeder/Det%20Konservative%20Folkepartis%20manglende%20opfyldelse%20af%20oplysningspligten.png)


1. Gennemsigtighed og gør det på en ordentligt måde og inde for en måned.

Den dataansvarlige skal give de oplysninger, der er omhandlet i stk. 1 og 2, inden for en rimelig frist efter indsamlingen af personoplysningerne, men senest inden for en måned under hensyn til de specifikke forhold, som personoplysningerne er behandlet under, jf. artikel 14, stk. 3, litra a. Den maksimale frist, inden for hvilken oplysningerne skal gives til den registrerede, er under alle omstændigheder én måned.


![Alt text](../Billeder/RetTilAtSletteData.png)

1. Bilederne kan godt blive slettet med mindre at han har givet samtykke til at det ikke kan slettes. 
Artikel 7 stk 3. siger at samtykke kan trækkes tilbage, med mindre man har givet samtykke til at det ikke kan trækkes tilbage.

2. Personen har højst sandsynligt skrevet under på at hun ikke kan få dem slettet. Det er ikke en samtykke erklæring men en kontrakt som hun har skrevet under på.

3. Nej alt data må ikke slettes da det har med penge at gøre og derfor skal virksomheden have data liggende omkring transaktionerne i minimum 5 år.

4. Det er hendes forældres profil og hun har ikke myndighed til at bestemme om de må ligge billeder op af hende. 