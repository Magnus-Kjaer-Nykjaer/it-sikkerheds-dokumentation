# Opgave beskrivelse

### Instruktioner

1. Forklar med egne ord hvad et intrusion detection system gør, underbyg med troværdige kilder (links)

2. Forklar med egne ord hvad et intrusion prevention system gør, underbyg med troværdige kilder (links)

3. Forklar forskellen på et Netværks IDS/IPS (NIDS) og et host IDS/IPS (HIDS)

4. Forklar med egne ord hvad signatur baseret detektering er, underbyg med troværdige kilder (links)

5. Forklar med egne ord hvad anomali baseret detektering er, underbyg med troværdige kilder (links)

6. Forklar mulige problemer med hhv. signatur og anomali baseret detektering

</br>

# Udføresle af opgave

1. Forklar med egne ord hvad et intrusion detection system gør, underbyg med troværdige kilder (links)
Et IDS sidder og passivt kigger på netværkstrafik og informerer om når der bliver lavet noget på netværket som der ikke skal laves. (https://www.cl.cam.ac.uk/~rja14/Papers/SEv2-c21.pdf)

2. Forklar med egne ord hvad et intrusion prevention system gør, underbyg med troværdige kilder (links)
Et IPS er aktivt. Det forsøger at stoppe opdagede trusler (https://www.paloaltonetworks.com/cyberpedia/what-is-an-intrusion-prevention-system-ips)

3. Forklar forskellen på et Netværks IDS/IPS (NIDS) og et host IDS/IPS (HIDS)
Netværks IDS kigger på netværket som en helhed og er lidt mere overfladisk og ser om der sker ting på netværket som der ikke må ske.

Host-based IDS: Beskytter på endpoint niveau. Beskytte de enkelte endpoints fra trusler.
(https://en.wikipedia.org/wiki/Intrusion_detection_system)

4. Forklar med egne ord hvad signatur baseret detektering er, underbyg med troværdige kilder (links)
Har en database med kendte trusler. Den kan så genkende, hvis nogle af disse trusler kommer ind på netværket.
Men den kan ikke opdage ukendte trusler.

(https://www.linkedin.com/advice/0/what-pros-cons-signature-based-vs-anomaly-based)

5. Forklar med egne ord hvad anomali baseret detektering er, underbyg med troværdige kilder (links)
Kender en model for normal netværks opførsel, og kan opdage anomalier på netværket.
Men har også falsk positive.

(https://www.linkedin.com/advice/0/what-pros-cons-signature-based-vs-anomaly-based)

6. Forklar mulige problemer med hhv. signatur og anomali baseret detektering
Signatur baseret detektion kan ikke registrere nye eller ukendte angreb eller varianter af eksisterende angreb, der ikke matcher nogen signatur i databasen. Den har også en høj frekvens af falske positiver eller falske alarmer, når legitim trafik forveksles med et angreb. Desuden kan signatur baseret detektion være ressourcekrævende og sænke netværkets ydeevne, da den skal scanne hver pakke med data mod et stort antal signaturer.

Anomali-baseret detektion kan være svært at etablere og opretholde en pålidelig baseline for normal adfærd, især for komplekse og heterogene netværk. Det har også en høj frekvens af falske negativer eller mistede angreb, når ondsindet trafik blandes med lovlig trafik eller efterligner normal adfærd. Ydermere kan anomali baseret detektion være dyrt og komplekst at implementere og drive, da det kræver avancerede teknologier og færdigheder.

(https://www.linkedin.com/advice/0/what-pros-cons-signature-based-vs-anomaly-based) 

</br>

# Kilder og brugbar links 
