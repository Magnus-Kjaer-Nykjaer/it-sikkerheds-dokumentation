# Opgave beskrivelse

1. Vælg et problem, gerne når det opstår, alternativt på bagkant. Det kunne være printerfejl, wifi connection issues, hjemmesider der er ‘væk’, langsomt internet, certifikat fejl, mv
2. Beskriv problemet. Vær opmærksom på forskellen mellem hvad der er observeret, og hvad der er “rygmarvsreaktion”.
3. Beskriv hvad du tror problemet er (ie. formuler en hypotese)
4. Find oplysninger der underbygger/afkræfter ideen (ie. se i logfiler, lav tests, check lysdioder, …) og skriv det ned
5. Hvis ideen blev afkræftet, lav en ny hypotese.
6. Beslut hvad der kan gøres for at afhjælpe eller mitigere fejlen, hvis den skulle komme igen.
7. Implementer det hvis ressourcerne tillader det, eller beskriv hvordan det skal håndteres til næste gang.
Bonus spørgsmål: Hvordan håndterede du at nå grænsen for din viden og/eller forståelse af problemet eller et del-element?

# Udføresle af opgave

## Opgave 3 - Fejlfinding

Problem: Computeren viser artifacter på skærmen og lukker ned, efter den har været tændt i noget tid(cirka 30 min eller når den er varm).

Hypotese: Jeg tror problemet er at grafik kortet er ved at dø og sender fejl strøm så computeren lukker ned.

Startede med at frem provokere fejlen.



# Kilder og brugbar links 
