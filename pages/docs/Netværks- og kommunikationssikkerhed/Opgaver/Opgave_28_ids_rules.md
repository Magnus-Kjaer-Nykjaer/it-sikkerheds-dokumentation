# Opgave beskrivelse

### Instruktioner

1. Læs om regler i Suricata dokumentationen og dan dig et overblik over mulighederne.

2. Lav en ny mappe på den vm hvor du har installeret Suricata

3. På suricata maskinen hent pcap filen fra https://www.malware-traffic-analysis.net/2018/07/15/2018-07-15-traffic-analysis-exercise.pcap.zip (brug wget, zip password er infected)

4. Skriv en regel i /etc/suricata/rules/local.rules der laver en alert på udp trafik fra 10.0.0.201 port 63448 til 5.138.53.160 port 41230
Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?

5. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor http content type er image/jpeg
Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?

6. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor http content type er application/x-bittorrent

7. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor ordet Betty_Boop indgår.
Dokumenter hvor mange alerts du ser, samt hvilke ip adresser og porte der er involveret i kommunikationen ?

</br>

# Udføresle af opgave

3. For at kunne unzip filen skal man installer pakke unzip for at kunne det og så køre det som følge 

        user@debian-base:~/pcap-files$ sudo apt install unzip
        [sudo] password for user:
        Reading package lists... Done
        Building dependency tree... Done
        Reading state information... Done
        Suggested packages:
        zip
        The following NEW packages will be installed:
        unzip
        0 upgraded, 1 newly installed, 0 to remove and 1 not upgraded.
        Need to get 172 kB of archives.
        After this operation, 393 kB of additional disk space will be used.
        Get:1 http://deb.debian.org/debian bullseye/main amd64 unzip amd64 6.0-26+deb11u1 [172 kB]
        Fetched 172 kB in 0s (2,359 kB/s)
        Selecting previously unselected package unzip.
        (Reading database ... 34221 files and directories currently installed.)
        Preparing to unpack .../unzip_6.0-26+deb11u1_amd64.deb ...
        Unpacking unzip (6.0-26+deb11u1) ...
        Setting up unzip (6.0-26+deb11u1) ...
        Processing triggers for mailcap (3.69) ...
        Processing triggers for man-db (2.9.4-2) ...
        user@debian-base:~/pcap-files$ unzip -P infected 2018-07-15-traffic-analysis-exercise.pcap.zip
        Archive:  2018-07-15-traffic-analysis-exercise.pcap.zip
        inflating: 2018-07-15-traffic-analysis-exercise.pcap
        user@debian-base:~/pcap-files$ ls
        2018-07-15-traffic-analysis-exercise.pcap  2018-07-15-traffic-analysis-exercise.pcap.zip  eve.json  fast.log  ping.pcapng  stats.log  suricata.log

4. Med denne regel kommer der to alerts og de er som følge 

        07/15/2018-06:17:51.292077  [**] [1:0:0] (null) [**] [Classification: (null)] [Priority: 3] {UDP} 10.0.0.201:63448 -> 5.138.53.160:41230
        07/15/2018-06:18:44.563423  [**] [1:0:0] (null) [**] [Classification: (null)] [Priority: 3] {UDP} 10.0.0.201:63448 -> 5.138.53.160:41230

5. Med reglen under fik jeg seks alerts som matcher image/jpeg 

        alert http any any <> any any (msg:"Tjekker for fil typen jpeg i http kald"; content:"image/jpeg";)

        07/15/2018-06:17:07.134995  [**] [1:0:0] Tjekker for fil typen jpeg i http kald [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49759
        07/15/2018-06:17:12.130152  [**] [1:0:0] Tjekker for fil typen jpeg i http kald [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49756
        07/15/2018-06:17:24.943853  [**] [1:0:0] Tjekker for fil typen jpeg i http kald [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49819
        07/15/2018-06:17:12.130281  [**] [1:0:0] Tjekker for fil typen jpeg i http kald [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49761
        07/15/2018-06:17:12.131119  [**] [1:0:0] Tjekker for fil typen jpeg i http kald [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49760
        07/15/2018-06:17:19.785508  [**] [1:0:0] Tjekker for fil typen jpeg i http kald [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49817

6. Med en regle som kigger på x-bittorrent applicationen fik jeg 720 alerts, reglen er som følge:

        alert http any any <> any any (content:"application/x-bittorrent";)

Og et eksempel på et resultat er 

        07/15/2018-06:19:01.105345  [**] [1:0:0] Trafik fra application/x-bittorrent" content:"application/x-bittorrent [**] [Classification: (null)] [Priority: 3] {TCP} 10.0.0.201:49805 -> 72.21.91.29:80

 
 7. 
        alert http any any <> any any (content:"Betty"; nocase;)


        07/15/2018-06:17:07.072463  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49757
        07/15/2018-06:17:19.683701  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49817
        07/15/2018-06:17:19.731994  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49817
        07/15/2018-06:17:19.785211  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 10.0.0.201:49817 -> 168.215.194.14:80
        07/15/2018-06:17:20.295483  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 10.0.0.201:49824 -> 72.21.202.62:80
        07/15/2018-06:17:20.471282  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 10.0.0.201:49831 -> 52.94.233.131:80
        07/15/2018-06:17:25.218440  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49818
        07/15/2018-06:17:27.242671  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 10.0.0.201:49834 -> 168.215.194.14:80
        07/15/2018-06:17:27.242671  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.194.14:80 -> 10.0.0.201:49834
        07/15/2018-06:17:38.415504  [**] [1:0:0] Trafik med ordet Betty-Boop i [**] [Classification: (null)] [Priority: 3] {TCP} 168.215.195.227:6969 -> 10.0.0.201:49850

</br>

# Kilder og brugbar links 
