# Opgave beskrivelse

Opsæt en router i vmware og ping en vm fra en anden og se dataen med wireshark

</br>

# Udføresle af opgave

Jeg startede ud med at geninstaller mit Kali Linux da jeg var kommet til at slette det.

Efter at jeg havde Kali oppe og køre, valgte jeg at gå ind og sætte subnetsne op, under "Edit-->Virtual Network Editor" i vmware.

Efter at have sat subnetsne lavet med de credentials som tutorialen gav, gik jeg ind og downloadede den .ova fil med OpenBSD routeren på, og importeret den i vmware. Når den var importeret valgte jeg routerens network adapteres som de subnets jeg lavede og kørte vmen.

Da jeg skulle sætte OpenBSD routeren op, kom jeg til ved første boot at lukke vmen ned før den nåde særligt langt, så jeg tror jeg fik låst den fast i en default setting. Hvor jeg ikke have indflydelse på at sætte mange af indstillingerne, så jeg valgte at geninstaller den for at se om det fiksede det, da jeg ikke kunne finde noget default login på nettet.

Det løste ikke mit problem med at geninstallere den.

</br>

# Kilder og brugbar links 

<https://moozer.gitlab.io/course-networking-basisc/05_more_routing/setup/#importing-router-1h>