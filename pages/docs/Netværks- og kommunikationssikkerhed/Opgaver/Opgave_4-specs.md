# Opgave beskrivelse

1. Find listen fra ww06, hvor lavede vi en liste over hardware og deres plads i OSI modellen.
2. Udvælg 2-3 forskellige enheder fra listen
3. Find specifikationerne for enheden
4. Hvad bliver listet som begrænsninger? Synes du det er høje tal? lave tal?

# Udføresle af opgave

## Opgave 4 - Specs

### Enhed 1 - Den jeg har der hjemme
Technicolor CGA4233

<https://dktconnect.dk/upload_dir/shop/CGA4233.pdf>

Udfylder IEEE standarderne, 802.11n, 802.11ac, 802.1x og 802.1q  

Ethernet:

WiFi styrker: 
2,4 GHz har to modes standrad optil 20 dBm og en high power der har 33 til 36 dBm.
5 GHz har kun en styrke og det er 36 dBm.

Lan: 4X porte op til 1000 Mbps

Har GUI og er managet.

Understøtter Voice over ip og andet telefoni over internettet, dette er nok fordi forbindelsen er bragt over telefon kabel.

### Enhed 2
TL-SG105 V6 Network Switch

<https://static.tp-link.com/res/down/doc/TL-SG105-108.pdf>

Udfylder IEEE standarderne, 802.3, 802.3u, 802.3ab, 802.3x, 802.1p

Ethernet:

WiFi styker:
Ingen

Lan:
5x optil 1000 Mbps

Er ikke mannaget

### Enhed 3
D-Link DIR-809 AC750

<https://eu.dlink.com/pl/pl/-/media/consumer_products/dir/dir-809/datasheet/dir_809_b1_datasheet_en_eu.pdf>

Udfylder IEEE standarderne,  802.11ac/n/g/b/a og 802.3u

Ethernet:

WiFi styker:
2,4 GHz op til 366 Mbps
5 GHz op til 433 Mbps

Lan: Fire lan porte med optil 100 Mbps.

Mulighed for at bruge den som en reaperter.

Har en Web setup wizard så den er nok lidt managet.

# Kilder og brugbar links 

<https://dktconnect.dk/upload_dir/shop/CGA4233.pdf>