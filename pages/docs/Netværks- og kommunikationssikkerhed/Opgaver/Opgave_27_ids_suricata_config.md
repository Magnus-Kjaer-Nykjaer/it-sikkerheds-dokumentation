# Opgave beskrivelse

### Instruktioner

1. Hent og åbn en basis debian vm uden desktop installeret.
    - Der er 2 brugere. user:user1234 og root:root1234
    - ssh server er installeret så du kan tilgå maskinen fra en anden VM, f.eks din Kali
    - Network adapter skal ændres til det vmnet du ønsker at anvende, du skal have internetforbindelse (ie. vmnet8)

2. Tegn et netværks diagram over netværket

3. Installer Suricata
    - sudo apt-get install suricata
    - kontroller at suricata er installeret: suricata -V

4. Konfigurer Suricata med din foretrukne tekst editor. Konfigurations filen er i /etc/suricata/suricata.yaml
 a. Under default-rule-path kan du angive en mappe hvor suricata henter regler, du kan angive flere lokationer. Ret så det ligner nedenstående:

        default-rule-path: /etc/suricata/rules

        rule-files:
            - local.rules

5. Opret filen local.rules i mappen /etc/suricata/rules og tilføj denne linie (regel) i local.rules filen:
alert icmp any any -> any any (msg:"ping/icmp detected"; sid:5000000;)

6. Test at suricata virker og accepterer din nye regel (det vil sige ingen errors i output), brug kommandoen sudo suricata -T, output bør ligne:

        user@debian-base:~/pcap-files$ sudo suricata -T
        22/4/2023 -- 14:04:27 - <Info> - Running suricata under test mode
        22/4/2023 -- 14:04:27 - <Notice> - This is Suricata version 6.0.1 RELEASE running in SYSTEM mode
        22/4/2023 -- 14:04:27 - <Notice> - Configuration provided was successfully loaded. Exiting.

Hvis der er fejl så fejlfind og ret indtil der ikke er fejl.

7. Lav en mappe der hedder pcap-files i user home og gem filen ping.pcapng i mappen

8. Kør pcapng i suricata med kommandoen sudo suricata -v -r ping.pcapng og kig på output, hvad ser du?

9. Undersøg indholdet i de 4 nye filer som suricata har genereret i pcap-files mappen, hvad er indholdet og hvad kan det bruges til?
Forstå og dokumenter det.

        user@debian-base:~/pcap-files$ ls -l
        total 24
        -rw-r--r-- 1 user user 6571 Apr 22 14:09 eve.json
        -rw-r--r-- 1 user user  308 Apr 22 14:09 fast.log
        -rw-r--r-- 1 user user 1152 Apr 22 13:07 ping.pcapng
        -rw-r--r-- 1 user user 1899 Apr 22 14:09 stats.log
        -rw-r--r-- 1 user user 1735 Apr 22 14:09 suricata.log

</br>

# Udføresle af opgave

        user@debian-base:~$ ip a
        1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
            link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
            inet 127.0.0.1/8 scope host lo
            valid_lft forever preferred_lft forever
            inet6 ::1/128 scope host
            valid_lft forever preferred_lft forever
        2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
            link/ether 00:0c:29:8f:f9:a7 brd ff:ff:ff:ff:ff:ff
            altname enp2s1
            inet 192.168.17.138/24 brd 192.168.17.255 scope global dynamic ens33
            valid_lft 1570sec preferred_lft 1570sec
            inet6 fe80::20c:29ff:fe8f:f9a7/64 scope link
            valid_lft forever preferred_lft forever

![Netværks diagram](../Opgaver/Billeder/SuricataNetvaerksDiagram.jpg)

    user@debian-base:~$ suricata -V

    This is Suricata version 6.0.1 RELEASE

    user@debian-base:~$ sudo nano /etc/suricata/suricata.yaml

    user@debian-base:~$ sudo nano /etc/suricata/rules/local.rules

    user@debian-base:~$ sudo suricata -T
    24/4/2023 -- 14:03:05 - <Info> - Running suricata under test mode
    24/4/2023 -- 14:03:05 - <Notice> - This is Suricata version 6.0.1 RELEASE running in SYSTEM mode
    24/4/2023 -- 14:03:05 - <Notice> - Configuration provided was successfully loaded. Exiting.

    user@debian-base:~/pcap-files$ wget https://ucl-pba-its.gitlab.io/23f-its-nwsec/                                                                                                                                                             pcap/ping.pcapng
    --2023-04-24 14:07:08--  https://ucl-pba-its.gitlab.io/23f-its-nwsec/pcap/ping.p                                                                                                                                                             capng
    Resolving ucl-pba-its.gitlab.io (ucl-pba-its.gitlab.io)... 35.185.44.232
    Connecting to ucl-pba-its.gitlab.io (ucl-pba-its.gitlab.io)|35.185.44.232|:443..                                                                                                                                                             . connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 1152 (1.1K) [application/octet-stream]
    Saving to: ‘ping.pcapng’

    ping.pcapng         100%[===================>]   1.12K  --.-KB/s    in 0s

    2023-04-24 14:07:08 (25.0 MB/s) - ‘ping.pcapng’ saved [1152/1152]

    user@debian-base:~/pcap-files$ ls
    ping.pcapng

    user@debian-base:~/pcap-files$ sudo suricata -v -r ping.pcapng
    24/4/2023 -- 14:08:43 - <Notice> - This is Suricata version 6.0.1 RELEASE runnin                                                                                                                                                             g in USER mode
    24/4/2023 -- 14:08:43 - <Info> - CPUs/cores online: 2
    24/4/2023 -- 14:08:43 - <Info> - fast output device (regular) initialized: fast.                                                                                                                                                             log
    24/4/2023 -- 14:08:43 - <Info> - eve-log output device (regular) initialized: ev                                                                                                                                                             e.json
    24/4/2023 -- 14:08:43 - <Info> - stats output device (regular) initialized: stat                                                                                                                                                             s.log
    24/4/2023 -- 14:08:43 - <Info> - 1 rule files processed. 1 rules successfully lo                                                                                                                                                             aded, 0 rules failed
    24/4/2023 -- 14:08:43 - <Info> - Threshold config parsed: 0 rule(s) found
    24/4/2023 -- 14:08:43 - <Info> - 1 signatures processed. 1 are IP-only rules, 0                                                                                                                                                              are inspecting packet payload, 0 inspect application layer, 0 are decoder event                                                                                                                                                              only
    24/4/2023 -- 14:08:43 - <Info> - Using unix socket file '/var/run/suricata-comma                                                                                                                                                             nd.socket'
    24/4/2023 -- 14:08:43 - <Notice> - all 3 packet processing threads, 4 management                                                                                                                                                              threads initialized, engine started.
    24/4/2023 -- 14:08:43 - <Info> - Starting file run for ping.pcapng
    24/4/2023 -- 14:08:43 - <Info> - pcap file ping.pcapng end of file reached (pcap                                                                                                                                                              err code 0)
    24/4/2023 -- 14:08:43 - <Notice> - Signal Received.  Stopping engine.
    24/4/2023 -- 14:08:43 - <Info> - time elapsed 0.039s
    24/4/2023 -- 14:08:43 - <Notice> - Pcap-file module read 1 files, 6 packets, 588 bytes
    24/4/2023 -- 14:08:43 - <Info> - Alerts: 2
    24/4/2023 -- 14:08:43 - <Info> - cleaning up signature grouping structure... complete

Ud fra det understående svar fra filen fast.log så fandt den to ting som matcher den regle som vi har været inde og sætte.

    user@debian-base:~/pcap-files$ cat fast.log
    04/22/2023-13:06:08.274513  [**] [1:5000000:0] ping/icmp detected [**] [Classification: (null)] [Priority: 3] {ICMP} 192.168.15.137:8 -> 192.168.15.128:0
    04/22/2023-13:06:08.274634  [**] [1:5000000:0] ping/icmp detected [**] [Classification: (null)] [Priority: 3] {ICMP} 192.168.15.128:0 -> 192.168.15.137:0

</br>

# Kilder og brugbar links 
