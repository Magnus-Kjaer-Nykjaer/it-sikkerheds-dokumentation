# Opgave beskrivelse

### Instruktioner

1. Hent opnsense .ova filen

2. Mens du venter, så dan dig et overblik over hvordan Emerging Threaths reglerne er organiseret og tag også et kig på et par regler, de kan hurtigt blive komplekse!

3. Åbn og konfigurer opnsense maskinen:
    - vmnet8 på network adapter 1
    - vmnet1 på network adapter 2
    - Start maskinen
    - Noter routerens lan og wan adresser.

Info om opnsense vm opsætning:

            root:root1234
            wan nwadapter 1: addr from dhcp
            lan nwadapter 2: 192.168.1.0/24 dhcp server enabled
            mngt iface: 192.168.1.1

4. Sæt en vm (victim) på opnsense lan interfacet vmnet1

5. Åbn en browser på victim og log på opnsense management interfacet: http://192.168.1.1

6. Sæt en kali maskine (attacker) på opnsense lan interfacet vmnet1

7. Tegn et netværksdiagram over dit netværk

8. I opnsense management interfacet skal du navigere til services->intrusion detection->administration
Her skal du vinge af i enabled, Promiscuous mode og du skal sætte interfaces til både wan og lan.
Husk at klikke på Apply i bunden af vinduet!
opnsense ids administration

9. Gå til Download fanen og find regelsættet der hedder ET open/emerging-scan og ving det af, klik på enable selected og derefter Download & update rules
opnsense ids download rules

10. Gå til Rules fanen og marker alle regler (der er 361! så find knappen der vælger dem alle på en gang) og aktiver dem, klik derefter på Apply
opnsense ids rules

11. Gå til Alerts fanen som gerne skulle være tom, det er her du vil kunne se hvad der registreres af suricata

12. På victim maskinen start en service der lytter på en port, f.eks med sudo python3 -m http.server

13. På Kali maskinen lav en nmap scanning på victim maskinens ip adresse f.eks sudo nmap -v -sV 192.168.1.101 (erstat ip med den adresse som din victim maskine har)

14. Refresh alerts og du burde kunne se at der er blevet detekteret noget
opnsense ids alerts
Hvis der ikke er nogle alerts så gennemgå ovenstående og fejlfind :-)

15. Undersøg dine alerts og genfind reglerne på Emerging Threaths

16. Lav en nmap scanning der ikke bliver fanget af suricata, altså ikke alerter.
Forklar hvilken type nmap scanning du lavede

17. Lav en nmap scanning der laver så mange alerts som muligt (mere end 4)
Forklar hvilken type nmap scanning du lavede

</br>

# Udføresle af opgave



</br>

# Kilder og brugbar links 
