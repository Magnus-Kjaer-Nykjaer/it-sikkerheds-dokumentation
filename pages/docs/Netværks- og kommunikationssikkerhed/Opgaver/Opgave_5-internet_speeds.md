# Opgave beskrivelse

1. Gå på speedtest
2. Hvad er hastigheden?
3. Best guess: hvad er bottleneck, og hvorfor.
Prøv evt. at køre den samtidigt på to maskiner på samme subnet/wifi.

# Udføresle af opgave

## Opgave 5 - Internet speeds

Som man kan se på billedet under er dette den første hastigheds test jeg kørte fra min stationær, med en kablet forbindelse.

![Net Hastighedstest fra stationær](./Billeder/InternetHastighed.PNG "Net Hastighedstest fra stationær")

Vores abonnoment er på 1000/100 så det er okay hastigheds måling for et internet med to brugere på, hvor den ene spiller og den anden tester hastighed.


Jeg lavede der efter en test med min bærbar og min stationære på samme tid, hvor bærbaren var på wifi og min stationære var kablet.

![Net Hastighedstest fra stationær med to pcer](./Billeder/HastighedsTestMedToPcer.PNG "Net Hastighedstest fra stationær med to pcer")

![Net Hastighedstest fra bærbar med to pcer](./Billeder/HastighedsTestMedToPcerB%C3%A6rbar.PNG "Net Hastighedstest fra bærbar med to pcer")

Så som man kan se så er det trædløse netværk ikke særlig godt inde på mit værelse, da det skal nå igennem to solide beton mure og derfor har det nok ikke mulighed for at køre med 5 GHz og køre derfor 2,4 GHz.

Ud over det kan man se et lille fald i upload hastigheden, men har ikke et godt svar til hvorfor det gør dette, ud over at vores router ikke er god nok til at håndtere flere enheder der uploader meget på en gang.

# Kilder og brugbar links 