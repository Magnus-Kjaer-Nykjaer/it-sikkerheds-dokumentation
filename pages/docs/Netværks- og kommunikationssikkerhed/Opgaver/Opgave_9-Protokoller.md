# Opgave beskrivelse

1. Find to-tre protokoller fra wiresharks test pakker som du kan genkende

2. Forstå hvad der foregår, evt. vha. google

3. Tag noter, og vis det næste gang
</br>

# Udføresle af opgave

BITTORRENT:

Der sker fejl med ACK segmentet i protokolen men tror ikke det er det der er fejlen.

![Bittorrent Failing ACK](../Opgaver/Billeder/BittorrentFailingACK.png)

</br>
mysql-ssl-larger:

Jeg tror at det der er usikkert ved dette er at den krypteret trafik kun sendes med TLSv1.0 som er en ældrer og usikker protokol.

![TLSv1.0](../Opgaver/Billeder/TLSv1.0.png)

</br>
ms-sql-tds-rpc-requests:

Alt data i denne stream bliver sendt med TDS(Tabular Data Stream) som er en måde at sende SQL kald mellem en klient og server.

Alt data der sendes via denne protokol er ikke krypteret så man har mulighed for at se alle de SQL queries som bliver sendt samt den data som de giver.

Så som man kan se på billedet står dataen bare i plain tekst.
![Wireshark TDS](../Opgaver/Billeder/WiresharkTDS.png)

</br>

# Kilder og brugbar links 


