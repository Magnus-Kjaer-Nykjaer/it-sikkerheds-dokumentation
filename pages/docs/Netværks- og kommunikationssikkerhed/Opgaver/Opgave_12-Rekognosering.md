# Opgave beskrivelse

### Instruktioner

1. Gennemfør THM rummet: <https://tryhackme.com/room/passiverecon>

2. Læs om og registrer en konto på securitytrails.com

3. Beskriv forskellen på aktiv og passiv rekognoscering

4. Lav en liste med de værktøjer du kender til aktiv rekognoscering, beskriv kort deres formål

5. Lav en liste med de værktøjer du kender til passiv rekognoscering, beskriv kort deres formål

6. Beskriv med dine egne ord hvad en fjendtlig aktør kan bruge rekognoscering til

7. Beskriv med dine egne ord hvad du kan bruge rekognoscering til, når du skal arbejde med sikkerhed

8. Brug kun PASSIV rekognoscering til at undersøge ucl.dk. Lav en lille rappport med de ting du har fundet ud af og hvordan en angriber evt. kan misbruge oplysningerne. Rapporten skal du IKKE offentliggøres på gitlab, kun på din egen computer, resultater kan du fortælle om til næste undervisning!

</br>

# Udføresle af opgave

### Spørgsmål 3

Aktiv rekognoscering er ting som nmap og wireshark som aktiv skal lytte og sender forspørgelser ud for at få information omkring det netværk som de er på.

Passiv rekognoscering er tekniker som indhenter data uden folk ved det, en side som hjælper med at lave passiv rekognoscering er dnsdumpster som har en masse data på rigtig mange forskelige dns servere.


### Spørgsmål 4

Nmap er et gratis og open source hjælpeprogram til netværksanalyse, herunder overvågning af netværkets sikkerhed.

Wireshark er et netværkspakke-analysesoftware, det er fri open-source. Det bruges til netværks-fejlfinding, -analysering, protokolsudbredelse og uddannelse.

Mirror port er en måde at spejle den data der sendes fra en port til en anden så man kan lytte til det.

### Spørgsmål 5 

DNSdumpster.com er en gratis domain undersøgelses værktøj som kan finde hosts relatteret til domainet.

### Spørgsmål 6

En fjendlig aktør kan bruge rekognoscering til at dane sig et scope omkring et angrebs mål.

### Spørgsmål 7

Lige som en fjendtlig aktør kan jeg også bruge rekognoscering til danne mig et scope for ens kundes systemer og netværk.

### Spørgsmål 8




</br>

# Kilder og brugbar links 
