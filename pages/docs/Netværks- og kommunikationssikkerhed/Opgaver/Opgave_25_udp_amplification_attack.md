# Opgave beskrivelse

## Instruktioner

1. Forklar kort hvordan UDP virker og hvorfor IP spoofing kan virke med UDP.

2. Lav et eksempel

3. Forklar kort hvad et denial of service angreb er

4. Lav et eksempel med DNS eller NTP der viser et DoS angreb

5. Hvilken "service" bliver denied?

6. Alle systemer har bottlenecks, kom med relevante eksempler i denne kontekst

</br>

# Udføresle af opgave

1. UDP er en protokol til overførelse af data. UDP giver ingen garanti for at data kommer frem, i den forstand at afstender ikke får besked om dataen er modtaget af modtageren. Grunden til at det er nemt at IP spoof en UDP protokol, er fordi at afsenderen ikke får besked om modtagelse af en pakke og derfor kan man nem lave et man in the middel attack, uden at afsender eller modtager finder ud af det.

2. Den måde man vil kunne gøre det på er ved at side som en man in the middle og modtage pakkerne fra sourcen og så sende dem videre til destinationen med en ændret source IP.
Grunden til at man overhoved kan få lov til at ændre source IP'en er fordi at UDP er det man kalder connection-less og derved bliver source IP'en ikke valideret.

3. Denial of service eller DOS er en måde at angribe en service, med fokus på at benægte brugere af denne service adgang til servicen.

4. sudo hping3 -2 --flood 192.168.17.1 

![UDP DoS](./Billeder/UDPDoS.jpg)

</br>

# Kilder og brugbar links 

<https://www.cisa.gov/news-events/alerts/2014/01/17/udp-based-amplification-attacks>