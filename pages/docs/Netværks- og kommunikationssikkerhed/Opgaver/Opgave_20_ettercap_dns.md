# Opgave beskrivelse

### Instruktioner

1. Start to Kali og router på et internt netværk med NAT connection

2. Den første Kali kalder vi "kali" og den anden "victim"

3. Bruges OpenBSD routeren, så får man problemer

4. Tilføj følgende linier i /etc/ettercap/etter.dns

        totallynotgoogle-B.dns PTR 8.8.4.4
        totallynotgoogle-A.dns PTR 8.8.8.8
        dnsspoof.test A 8.8.8.8 3600
        Start ettercap

5. Stop sniffing

6. Scan for ip adresser

7. Add Victim as target 1 and router as target 2.

8. Enable "dns_spoof" plugin

9. Start ARP poisoning

10. Start sniff

11. Test at spoofing virker: ping dnsspoof.test

12. Start ping dns.google på victim

13. Kan det ses i wireshark på kali? Er der noget at bemærke i output eller i terminalen?

14. Forklar hvad ettercap gør

</br>

# Udføresle af opgave


    
</br>

# Kilder og brugbar links 
