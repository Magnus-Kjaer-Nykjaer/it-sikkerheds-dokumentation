# Opgave beskrivelse

Instruktioner

1. Noget med at hente en VM med en webserver, måske en raspberry?

2. Start wireshark

3. Gå ind på http http://mirror.one.com/debian/ og brug din browsers view source

4. Genfind trafikken i wireshark, brug follow tcp stream

5. Hent den samme side via https https://mirror.one.com/debian/

6. Genfind trafikken i wireshark, brug follow tcp stream

7. Brug curl/wget fra kommando linien til at hente HTTP fra linket i punkt 3

8. Sammenlign med resultatet fra browserens view source

</br>

# Udføresle af opgave

http: Man kan se hele korrespondancen mellem min pc og web serveren

    Accept-Encoding: gzip, deflate
    Connection: keep-alive
    Upgrade-Insecure-Requests: 1

https: Selve loadingen af siden kan man se i plain tekst ved både http og https men https har nogle flere settings på dens kald so man kan se på billedet under så bliver siden bedt om ikke at cache samt opgradere kaldet væk fra et insecure kald.

    Accept-Encoding: gzip, deflate
    DNT: 1
    Connection: keep-alive
    Upgrade-Insecure-Requests: 1
    Pragma: no-cache
    Cache-Control: no-cache

wget http: Den giver ti gange mindre data end hvis man gør det igennem browseren
    
</br>

# Kilder og brugbar links 
