# Opgave beskrivelse

1. Find læringsmålene i studieordningen på https://www.ucl.dk/studiedokumenter/it-sikkerhed og del linket mellem jer
2. I jeres team diskuter hvordan i forestiller jer at læringsmålene er relevante for faget og hvad i kan bruge dem til, begrund jeres valg og noter dem så vi kan diskutere det på klassen.

# Udføresle af opgave

Vi har læst læringsmålene og i gruppen snakket om hvad det indebære og handler om.

Her under kan man se de ting vi fik skrevet ned omkring læringsmålene.

Hvordan er læringsmålene relevante?
- vidensdelen er grundelementer/teknologier inden for faget.
- færdighedsdelen er relevante pga. at sikkerhedsdelen af faget. altså det giver mening at vi kender relevante sårbarheder og kan identificere dem.
- kompetence delen er relevant fordi, det giver mening at man har så meget viden om et sikkert netværk, at man kan opbygge et fra bunden af, samt finde huller og forbedre et der ikke er sikkert.

Efter at have snakket ude i grupperene, snakkede vi på klassen om der var noget vi ikke forstod og om hvad læringsmålene indebære.


# Kilder og brugbar links 
