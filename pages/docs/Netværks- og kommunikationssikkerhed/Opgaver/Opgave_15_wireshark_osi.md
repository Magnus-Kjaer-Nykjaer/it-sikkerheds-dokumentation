# Opgave beskrivelse

Instruktioner

1. Start din Kali maskine og check at den har internet forbindelse

2. Start wireshark og lyt på det udvendige interface

3. Start en kommando prompt og brug den til at lave et opslag på f.eks. dr.dk

4. Gennemgå trafiken i wireshark og fortæl hvad der ses

5. Tegn et netværks diagram med de vigtigste enheder

6. Ud fra wireshark, find ip adresser og MAC adresser på disse enheder

</br>

# Udføresle af opgave

Som man kan se på billedet under så den første dig jeg lavede var dirkete på dr.dk så som nummer 2 og 3 viser havde min lokale dns server allerede standard dns viden omkring dr.dk, men da jeg så bad den om at specefikt finde data på dr.dk fra min lokale dns server kalde den ud og opdateret dataen og gav noget mere tilbage.

![dig på dr.dk](../Opgaver/Billeder/digdr.PNG)

Dette er et diagram over hvordan jeg tror det ser ud og fungere, det ser ikke ud til at den kigger længere ud end til min ISPs dns server.

![DNS diagram](../Opgaver/Billeder/DNSNetv%C3%A6rksDiagram.drawio.png)
    
</br>

# Kilder og brugbar links 
