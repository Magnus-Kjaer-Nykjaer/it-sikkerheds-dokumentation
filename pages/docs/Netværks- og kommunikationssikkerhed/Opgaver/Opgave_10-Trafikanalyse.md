# Opgave beskrivelse

1. Start wireshark på lokal net/firma net og saml noget data op

2. Gem PCAPS

3. Se på det: er der ukendte protokoller? ukendte ip adresser? andet interessant?

4. Dyk ned i 2-3 streams, og forstå hvad der foregår.

5. Tag noter og vis det næste gang

</br>

# Udføresle af opgave

### Routerens cald til min pc for at den ved hvem ip er
![Router Who Is](../Opgaver/Billeder/RouterWhoIs.PNG)


### God fremvsning af TLS hanshake
![Fremvisning af TLS handshake](../Opgaver/Billeder/GodFremvisningAfTLSKommonikation.PNG)

Se teori på dette link <https://www.cloudflare.com/en-gb/learning/ssl/what-happens-in-a-tls-handshake/>


### Ligesom billedet over så er her en Who is fra routeren bare fokuseret på IPv6 addresser
![IPv6 Who is](../Opgaver/Billeder/IPv6RouterWhoIs.png)


### Jeg vidste ikke at chromium browseres havde deres egen UDP protokol der hedder QUIC
![QUIC protokol](../Opgaver/Billeder/QUICProtokol.png)

</br>

# Kilder og brugbar links 
