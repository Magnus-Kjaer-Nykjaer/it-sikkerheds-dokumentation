# Opgave beskrivelse

1. Installér iperf
2. Kør Iperf mod en public server, e.g. <code>iperf3 -c -some server- -t 10 -i 1 -p -some port- </code>
3. Kør Iperf mod en public server (modsat retning), e.g. <code>iperf3 -c -some server- -t 10 -i 1 -p -some port- -R</code>
4. Sammenlign resultat. Var hastigheder som forventet?
Note: Der er skrappe begrænsninger på brugen af de forskellige servere, f.eks at der kun er en ad gangen der kan bruge servicen.

# Udføresle af opgave

## Opgave 6 - more internet speed

Jeg kørte tutorialen igennem på det link som var givet og som er i kilder på denne side.

Det gik rimelig fejlfrit, på nær lidt gennemopfriskning af nogle kommandoer, indtil jeg skulle køre det.

Men jeg stødte på et problem med at den fejlede i alle testen på de forskelige porte, som man kan se i den log der er under.


    Loaded: loaded (/etc/systemd/system/iperf3-server@.service; enabled; preset: disabled)
    Active: failed (Result: exit-code) since Sun 2023-02-19 22:47:55 CET; 2min 19s ago
    Duration: 56ms
    Process: 950 ExecStart=/usr/bin/iperf3 -s -1 -p 9231 (code=exited, status=203/EXEC)
    Main PID: 950 (code=exited, status=203/EXEC)
    CPU: 3ms

    Feb 19 22:47:55 kali systemd[1]: iperf3-server@9231.service: Scheduled restart job, restart counter is at 5.
    Feb 19 22:47:55 kali systemd[1]: Stopped iperf3 server on port 9231.
    Feb 19 22:47:55 kali systemd[1]: iperf3-server@9231.service: Start request repeated too quickly.
    Feb 19 22:47:55 kali systemd[1]: iperf3-server@9231.service: Failed with result 'exit-code'.
    Feb 19 22:47:55 kali systemd[1]: Failed to start iperf3 server on port 9231.

Fordi det blev for sent på aftenen gav jeg op og gik i seng

# Kilder og brugbar links 

<https://iperf.fr/iperf-servers.php>