# Opgave beskrivelse

### Instruktioner

1. Løs opgaven i digdug rummet <https://tryhackme.com/room/digdug>

</br>

# Udføresle af opgave

Kommando for at finde flaget: 

    dig @10.10.102.130 givemetheflag.com

flag: 
    
    flag{0767ccd06e79853318f25aeb08ff83e2}
    
</br>

# Kilder og brugbar links 

Link jeg har brugt til jælp med svaret
<https://linux.die.net/man/1/dig>