# Opgave beskrivelse

1. Install ssldump either in Kali or similar Linux box
2. Run it from the command line: ssldump -j -ANH -n -i any | jq .
3. Generate some HTTPS traffic, e.g. by browsing
4. Evaluate what is shown.
    - How does ssl dump know the domain name? Could we turn that off?
    - When handshaking, the certificate chain is shown. Should that not be secret?

</br>

# Udføresle af opgave

Lidt af den ssldump som jeg kørte på min kali maskine

    {
    "connection_number": 1,
    "record_count": 1,
    "timestamp": "1678631819.1539",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 37178,
    "dst_name": "34.117.237.239",
    "dst_ip": "34.117.237.239",
    "dst_port": 443,
    "record_len": 512,
    "record_ver": "3.1",
    "msg_type": "Handshake",
    "handshake_type": "ClientHello",
    "ja3_str": "771,4865-4867-4866-49195-49199-52393-52392-49196-49200-49162-49161-49171-49172-156-157-47-53,0-23-65281-10-11-35-16-5-34-51-43-13-45-28-21,29-23-24-25-256-257,0",                                                       
    "ja3_fp": "579ccef312d18482fc42e2b822ca2430"
    }
    {
    "connection_number": 1,
    "record_count": 2,
    "timestamp": "1678631819.1809",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 122,
    "record_ver": "3.3",
    "msg_type": "Handshake",
    "handshake_type": "ServerHello",
    "ja3s_str": "771,4865,51-43",
    "ja3s_fp": "eb1d94daa7e0344597e756a1fb6e7054"
    }
    {
    "connection_number": 1,
    "record_count": 3,
    "timestamp": "1678631819.1809",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 1,
    "record_ver": "3.3",
    "msg_type": "ChangeCipherSpec"
    }
    {
    "connection_number": 1,
    "record_count": 4,
    "timestamp": "1678631819.1825",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 4435,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 2,
    "record_count": 1,
    "timestamp": "1678631819.5777",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 45790,
    "dst_name": "34.160.144.191",
    "dst_ip": "34.160.144.191",
    "dst_port": 443,
    "record_len": 211,
    "record_ver": "3.1",
    "msg_type": "Handshake",
    "handshake_type": "ClientHello",
    "ja3_str": "771,49195-49199-52393-52392-49196-49200-49162-49161-49171-49172-156-157-47-53,0-23-65281-10-11-35-16-5-13-28,29-23-24-25,0",                
    "ja3_fp": "fb0aa01abe9d8e4037eb3473ca6e2dca"
    }
    {
    "connection_number": 2,
    "record_count": 2,
    "timestamp": "1678631819.6098",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 72,
    "record_ver": "3.3",
    "msg_type": "Handshake",
    "handshake_type": "ServerHello",
    "ja3s_str": "771,49199,23-65281-11-35-16",
    "ja3s_fp": "9d9ce860f1b1cbef07b019450cb368d8"
    }
    {
    "connection_number": 2,
    "record_count": 3,
    "timestamp": "1678631819.6098",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 4062,
    "record_ver": "3.3",
    "msg_type": "Handshake",
    "handshake_type": "Certificate",
    "cert_chain": [
        {
        "cert_der": "MIIFTDCCBDSgAwIBAgISBGkTQga+WpAjWw/VxhAmh9d3MA0GCSqGSIb3DQEBCwUAMDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQDEwJSMzAeFw0yMzAyMDQwNzA2MDVaFw0yMzA1MDUwNzA2MDRaMC4xLDAqBgNVBAMTI2NvbnRlbnQtc2lnbmF0dXJlLTIuY2RuLm1vemlsbGEubmV0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx77B1xIMCdGSaH/eQAeunsCxkmNGbGgkgC9NfbBvD9HItigwF57gHNTIagOZES8gWn0i+lkVhBaPtGQzqISF+pEfwMdr2BaMLEFj3BnHkirCsLpCpTl/9qlQCfbRBwHmjb3RBvC7XsoXKSqjQX6Kvo3dBby9LqJq1+B4ljB9B+ytTdt8tjLDvkRkUIWU7+veQpILWn5prWIHV+8W7KDwP2VkGkt2z2A7lIzoQa3ZhTM+ZxCx/2AwD/j1hhf9sYdDg3Bh7KFW4oLfqjuVbJ3zm9QO2n5kkDVGKH/2br8JgT/sMQL+Jd+pGmpoco3/f1da/B1IHLW3E3mo0u1v+gWAfwIDAQABo4ICXjCCAlowDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBTu/up1juXtV2NNtzdmVn27s/4q9jAfBgNVHSMEGDAWgBQULrMXt1hWy65QCUDmH6+dixTCxjBVBggrBgEFBQcBAQRJMEcwIQYIKwYBBQUHMAGGFWh0dHA6Ly9yMy5vLmxlbmNyLm9yZzAiBggrBgEFBQcwAoYWaHR0cDovL3IzLmkubGVuY3Iub3JnLzAuBgNVHREEJzAlgiNjb250ZW50LXNpZ25hdHVyZS0yLmNkbi5tb3ppbGxhLm5ldDBMBgNVHSAERTBDMAgGBmeBDAECATA3BgsrBgEEAYLfEwEBATAoMCYGCCsGAQUFBwIBFhpodHRwOi8vY3BzLmxldHNlbmNyeXB0Lm9yZzCCAQQGCisGAQQB1nkCBAIEgfUEgfIA8AB3ALc++yTfnE26dfI5xbpY9Gxd/ELPep81xJ4dCYEl7bSZAAABhht2B5UAAAQDAEgwRgIhAJ0ziqoGsVKNV6uD0v6vtxjT6HACg6+7nFAuZkoYZnplAiEAqPzSjA/xqvrpB95kRTA6nW67rhp2o4xN3SeNOy8nDK4AdQB6MoxU2LcttiDqOOBSHumEFnAyE4VNO9IrwTpXo1LrUgAAAYYbdgelAAAEAwBGMEQCIDEviXvsK+y466HedS4hyEGhoJxhWfxOV7zbZh53OytWAiBdft4vg3tovLFt9JeDE/wLDjfb8NIttczInlnicOGY8TANBgkqhkiG9w0BAQsFAAOCAQEAFjKnicreAoA5OAq79zEA/oy/3EvFHQ2yVyw7X1UUAxSzVcUJ21kDF7XEVJUdGkVFM4pTkqDoFn/hiT/CDTr+ZsOmYTEO9csLj6EWtof5ABf11BaPoNuueOlXt4SSL7rGVJytodwrln3Dr5K18Y/Q5GWA4+LhfBDbB+aCbSP9tFYzpNsjl2jaa4D9x6ISRJ7tRijWnvcuoKEmkIkC8tnK2q5FTtNCy73XPOysQdVOWiLWK3Mc8z0l0G8/cC5QisOBQ6MqLQZz4ydlhfJymd1TWMapRFV1jMBbQYtKsGaTRYgVEkciQG+s54/bJ3dlbEgVr/V7EOlTpXYj/auk3KVOlA==",           
        "cert_subject": "/CN=content-signature-2.cdn.mozilla.net",
        "cert_issuer": "/C=US/O=Let's Encrypt/CN=R3",
        "cert_serial": "04:69:13:42:06:be:5a:90:23:5b:0f:d5:c6:10:26:87:d7:77"
        },
        {
        "cert_der": "MIIFFjCCAv6gAwIBAgIRAJErCErPDBinU/bWLiWnX1owDQYJKoZIhvcNAQELBQAwTzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2VhcmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMjAwOTA0MDAwMDAwWhcNMjUwOTE1MTYwMDAwWjAyMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNTGV0J3MgRW5jcnlwdDELMAkGA1UEAxMCUjMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC7AhUozPaglNMPEuyNVZLD+ILxmaZ6QoinXSaqtSu5xUyxr45r+XXIo9cPR5QUVTVXjJ6oojkZ9YI8QqlObvU7wy7bjcCwXPNZOOftz2nwWgsbvsCUJCWH+jdxsxPnHKzhm+/b5DtFUkWWqcFTzjTIUu61ru2P3mBw4qVUq7ZtDpelQDRrK9O8ZutmNHz6a4uPVymZ+DAXXbpyb/uBxa3Shlg9F8fnCbvxK/eG3MHacV3URuPMrSXBiLxgZ3Vms/EY96Jc5lP/Ooi2R6X/ExjqmAl3P51T+c8B5fWmcBcUr2Ok/5mzk53cU6cG/kiFHaFpriV1uxPMUgP17VGhi9sVAgMBAAGjggEIMIIBBDAOBgNVHQ8BAf8EBAMCAYYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMBIGA1UdEwEB/wQIMAYBAf8CAQAwHQYDVR0OBBYEFBQusxe3WFbLrlAJQOYfr52LFMLGMB8GA1UdIwQYMBaAFHm0WeZ7tuXkAXOACIjIGlj26ZtuMDIGCCsGAQUFBwEBBCYwJDAiBggrBgEFBQcwAoYWaHR0cDovL3gxLmkubGVuY3Iub3JnLzAnBgNVHR8EIDAeMBygGqAYhhZodHRwOi8veDEuYy5sZW5jci5vcmcvMCIGA1UdIAQbMBkwCAYGZ4EMAQIBMA0GCysGAQQBgt8TAQEBMA0GCSqGSIb3DQEBCwUAA4ICAQCFyk5HPqP3hUSFvNVneLKYY611TR6WPTNlclQtgaDqw+34IL9fzLdwALduO/ZelN7kIJ+m74uyA+eitRY8kc607TkC53wlikfmZW4/RvTZ8M6UK+5UzhK8jCdLuMGYL6KvzXGRSgi3yLgjewQtCPkIVz6D2QQzCkcheAmCJ8MqyJu5zlzyZMjAvnnAT45tRAxekrsu94sQ4egdRCnbWSDtY7kh+BImlJNXoB1lBMEKIq4QDUOXoRgffuDghje1WrG9ML+Hbisq/yFOGwXD9RiX8F6sw6W4avAuvDszue5L3sz85K+EC4Y/wFVDNvZo4TYXao6Z0f+lQKc0t8DQYzk1OXVu8rp2yJMC6alLbBfODALZvYH7n7do1AZls4I9d1P4jnkDrQoxB3UqQ9hVl3LEKQ73xF1OyK5GhDDX8oVfGKF5u+decIsH4YaTw7mP3GFxJSqv3+0lUFJoi5Lc5da149p90IdshCExroL1+7mryIkXPeFM5TgO9r0rvZaBFOvV2z0gp35Z0+L4WPlbuEjN/lxPFin+HlUjr8gRsI3qfJOQFy/9rKIJR0Y/8Omwt/8oTWgy1mdeHmmjk7j1nYsvC9JSQ6ZvMldlTTKB3zhThV1+XWYp6rjd5JW1zbVWEkLNxE7GJThEUG3szgBVGP7pSWTUTsqXnLRbwHOoq7hHwg==",      
        "cert_subject": "/C=US/O=Let's Encrypt/CN=R3",
        "cert_issuer": "/C=US/O=Internet Security Research Group/CN=ISRG Root X1",                                                                          
        "cert_serial": "91:2b:08:4a:cf:0c:18:a7:53:f6:d6:2e:25:a7:5f:5a"
        },
        {
        "cert_der": "MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMTDkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1owTzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2VhcmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XCov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpLwYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+DLtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5ysR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZXmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBcSLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2qlPRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TNDTwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYwSwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEBATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQub3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9EU1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26ZtuMA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuGWCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9Ohe8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFCDfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5",                                                                
        "cert_subject": "/C=US/O=Internet Security Research Group/CN=ISRG Root X1",                                                                         
        "cert_issuer": "/O=Digital Signature Trust Co./CN=DST Root CA X3",
        "cert_serial": "40:01:77:21:37:d4:e9:42:b8:ee:76:aa:3c:64:0a:b7"
        }
    ]
    }
    {
    "connection_number": 2,
    "record_count": 4,
    "timestamp": "1678631819.6098",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 300,
    "record_ver": "3.3",
    "msg_type": "Handshake",
    "handshake_type": "ServerKeyExchange"
    }
    {
    "connection_number": 2,
    "record_count": 5,
    "timestamp": "1678631819.6098",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 4,
    "record_ver": "3.3",
    "msg_type": "Handshake",
    "handshake_type": "ServerHelloDone"
    }
    {
    "connection_number": 2,
    "record_count": 6,
    "timestamp": "1678631819.6136",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 45790,
    "dst_name": "content-signature-2.cdn.mozilla.net",
    "dst_ip": "34.160.144.191",
    "dst_port": 443,
    "record_len": 37,
    "record_ver": "3.3",
    "msg_type": "Handshake",
    "handshake_type": "ClientKeyExchange"
    }
    {
    "connection_number": 2,
    "record_count": 7,
    "timestamp": "1678631819.6136",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 45790,
    "dst_name": "content-signature-2.cdn.mozilla.net",
    "dst_ip": "34.160.144.191",
    "dst_port": 443,
    "record_len": 1,
    "record_ver": "3.3",
    "msg_type": "ChangeCipherSpec"
    }
    {
    "connection_number": 2,
    "record_count": 8,
    "timestamp": "1678631819.6136",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 45790,
    "dst_name": "content-signature-2.cdn.mozilla.net",
    "dst_ip": "34.160.144.191",
    "dst_port": 443,
    "record_len": 40,
    "record_ver": "3.3",
    "msg_type": "Handshake"
    }
    {
    "connection_number": 1,
    "record_count": 5,
    "timestamp": "1678631819.6414",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 37178,
    "dst_name": "contile.services.mozilla.com",
    "dst_ip": "34.117.237.239",
    "dst_port": 443,
    "record_len": 1,
    "record_ver": "3.3",
    "msg_type": "ChangeCipherSpec"
    }
    {
    "connection_number": 1,
    "record_count": 6,
    "timestamp": "1678631819.6414",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 37178,
    "dst_name": "contile.services.mozilla.com",
    "dst_ip": "34.117.237.239",
    "dst_port": 443,
    "record_len": 53,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 7,
    "timestamp": "1678631819.6434",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 37178,
    "dst_name": "contile.services.mozilla.com",
    "dst_ip": "34.117.237.239",
    "dst_port": 443,
    "record_len": 165,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 8,
    "timestamp": "1678631819.6442",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 37178,
    "dst_name": "contile.services.mozilla.com",
    "dst_ip": "34.117.237.239",
    "dst_port": 443,
    "record_len": 228,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 2,
    "record_count": 9,
    "timestamp": "1678631819.6479",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 239,
    "record_ver": "3.3",
    "msg_type": "Handshake"
    }
    {
    "connection_number": 2,
    "record_count": 10,
    "timestamp": "1678631819.6479",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 1,
    "record_ver": "3.3",
    "msg_type": "ChangeCipherSpec"
    }
    {
    "connection_number": 2,
    "record_count": 11,
    "timestamp": "1678631819.6479",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 40,
    "record_ver": "3.3",
    "msg_type": "Handshake"
    }
    {
    "connection_number": 2,
    "record_count": 12,
    "timestamp": "1678631819.6479",
    "src_name": "content-signature-2.cdn.mozilla.net",
    "src_ip": "34.160.144.191",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 45790,
    "record_len": 64,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 9,
    "timestamp": "1678631819.6727",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 519,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 10,
    "timestamp": "1678631819.6727",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 57,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 11,
    "timestamp": "1678631819.6739",
    "src_name": "192.168.17.131",
    "src_ip": "192.168.17.131",
    "src_port": 37178,
    "dst_name": "contile.services.mozilla.com",
    "dst_ip": "34.117.237.239",
    "dst_port": 443,
    "record_len": 26,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 12,
    "timestamp": "1678631819.6971",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 26,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 13,
    "timestamp": "1678631819.8420",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 225,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 14,
    "timestamp": "1678631819.8420",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 38,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 15,
    "timestamp": "1678631819.8420",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 26,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }
    {
    "connection_number": 1,
    "record_count": 16,
    "timestamp": "1678631819.8420",
    "src_name": "contile.services.mozilla.com",
    "src_ip": "34.117.237.239",
    "src_port": 443,
    "dst_name": "192.168.17.131",
    "dst_ip": "192.168.17.131",
    "dst_port": 37178,
    "record_len": 34,
    "record_ver": "3.3",
    "msg_type": "application_data"
    }


</br>

# Kilder og brugbar links 
