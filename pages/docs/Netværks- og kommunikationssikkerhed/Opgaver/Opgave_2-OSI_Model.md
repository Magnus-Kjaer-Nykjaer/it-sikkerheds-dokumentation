# Opgave beskrivelse

1. Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.
2. Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad-som-helst med et netstik/wifi er ok at have med.
3. Forbered jer til diskussion/præsentation af hvad i har fundet så vi kan dele viden på klassen.

# Udføresle af opgave

De syv lag i OSI modellen er som følger

    7.  Applikationslaget
    6.  Præsentationslaget 
    5.  Sessionlaget
    4.  Transportlaget  -- Firewall -- TCP, UDP
    3.  Netværkslaget   -- Routers  -- IP adresser
    2.  Data Link-laget -- Switches -- MAC adresser
    1.  Det Fysiske lag -- Kabler


## Hardware enheder på OSI modellen

Det fysiske lag har ting som Ethernet kable, fiber kalber og lignende netværks kalber, men sådan noget som WIFI er også en del af det fysiske lag.

Lag nummer to indebær hardware så som ens internet kort eller en Switch og ligenden hardware der tager imod Ethernet kabler.

Lag nummer tres hardware eksempler er ting som en Router eller lignende ting der håndtere netværk.

Lag fire sørger for at tjekke at tingene kommer frem og i den rigtige rækkefølge. Firewall, TCP og UDP

### OSI modellen

![OSI Modellen](./Billeder/packtrav-osi-layers.png "OSI Modellen")

![OSI modellen med ting](./Billeder/OSIModellenMedTingP%C3%A5.png)
    
# Kilder og brugbar links 

<https://webintegrato.blogspot.com/2014/02/osi-modellen.html>
<https://www.practicalnetworking.net/series/packet-traveling/osi-model/>