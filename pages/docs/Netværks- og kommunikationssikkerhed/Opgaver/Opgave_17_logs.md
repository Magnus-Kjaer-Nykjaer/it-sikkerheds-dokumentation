# Opgave beskrivelse

Instruktioner

1. Start default open bsd router og kali linux'

2. Log in i konsole på routeren, og notér ip adressen på det eksterne interface: ifconfig em0

3. Åben en terminal i kali, og forbind routern: `ssh sysuser@

4. Se hvad der er af logs: cd /var/logs efterfult af ls

5. Hvad er der af log filer?

6. messages er vigtig i bsd (på linux er den tilsvarende syslog)

7. auth indeholder authentication events

8. Der er ikke DNS logs tilgængelig i default konfigurationen, men det kan slås til. Se logfile directive

9. Tricks: tail -f auth og start en ny ssh connection til routeren. Hvad sker der?

10. Log filer bliver allerede hentet ind fra grafana serveren til grafana serveren. Start server-mon02.

11. Gå til grafana web interface. Default url er http://192.168.111.20:3000

12. Gå til explore og lav en simpel loki søgning, f.eks. {filename="/var/log/syslog"} |= "ssh"

13. Lav også: {filename="/var/log/auth.log"} |= "ssh"

14. Sammelign

</br>

# Udføresle af opgave


    
</br>

# Kilder og brugbar links 
