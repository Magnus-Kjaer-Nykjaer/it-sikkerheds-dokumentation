# Opgave beskrivelse

### Instruktioner

1. Læs op på SSL inspection

2. Forklar hvad er er og hvad det bruges til

3. Tegn et netværksdiagram med relevante enheder og forklar hvad der sker.

</br>

# Udføresle af opgave

SSL inspection her flere navne som er HTTPS inspection, SSL inspection, TLS inspection, TLS break and inspect og HTTPS interception.



![SSL Inspection](../Opgaver/Andre%20filer/SSLInspectionDiagram.png)
    
</br>

# Kilder og brugbar links 

<https://www.cloudflare.com/en-gb/learning/security/what-is-https-inspection/>