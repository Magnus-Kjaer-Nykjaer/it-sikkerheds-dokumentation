# Opgave beskrivelse

1. papir+blyant øvelse.
Lav en IP subnet oversigt over dine devices og de tilhørende interne og eksterne netværk.
Anvend meget gerne det netværk i påtænker til semester projektet!

2. Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.

3. Set netbox VM op og dokumentér dit netværk via netbox interfacet. Brug getting started fra netbox dokumentationen (som er lavet med mkdocs!!)

        Netbox credentials:  
        consolen viser ip adressen
        netbox:netbox som password til web interface.
        andre logins i console

- Hvis i allerede har netværket til semester projektet skitseret eller tegnet som netværksdiagram så anvend det

- Alternativt kig på netværket i den virksomhed du er ansat i

- Ellers søg på nettet efter large network diagram eller large network topology

4. Hvis i bruger en virksomhed i opgaven så se på det IPAM dokumentation som virksomheden allerede har og forhold dig til det.
Alternativt research hvilke andre måder i kan lave IPAM dokumentation på. Noter det som en liste i jeres dokumentation.

</br>

# Udføresle af opgave

Startede med at downloade den .ova fil som Morten har lavet og indssætte den i VMware og navngive den NetBoxVM.

Der efter valgte jeg at åben NetBox vmen og gå ind på web versionen af NetBox og klikke rundt der inde og teste lidt forskeligt.

Jeg fulgte en del af tutorialen i den linked getting started ting.

</br>

# Kilder og brugbar links 
