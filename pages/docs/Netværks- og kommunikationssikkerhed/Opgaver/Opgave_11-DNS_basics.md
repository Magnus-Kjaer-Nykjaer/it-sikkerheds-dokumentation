# Opgave beskrivelse

### Instruktioner

1. Gennemfør rummet <https://tryhackme.com/room/dnsindetail>

2. Skriv dine opdagelser ned undervejs og dokumenter det på gitlab

Besvar herefter følgende om DNS og dokumenter undervejs:

1. Forklar med dine egne ord hvad DNS er og hvad det bruges til.

2. Hvilke dele af CIA er aktuelle og hvordan kan DNS sikres? <https://blog.cloudflare.com/dns-encryption-explained/>

3. Hvilken port benytter DNS ?

4. Beskriv DNS domæne hierakiet

5. Forklar disse DNS records: A, AAAA, MX, TXT og CNAME.

6. Brug nslookup til at undersøge hvor mange mailservere ucl.dk har

7. Brug dig til at finde txt records for ucl.dk

8. Brug wireshark til:

    - Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik

    - Lav screenshots af eksempler på DNS trafik

</br>

# Udføresle af opgave

Kommandoer man kan køre for at se DNS info:

    nslookup webside.com
    nslookup --type=A webside.com       -- viser IPv4 addressen for domainet
    nslookup --type=CNAME website.com   -- viser CNAME for domainet
    nslookup --type=MX website.thm      -- viser MX for domainet
    nslookup --type=TXT website.thm     -- viser TXT for domainet

Her er en model over flowet for et standard DNS kald:
![DNS flow model](../Opgaver/Billeder/dnsmodel.png)


### Opgaver:

#### Spørgsmål 1

DNS (Domain Name System) er et system som bruges på internettet til at systemer ved hvad ip addressen er på den hjemmeside de gerne vil tilgå via dens URL.

#### Spørgsmål 2

Kryptering af DNS protokolen har med confidentiality i CIA modellen, da man lige nu har mulighed for at se alt den data som bliver sendt via DNS.

#### Spørgsmål 3

53

#### Spørgsmål 4

Inden for DNS findes der er hieraki som bruges til at bestemme hvem der ved hvad om en hjemmes side domain name.

Det første lag er den lokale chache man har liggende lokalt på sin maskine som kan indeholde DNS informaion, denne lokale chache indeholder ting som localhost samt andet du fornylig har tilgået.

Det andet lag er den recursive DNS server som endten kan være en lokal server eller kan være hos din ISP, hvilket er tilfældet for de fleste folk. Dette lag bruges til at spare tid for at slippe for at skulle igennem hele processen for at tilgå hyppige ting så som google, facebook og andre sider mange tilgår ofte.

Det tredje lag er root laget som er det lag der søger for at pege dit kald hen til den rigtige TLD server, dette gøres via det TLD du har bag ved så .dk vil fortæle root serveren at den skal sende dig til .dk serveren.

Det fjerde lag er TLD laget som sørger for at sende kaldet vider til Authorative DNS serveren.

Det femte lag er Authorative laget som indeholder den ip som maskine skal bruge til at tilgå siden. 

#### Spørgsmål 5

A er forkortelsen for den IPv4 adresser ens domain peger på

AAAA er forkortelsen for den IPv6 adresser ens domain peger på 

MX er den forkotelse man bruger til at hentyde til mail servere

TXT er den forkotelse man burger til hentyde til egen defineret værdier, bruges ofte til autorisation af sit domæne.

CNAME (Cronical name record) er den forkotelse man bruger til at hentyde til subdomain navne

#### Spørgsmål 6

![Mail server på ucl](../Opgaver/Billeder/UCLMailservere.png)

#### Spørgsmål 7

![Dig kommando på ucl.dk](../Opgaver/Billeder/DigKommando.png)

#### Spørgsmål 8

Man skal filtrere på UDP stream for at få den til at vise DNS streamen

![WireShark DNS stream](../Opgaver/Billeder/WireSharkDNS.png)

</br>

# Kilder og brugbar links 
