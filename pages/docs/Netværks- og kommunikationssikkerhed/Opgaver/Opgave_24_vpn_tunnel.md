# Opgave beskrivelse

### Instruktioner

1. Start 2x kali på det samme subnet

2. ping 8.8.8.8 på begge og genfind trafik i wireshark

3. Tegn et netværksdiagram med relevant enheder

4. set up wg client på den ene and server på den anden

5. Der er en guide her

6. Brug 172.16.100.1/24 til serveren og 172.16.100.2/32 til klienten

7. Tilføj en route på klienten sudo ip route add 8.8.8.0/8 via 172.16.100.1

8. Ping 172.16.100.1 fra klienten og genfind trafik

9. Ping 8.8.8.8 fra client og genfind trafik.

10. virker tunnelen?

11. updater netværksdiagram med nye ip adresser og interfaces

12. Se på routing tables og foklar hvad der ses

</br>

# Udføresle af opgave

Server

    [Interface]

    Address = 172.16.100.1/24

    SaveConfig = true

    ListenPort = 51820

    PrivateKey = 0NailF2tQ3OyhfoDPwifEfuj9DthRInviZ4hJrUuY1s=
    

    [Peer]

    PublicKey = 3fixeGniaWDcETIs1UiTthF3RIkhOafRdlFtkDuAW1E= 

    AllowedIPs = 172.16.100.2/32

Client

    [Interface]

    Address = 172.16.100.2/32

    PrivateKey = gJzbFUYGzg0fPJdyjoroKCWAZzs3UlwqAA+Dk5b3OF8=

    
    [Peer]

    PublicKey = 2bbkcrsMRjqukIzQu9pUrK0z5grQR8XfhxAJM4tF3ls=

    Endpoint = 172.16.100.1/24:51820

    AllowedIPs = 0.0.0.0/0

    PersistentKeepalive = 21

![Wireguard Server Opstart](../Opgaver/Billeder/Wireguard.jpg)

![Wireguard client](../Opgaver/Billeder/wireguardclient.jpg)

</br>

# Kilder og brugbar links 
