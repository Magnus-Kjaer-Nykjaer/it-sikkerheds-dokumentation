# Opgave beskrivelse

Instruktioner

1. Start din Kali maskine og en router, check at der er internet forbindelse

2. Tegn et netværksdiagram med de relevante enheder

3. Find to egnede steder i netværket hvor man kan lytte til host trafik og til ekstern internet trafikken

4. Start to instances af wireshark eller tcpdump der lytter på de valgte punkter

5. Connect til en hjemmeside fra kalien. Brug curl eller wget for at reducere mængden af trafik

6. Genfind trafikken i begge wireshark streams

7. Vis forskellene på lag 2 og 3

</br>

# Udføresle af opgave

![NAT Diagram](../Opgaver/Billeder/NatTrafik.PNG)

Man kan se på billedet under den trafik der er mellem min kali maskine og den side som man rammer når man prøver at curl google.com. Den rammer dog en 301 site moved, så det er ikke google 8.8.8.8 ip adresse man får.

![Google curl kald](../Opgaver/Billeder/TCPTrafikMellemPCOgCurlGooglecom.png)

Er ikke sikker på at det var dette der menes men jeg startede en ny wireshark på min host maskine som lyttede til min maskines wifi forbindelse, der efter kørte jeg en curl på google.com på min kali og så hvad jeg fik af resultater.

Så som man kan se på billedet under så er der ikke den store forskel i TCP strømmen.

![Lyt fra host på kali curl kald](../Opgaver/Billeder/TCPHostTrafikFraKali.png)

Havde dog regnet med at at jeg kunne se lidt mere fra host maskinen af men når det kun er TCP strømen jeg følger giver det vel mening kun at se den.

</br>

# Kilder og brugbar links 
