# Opgave beskrivelse

Lav netværks diagram af nedenstående billed

![Tavle billed af netværksdiagram](../Opgaver/Billeder/SimpeltNetvaerkFraTavlen.jpg)
</br>

# Udføresle af opgave

## Fysisk Diagram

![Fysisk Diagram](../Opgaver/Billeder/FysiskNetvaerk.PNG)

## Logisk Diagram

![Logisk Diagram](../Opgaver/Billeder/LogiskNetvaerk.png)


</br>

# Kilder og brugbar links 
