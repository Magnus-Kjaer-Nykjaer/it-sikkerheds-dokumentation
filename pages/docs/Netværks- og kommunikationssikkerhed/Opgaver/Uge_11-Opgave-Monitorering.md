# Opgave beskrivelse

### Testing
1. Download a test vm from here and install it in vmware workstation

2. Connect it to vmnet111 (or the equivante 192.168.111.0/24 subnet)

3. There is an information page on http://192.168.111.20

Unprivileged user is sysuser:sysuser123 and admins user is root:root123

### Working with grafana
1. Make a block diagram showing the relationship between node_exporter, prometheus and grafana.

2. Add a grafana dashboard that uses the node_exporter values from prometheus

3. Add a grafana dashboard that uses the logs from loki

4. Set up an extra VM with node_exporter and update prometheus to scrape from it

</br>

# Udføresle af opgave

Hentede .ova filen ned til debian serveren og til gik den via det givet link.

Ud over det gik jeg ind og installeret den nye .ova fil til OpgenBSD, for at have en der er på den nyeste version.

Efter prøvede jeg at få frem vist noget data, men stødte ind i et problem med at den ikke kunne finde noget.

Jeg fandt så ud af ved at gå direkte ind i promethus siden at den udleveret debian server har en lokal tid der er en time foran og derfor skal man vente en time før den vise data.

Efter en times vente tid begynde den af fremvise data.

![Grafan dashboard med promethues](../Opgaver/Billeder/GrafanaDashborad.png)

</br>

# Kilder og brugbar links 
