
</br>

## Stats af min Raspberry pi
Raspberry pi 3 model b v1.2
64 gb sd kort

Køre Raspberry Pi OS Lite

Username: mkn

Password: DetteErEtKodeord

Efter at have sat den op og updated og upgraded den installeret jeg docker.

### Docker 
Fulgte denne guide for docker installationen: <https://pimylifeup.com/raspberry-pi-docker/>

docker download command : 

    curl -sSL https://get.docker.com | sh

docker user commands : 

    sudo usermod -aG docker mkn

Kommando for at se om en user er en del af docker gruppen: groups

For at test om docker virker køre vi kommandoen: 

    docker run hello-world

Da den ikke er installeret lokalt henter den den nyeste version og køre den.

Så som man kan se under så virkede det efter at den fandt docker imaged

    Hello from Docker!
    This message shows that your installation appears to be working correctly.


### Portainer 
Da det virkede begynde jeg på portainer guiden: <https://pimylifeup.com/raspberry-pi-portainer/>

Den første kommando jeg kørte var: 
    
    sudo docker pull portainer/portainer-ce:latest

For at download den nyeste version af portainer.

Der efter kørte jeg kommandoen 

    sudo docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest

Det denne kommando gør er at den siger at portainer skal køre på porten 9000 og at containeren skal hedde portainer og at den altid skal genstarte hvis den går ned.

Efter at den har startede portainer containeren kan den tilgås via RaspberryPiens ip addresse på port 9000.

    http://192.168.87.154:9000

Da vi nu har en side vi kan tilgå skal vi lave first setup af vores portainer bruger.

Username: mknPortainer

Efter at have lavet brugeren til portainer og valgt at det er docker den skal køre, er alt sat op og klart til ntopng.

### ntopng

<https://hub.docker.com/r/ntop/ntopng>

I forsøget på at på ntop til at køre via portainer, endte jeg med at ødelæg portainers contianer side så den ikke vil vise den længere.

Jeg har så derfor valgt at slette både den ntop container som kørte og den portianer container der kørte.

Der efter prøvede jeg at køre det seperat i men så løb den ind i denne fejl da jeg docker runnede den.

    mkn@raspberrypi:~ $ docker run -it -p 3000:3000 --net=host ntop/ntopng:latest -i 192.168.87.1
    WARNING: Published ports are discarded when using host network mode
    WARNING: The requested image's platform (linux/amd64) does not match the detected host platform (linux/arm64/v8) and no specific platform was requested
    exec /run.sh: exec format error

Er kommet frem til at grunden til at jeg ikke kan få ntop til at virke som docker image er fordi docker containeren ikke understøtter arm64 og det derfor ikke kan køre på raspery pi som docker.

## Links og kilder
Raspberry documentation:<https://www.raspberrypi.com/documentation/computers/getting-started.html#setting-up-your-raspberry-pi>

Portainer tutorial:<https://pimylifeup.com/raspberry-pi-portainer/>