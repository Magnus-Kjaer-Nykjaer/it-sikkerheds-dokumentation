# Brugbar software og links

<https://nmap.org/> -- Nmap er et gratis og open source hjælpeprogram til netværksanalyse, herunder overvågning af netværkets sikkerhed.

<https://www.wireshark.org/> -- Wireshark er et netværkspakke-analysesoftware, det er fri open-source. Det bruges til netværks-fejlfinding, -analysering, protokolsudbredelse og uddannelse.

<https://www.openbsd.org/> -- OpenBSD er et frit styresystem i UNIX-familien med rødder i det oprindelige Berkeley Software Distribution.

<https://manualsbrain.com/en/> -- Hjemmeside til at finde manualer på mange forskelige produkter.

<https://app.diagrams.net/> -- Diagram side

<https://demo.netbox.dev/> -- Netværks håndterings system. Docs:<https://docs.netbox.dev/en/stable/>

<https://www.ntop.org/products/traffic-analysis/ntop/> -- High-Speed Web-based Traffic Analysis and Flow Collection

<https://dnsdumpster.com/> -- DNSdumpster.com is a FREE domain research tool that can discover hosts related to a domain.

<https://github.com/SpiderLabs/ModSecurity> -- ModSecurity er en open source WAF (Web application firewall).

<https://www.shodan.io/> -- Shodan er en søgemaskine, der lader brugere søge efter forskellige typer servere forbundet til internettet ved hjælp af en række forskellige filtre.

