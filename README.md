[![pipeline status](https://gitlab.com/Magnus-Kjaer-Nykjaer/it-sikkerheds-dokumentation/badges/main/pipeline.svg)](https://gitlab.com/Magnus-Kjaer-Nykjaer/it-sikkerheds-dokumentation/-/commits/main)

# Velkommen til min dokumentations side

For at komme ind på min dokumentations side kan du bruge dette link:
<a href="https://magnus-kjaer-nykjaer.gitlab.io/it-sikkerheds-dokumentation/">Dokumentations side</a>

